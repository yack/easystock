# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for users tasks """

import logging

import formencode
from formencode import validators
import pytz

from easystock.forms import PasswordMatchValidator
from easystock.forms import SiteValidator
from easystock.models import User


log = logging.getLogger(__name__)


#
# Validators
#

class UsernameValidator(validators.FormValidator):

    def _from_python(self, value, state):
        return value

    def _to_python(self, field_dict, state):
        # Transform username to lowercase
        field_dict['username'] = field_dict['username'].lower()

        return field_dict


#
# Forms
#

# users.create, users.modify
class ModifyForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    email = validators.Email(if_missing=None, if_empty=None)
    firstname = validators.String(if_missing=None, if_empty=None)
    lastname = validators.String(if_missing=None, if_empty=None)
    password = validators.String(if_missing=None, if_empty=None)
    password_confirm = validators.String(if_missing=None, if_empty=None)
    profiles = formencode.ForEach(validators.OneOf(list(User.PROFILES.keys()), not_empty=True))
    site = SiteValidator(not_empty=True)
    status = validators.OneOf(list(User.STATUSES.keys()), not_empty=True)
    timezone = validators.OneOf(pytz.all_timezones, if_missing=None, if_empty=None)
    username = validators.String(not_empty=True)

    chained_validators = [PasswordMatchValidator(), UsernameValidator()]


# users.profile
class ProfileForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    email = validators.Email(if_missing=None, if_empty=None)
    firstname = validators.String(if_missing=None, if_empty=None)
    lastname = validators.String(if_missing=None, if_empty=None)
    password = validators.String(if_missing=None, if_empty=None)
    password_confirm = validators.String(if_missing=None, if_empty=None)
    timezone = validators.OneOf(pytz.all_timezones, if_missing=None, if_empty=None)
    username = validators.String(not_empty=True)

    chained_validators = [PasswordMatchValidator(), UsernameValidator()]


# users.search
class SearchForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    email = validators.String(if_missing=None, if_empty=None)
    firstname = validators.String(if_missing=None, if_empty=None)
    format = validators.OneOf(['csv', 'xlsx', 'html'], if_empty='html', if_missing='html', if_invalid='html')
    lastname = validators.String(if_missing=None, if_empty=None)
    profile = validators.OneOf(list(User.PROFILES.keys()), if_empty=None, if_missing=None)
    site = SiteValidator(if_missing=None, if_empty=None)
    status = validators.OneOf(list(User.STATUSES.keys()), if_empty=None, if_missing=None)
    username = validators.String(if_missing=None, if_empty=None)
