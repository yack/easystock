# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for assets """

import logging

import formencode
from formencode import validators

from pyramid_helpers.forms.validators import DateTime
from pyramid_helpers.i18n import N_

from easystock.forms import AreaValidator
from easystock.forms import ProductValidator
from easystock.forms import UserValidator
from easystock.models import Asset


log = logging.getLogger(__name__)


#
# Forms
#

# api.assets.create
class CreateForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    area = AreaValidator(not_empty=True)
    author = UserValidator(not_empty=True)
    date = DateTime(format=N_('%m/%d/%Y %H:%M'), is_naive_utc=True, if_missing=None, if_empty=None)
    product = ProductValidator(not_empty=True)
    serial_number = validators.String(if_missing=None, if_empty=None)
    state = validators.OneOf([k for k in Asset.STATES], not_empty=True)


# api.assets.update
class UpdateForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    area = AreaValidator(not_empty=True)
    author = UserValidator(not_empty=True)
    date = DateTime(format=N_('%m/%d/%Y %H:%M'), is_naive_utc=True, if_missing=None, if_empty=None)
    serial_number = validators.String(if_missing=None, if_empty=None)
    state = validators.OneOf([k for k in Asset.STATES], not_empty=True)


# assets.search
class SearchForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    area = AreaValidator(if_empty=None, if_missing=None)
    category = validators.String(if_empty=None, if_missing=None)
    date = DateTime(format=N_('%m/%d/%Y %H:%M'), is_naive_utc=True, if_missing=None, if_empty=None)
    label = validators.String(if_empty=None, if_missing=None)
    product = ProductValidator(if_empty=None, if_missing=None)
    serial_number = validators.String(if_missing=None, if_empty=None)
    state = validators.OneOf([k for k in Asset.STATES], if_missing=None, if_empty=None)
