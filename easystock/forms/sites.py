# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for sites tasks """

import logging

import formencode
from formencode import validators

from easystock.models import Area
from easystock.models import Site
from easystock.forms import AreaValidator

log = logging.getLogger(__name__)


class AreaSchema(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    area = AreaValidator(if_missing=None, if_empty=None)
    building = validators.String(not_empty=True)
    floor = validators.Int(not_empty=True)
    room = validators.String(not_empty=True)
    status = validators.OneOf(list(Area.STATUSES.keys()), not_empty=True)


#
# Forms
#

# sites.areas
class AreasForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    areas = formencode.ForEach(AreaSchema)


# sites.create, sites.modify
class ModifyForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    address = validators.String(if_missing=None, if_empty=None)
    description = validators.String(if_missing=None, if_empty=None)
    name = validators.String(not_empty=True)
    status = validators.OneOf(list(Site.STATUSES.keys()), not_empty=True)


# sites.search
class SearchForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    address = validators.String(if_missing=None, if_empty=None)
    description = validators.String(if_missing=None, if_empty=None)
    name = validators.String(if_missing=None, if_empty=None)
    status = validators.OneOf(list(Site.STATUSES.keys()), if_empty=None, if_missing=None)
