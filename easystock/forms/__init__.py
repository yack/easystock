# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for authentication """

import logging

import formencode
from formencode import validators

from pyramid_helpers.forms.validators import List
from pyramid_helpers.i18n import N_

from easystock.models import DBSession
from easystock.models import Area
from easystock.models import Asset
from easystock.models import Product
from easystock.models import Site
from easystock.models import User


log = logging.getLogger(__name__)


#
# Validators
#
class PasswordMatchValidator(validators.FormValidator):
    messages = {
        'dont_match': N_('Passwords do not match'),
    }

    def _from_python(self, value, state):
        return value

    def _to_python(self, field_dict, state):
        errors = {}

        if field_dict['password'] != field_dict['password_confirm']:
            errors['password'] = formencode.Invalid(self.message('dont_match', state), field_dict['password'], state)
            errors['password_confirm'] = formencode.Invalid(self.message('dont_match', state), field_dict['password_confirm'], state)

        if errors:
            error_list = sorted(errors.items())
            error_message = ', '.join([value.msg for name, value in error_list if value])
            raise formencode.Invalid(error_message, field_dict, state, error_dict=errors)

        # No error
        return field_dict


class SimpleObjectValidatorMixin:
    """ Validator mixin for SQLAlchemy ORM classes """

    __model__ = None
    __msg__ = N_(u'Invalid value')
    __pkey__ = 'id'

    def __init__(self, *args, **kwargs):
        # Calling inherited
        super().__init__(*args, **kwargs)

        # Set custom message
        self._messages['integer'] = self.__msg__
        self._messages['invalid'] = self.__msg__

    def _convert_from_python(self, value, state):
        if not isinstance(value, self.__model__):
            raise formencode.Invalid(self.message('invalid', state), value, state)

        value = getattr(value, self.__pkey__)

        # Calling inherited
        return super()._convert_from_python(value, state)

    def _convert_to_python(self, value, state):
        assert self.__model__ is not None, 'Using an unconfigured SimpleObjectValidatorMixin class, please set __model__ attribute'

        # Calling inherited
        value = super()._convert_to_python(value, state)

        instance = DBSession.query(self.__model__).get(value)
        if instance is None:
            raise formencode.Invalid(self.message('invalid', state), value, state)

        return instance


class AreaValidator(SimpleObjectValidatorMixin, validators.Int):
    """ Area object validator """
    __model__ = Area


class AssetValidator(SimpleObjectValidatorMixin, validators.Int):
    """ Asset object validator """
    __model__ = Asset


class ProductValidator(SimpleObjectValidatorMixin, validators.Int):
    """ Product object validator """
    __model__ = Product


class SiteValidator(SimpleObjectValidatorMixin, validators.Int):
    """ Site object validator """
    __model__ = Site


class UserValidator(SimpleObjectValidatorMixin, validators.Int):
    """ User object validator """
    __model__ = User


#
# Forms
#
class AutocompleteForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    exact = validators.StringBool(if_missing=False, if_empty=False)
    excluded_ids = List(validators.Int())
    selected_ids = List(validators.Int())
    term = validators.String(if_missing=None, if_empty=None)


# auth.sign-in
class SignInForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    came_from = validators.String(if_missing=None, if_empty=None)
    username = validators.String(not_empty=True)
    password = validators.String(not_empty=True)


class InventorySearchForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    barcode = validators.String(if_empty=None, if_missing=None)


# imports.upload
class UploadForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    create_only = validators.StringBool(if_missing=False, if_empty=False)
    qfile = validators.FieldStorageUploadConverter(not_empty=True)
    validators.FieldStorageUploadConverter(if_missing=None, if_empty=None)
    sheet = validators.String(if_missing=None, if_empty=None)
    target = validators.String(if_missing=None, if_empty=None)
