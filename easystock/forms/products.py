# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for products """

import logging

import formencode
from formencode import validators

from easystock.models import Product


log = logging.getLogger(__name__)


#
# Forms
#

# products.create
# products.modify
class ModifyForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    barcode = validators.String(if_empty=None, if_missing=None)
    brand = validators.String(if_empty=None, if_missing=None)
    category = validators.String(not_empty=True)
    label = validators.String(not_empty=True)
    model = validators.String(if_empty=None, if_missing=None)
    reference = validators.String(if_empty=None, if_missing=None)
    status = validators.OneOf([k for k in Product.STATUSES], not_empty=True)


# products.search
class SearchForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    barcode = validators.String(if_empty=None, if_missing=None)
    brand = validators.String(if_empty=None, if_missing=None)
    category = validators.String(if_empty=None, if_missing=None)
    model = validators.String(if_empty=None, if_missing=None)
    label = validators.String(if_empty=None, if_missing=None)
    status = validators.OneOf([k for k in Product.STATUSES], if_empty=None, if_missing=None)
