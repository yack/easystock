# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for stocks """

import logging

import formencode
from formencode import validators

from pyramid_helpers.forms.validators import DateTime
from pyramid_helpers.i18n import N_

from easystock.forms import AreaValidator
from easystock.forms import ProductValidator
from easystock.forms import UserValidator
from easystock.models import Stock

log = logging.getLogger(__name__)


#
# Forms
#

# api.stocks.create
class CreateForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    action = validators.OneOf([k for k in Stock.ACTIONS], not_empty=True)
    area = AreaValidator(not_empty=True)
    author = UserValidator(not_empty=True)
    date = DateTime(format=N_('%m/%d/%Y %H:%M'), is_naive_utc=True, if_missing=None, if_empty=None)
    message = validators.String(if_missing=None, if_empty=None)
    product = ProductValidator(not_empty=True)
    value = validators.Int(min=0, max=10000, not_empty=True)


# stocks.search
class SearchForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    action = validators.OneOf([k for k in Stock.ACTIONS], if_missing=None, if_empty=None)
    area = AreaValidator(if_empty=None, if_missing=None)
    category = validators.String(if_empty=None, if_missing=None)
    date = DateTime(format=N_('%m/%d/%Y %H:%M'), is_naive_utc=True, if_missing=None, if_empty=None)
    label = validators.String(if_empty=None, if_missing=None)
    product = ProductValidator(if_empty=None, if_missing=None)
    view_all = validators.StringBool(if_empty=False, if_missing=False)
