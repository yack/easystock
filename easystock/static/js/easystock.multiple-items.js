/*
 * EasyStock -- Simple Stock and Assets mamagement tool
 * By: Cyril Lacoux <clacoux@ifeelgood.org>
 *
 * Copyright (C) 2018-2019 Cyril Lacoux
 * https://gitlab.com/yack/easystock
 *
 * This file is part of EasyStock.
 *
 * EasyStock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * EasyStock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Multiple items management
 */
function multipleItemsAdd(e) {
    if (e) {
        e.preventDefault();
    }
    var container = $(this).parents('.multiple-items:first');
    var group = container.data('group');

    // Clone last child
    var item = container.find('.item:first').clone();

    // Reset inputs
    multipleItemsReset(item);

    // Append new item
    $(container).find('.item:last').after(item);

    multipleItemsRewriteIndexes(group);

    // Restore datepicker
    if ($.fn.datetimepicker) {
        item.find('.date.datepicker').datetimepicker({
            locale: language,
            format: 'L'
        });
    }

    // Restore selectpicker
    if ($.fn.select2) {
        item.find('.select2-container').remove();
        item.find('select.selectpicker').show().select2(select2Options);
    }

    // Del button
    item.find('a.del-item').on('click', multipleItemsConfirmDel);

    // Up/down buttons
    item.find('a.item-down').on('click', multipleItemsMoveDown);
    item.find('a.item-up').on('click', multipleItemsMoveUp);

    $(container).trigger('multipleItems.addItem', item);

    return false;
}


function multipleItemsDel(container, item) {
    var group = container.data('group');

    item.remove();

    multipleItemsRewriteIndexes(group);

    $(container).trigger('multipleItems.delItem');
}


function multipleItemsConfirmDel(e) {
    e.preventDefault();

    var container = $(this).parents('.multiple-items:first');
    var item = $(this).parents('.item:first');
    var message = [];

    // Retrieved label + value of all item's fields
    $.each(item.find(':input'), function(i, input) {
        var data, label, val;

        label = $(input).attr('placeholder');
        if (!label || input.id.search('s2id') === 0) {
            return;
        }

        if ($(input).hasClass('select2-offscreen')) {
            data = $(input).select2('data');
            if (!data) {
                return;
            }
            if ($.isArray(data)) {
                val = [];
                $.each(data, function(i, v) {
                    val.push(v.text);
                });
                val = val.join(', ');
            }
            else {
                val = data.text;
            }
        }
        else {
            val = $(input).val();
        }
        if (val) {
            message.push('<strong>' + label + '</strong>: ' + val);
        }
    });

    if (message.length > 0) {
        alertify.confirm()
            .setting('title', _('Do you really want to delete the following data ?'))
            .setting('message', message.join('<br>'))
            .setting('oncancel', function() {
                // Abort
            })
            .setting('onok', function() {
                multipleItemsDel(container, item);
            })
            .show();
    }
    else {
        // No confirmation: item fields are all empty
        multipleItemsDel(container, item);
    }
}


function multipleItemsManageButtons(group) {
    var container = $('.multiple-items[data-group="' + group + '"]');
    var count = $(container).find('.item').length;

    if (count > 1) {
        // All lines have «del» button
        $(container).find('.item a.del-item').removeClass('disabled');
    } else {
        // Only one line, disabling «del» button
        $(container).find('.item a.del-item').addClass('disabled');
    }

    $(container).find('a.item-down, a.item-up').removeClass('disabled');
    $(container).find('.item:first a.item-up').addClass('disabled');
    $(container).find('.item:last a.item-down').addClass('disabled');
}


function multipleItemsMoveDown(e) {
    e.preventDefault();

    var container = $(this).parents('.multiple-items:first');
    var group = $(container).data('group');
    var item = $(this).parents('.item:first');

    item.insertAfter(item.next());

    multipleItemsRewriteIndexes(group);

    $(container).trigger('multipleItems.itemDown', item);
}


function multipleItemsMoveUp(e) {
    e.preventDefault();

    var container = $(this).parents('.multiple-items:first');
    var group = $(container).data('group');
    var item = $(this).parents('.item:first');

    item.insertBefore(item.prev());

    multipleItemsRewriteIndexes(group);

    $(container).trigger('multipleItems.itemUp', item);
}


function multipleItemsReset(item) {
    // Reset inputs
    item.find('input:not([type="checkbox"]):not(input[type="radio"])').val('');
    item.find('input[type="checkbox"],input[type="radio"]').prop('checked', false);
    if ($.fn.select2) {
        item.find('.select2-container + input').select2('val', '');
        item.find('.select2-container + select').val('');
    }
    else {
        item.find('select').val('');
    }
    item.find('textarea').html('');

    // Remove any errors
    item.find('.input-error').remove();
}


function multipleItemsRewriteIndexes(group) {
    var container = $('.multiple-items[data-group="' + group + '"]');

    $(container).find('.item').each(function(index, item) {
        $(item).find('input, select, textarea').each(function(idx, input) {
            if ($(input).attr('name') === undefined) {
                return;
            }
            var fieldType = $(input).attr('name').split('.')[1];
            if (fieldType) {
                $(input)
                    .attr('id', group + '-' + index + '-' + fieldType)
                    .attr('name', group + '-' + index + '.' + fieldType);
            }
        });
    });

    multipleItemsManageButtons(group);
}


function multipleItemsSetup(container) {
    if (!container) {
        $('.multiple-items').each(function(index, container) {
            multipleItemsSetup(container);
        });

        return;
    }

    var group = $(container).data('group');

    multipleItemsManageButtons(group);

    // Add/del buttons
    $(container).find('a.add-item').on('click', multipleItemsAdd);
    $(container).find('a.del-item').on('click', multipleItemsConfirmDel);

    // Up/down buttons
    $(container).find('a.item-down').on('click', multipleItemsMoveDown);
    $(container).find('a.item-up').on('click', multipleItemsMoveUp);
}


/*
 * Initialization
 */
$(document).ready(function() {
    multipleItemsSetup();
});
