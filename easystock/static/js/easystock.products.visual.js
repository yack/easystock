/*
 * EasyStock -- Simple Stock and Assets mamagement tool
 * By: Cyril Lacoux <clacoux@ifeelgood.org>
 *
 * Copyright (C) 2018-2019 Cyril Lacoux
 * https://gitlab.com/yack/easystock
 *
 * This file is part of EasyStock.
 *
 * EasyStock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * EasyStock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Global initialization
 */
$(document).ready(function() {
    // Stock chart
    $('div.chart').each(function(index, e) {
        var labels = Array();
        var values = Array();

        $('tr.chart-data', e).each(function(i, tr) {
            labels.push($(tr).data('stamp'));
            values.push($(tr).data('value'));
        });

        // Reorder data
        labels.reverse();
        values.reverse();

        var ctx = $('canvas', e);

        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    borderColor: '#3c8dbc',
                    borderWidth: 1,
                    data: values,
                    fill: false,
                    steppedLine: true
                }]
            },
            options: {
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        type: 'time',
                        display: true,
                        time: {
                            max: moment(),
                            parser: 'L LT',
                            round: 'day'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0,
                            beginAtZero: true,
                            callback: function(value, index, values) {
                                // Make sure a tick is not a fractional value
                                if (Math.floor(value) === value) {
                                    return value;
                                }
                            }
                        }
                    }]
                },
            }
        });

        $(e).data('chart', chart);
    });

    // Asset modal
    initAreaAutocompleter('#asset-area', {minimumInputLength: 0});

    $('a.new-asset').on('click', function(e) {
        e.preventDefault();

        var $modal = $('#asset-modal');

        // Show it!
        $modal.modal();
    });

    // Stock modal
    $('#stock-value').TouchSpin({
        min: 0,
        max: 100000
    });

    initAreaAutocompleter('#stock-area', {minimumInputLength: 0});

    $('a.new-stock').on('click', function(e) {
        e.preventDefault();

        var action = $(this).data('action');
        var $modal = $('#stock-modal');
        var title;

        // Set title
        if (action == 'in') {
            title = _('Record input operation');
        }
        else if (action == 'out') {
            title = _('Record output operation');
        }
        else if (action == 'stock') {
            title = _('Record stocktake operation');
        }
        else {
            alertify.notify(_('Invalid action'), 'error');
            return;
        }

        // Set action
        $('input[name="action"]', $modal).val(action);

        $('.modal-title', $modal).html(title);

        // Show it!
        $modal.modal();
    });
});
