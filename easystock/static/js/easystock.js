/*
 * EasyStock -- Simple Stock and Assets mamagement tool
 * By: Cyril Lacoux <clacoux@ifeelgood.org>
 *
 * Copyright (C) 2018-2019 Cyril Lacoux
 * https://gitlab.com/yack/easystock
 *
 * This file is part of EasyStock.
 *
 * EasyStock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * EasyStock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Global variables
 */
var formValues = {};
var select2Options = {
    adaptDropdownCssClass: function(klass) { if (klass == 'no-search') { return klass; } },
    allowClear: true,
    placeholder: '--',
    theme: 'bootstrap'
};

var language = null;
var messages = null;
var translations;


/*
 * I18n
 */
function _(string) {
    var translated = translations.gettext(string);
    return (translated !== '') ? translated : string;
}


function ngettext(singular, plural, n) {
    return translations.ngettext(singular, plural, n);
}


/*
 * API error callback
 */
function apiErrorCallback(jqXHR, textStatus, errorThrown) {
    if (jqXHR.status >= 500) {
        alertify.notify(_('Failed to communicate with remote server'), 'error');
        return;
    }

    if (jqXHR.responseJSON && jqXHR.responseJSON.message) {
        alertify.notify(jqXHR.responseJSON.message, 'error');
    }
    else if (jqXHR.status == 403) {
        alertify.notify(_('Access denied'), 'error');
    }
    else {
        alertify.notify(_('An unexplained error occured'), 'error');
    }
}


/*
 * API links
 *
 * An API link is a link written like this:
 * <a href="/some/where" data-method="post" date-foo="bar">Do something using AJAX call to API</a>
 *
 * In this case, method employed to call the API will be `POST` and `{foo: "bar"}` will be sent to the service.
 * Default method is `GET`

 * On success the page is reloaded
 * On error return message is displayed
 */
function apiLinkSetup(context) {
    $('a.api-link', context).on('click', function(e) {
        e.preventDefault();

        var element = $(this);

        var url = $(this).attr('href');
        var data = $.extend({}, $(this).data());
        var method;
        var $modal;

        function callAPI() {
            element.addClass('disabled');

            if ($modal) {
                $modal.modal();
            }

            $.ajax(url, {
                data: data,
                dataType: 'json',
                type: method,
                success: function(data) {
                    if (data && data.message) {
                        alertify.notify(data.message, data.result ? 'success' : 'error');
                    }
                    if (data) {
                        // Reload the page
                        if (data.reload) {
                            document.location.reload();
                        }
                        else if (data.location) {
                            document.location = data.location;
                        }
                    }
                },
                error: apiErrorCallback,
                complete: function() {
                    if ($modal) {
                        $modal.modal('hide');
                    }
                    element.removeClass('disabled');
                }
            });
        }

        if (data.method) {
            method = data.method.toUpperCase();
            delete data.method;
        }
        else {
            method = 'GET';
        }

        if (data.modal) {
            $modal = $(data.modal);
        }

        if (data.confirmMessage) {
            var confirmMessage = data.confirmMessage;
            delete data.confirmMessage;

            alertify.confirm(confirmMessage, function() { callAPI(); }).setting('title', _('Confirmation is required'));
        }
        else {
            callAPI();
        }
    });
}


/*
 * Form reset button
 */
function formResetSetup() {
    $('form .form-reset, form .form-erase').on('click', function(e) {
        var erase = $(this).hasClass('form-erase');
        var $form = $(this).parents('form:first');
        var formName = $form.attr('name');
        var values = formValues[formName];
        var inputName;

        e.preventDefault();

        if (!values) {
            return;
        }

        for (inputName in values) {
            var field = $form.find('[name="' + inputName + '"]');
            var value = erase ? '' : values[inputName];

            if (field.is('input[type="checkbox"]')) {
                field.prop('checked', value ? true : false);
            }
            else {
                field.val(value).trigger('change');
            }
        }
    });
}


/*
 * Partial block setup
 */
function partialBlockSetup(context) {
    $('a.partial-link', context).css({'outline': 'none'}).on('click', function(e) {
        e.preventDefault();

        var btn = $(this);
        var url = btn.attr('href');

        var target;
        var target_selector = btn.data('target');
        if (target_selector) {
            target = $(target_selector);
        }
        else {
            target = btn.parents('.partial-block:first');
        }

        var partialKey = target.data('partial-key');
        var partialMethod = target.data('partial-method');
        var data = {};

        data[partialKey] = true;

        if (partialMethod == 'append') {
            $(this).addClass('loading');
        }
        else {
            target.append('<div class="overlay"></div><div class="loading-img"></div>');
        }

        $.ajax({
            url: url,
            data: data,
            type: 'get',
        }).done(function(content) {
            if (partialMethod == 'append') {
                target.append(content);
            }
            else {
                target.html(content);
            }
            partialBlockSetup(target);
            target.trigger('load');

            if (partialMethod == 'append') {
                if (content) {
                    btn.removeClass('loading');
                }
                else {
                    btn.remove();
                }
            }
            else {
                // Scroll to target, offset is due to navbar
                $.scrollTo(target, 400, {offset: -70});
            }
        }).fail(function() {
            alertify.notify(_('Failed to communicate with remote server'), 'error');
        });
    });
}


/*
 * API form setup
 */
function apiFormSetup(context) {
    $('form.api-form', context).submit(function(e) {
        var form = $(this);
        var data = form.serializeArray();
        var method = form.attr('method') || 'post';
        var url = form.attr('action');

        e.preventDefault();

        // Remove error containers
        form.find('div.alert.input-error').remove();

        $.ajax({
            url: url,
            type: method,
            data: data,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                if (data && data.message) {
                    alertify.notify(data.message, data.result ? 'success' : 'error');
                }
                if (data && data.result) {
                    // Reload the page
                    if (data.reload) {
                        document.location.reload();
                    }
                    else if (data.location) {
                        document.location = data.location;
                    }
                } else {
                    if (data.errors) {
                        var field, fieldName, errorContainer;
                        for (fieldName in data.errors) {
                            // Display error after field input
                            errorContainer = $('<div class="alert alert-danger input-error">' + data.errors[fieldName] + '</div>');
                            errorContainer.append('<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>');

                            field = form.find('[name="' + fieldName + '"]');
                            field.after(errorContainer);
                        }
                    }
                }
            },
            error: apiErrorCallback
        });
    });
}


/*
 * Nav tabs setup
 */
function navTabsSetup() {
    var $navtabs = $('#main-tab');

    if ($navtabs.length === 0) {
        return;
    }

    /* Bookmarkable tabs */
    var anchor = document.location.hash;
    if (anchor) {
        $('a[href=' + anchor + ']', $navtabs).tab('show');
    }

    $('a[data-toggle=tab]', $navtabs).on('click', function(event) {
        document.location.hash = $(this).attr('href');
    });
}


/*
 * Global initialization
 */
$(document).ready(function() {
    // Load Gettext translations
    var catalog = window['BABEL_TO_LOAD_' + language] || {};
    translations = babel.Translations.load(catalog).install();

    /* Nav tabs */
    navTabsSetup();

    // Required fields
    $('.form-required').find('label:not(.checkbox-label)').after('<span class="text text-red">*</span>');

    // AlertifyJS default settings
    alertify.defaults = {
        // dialogs defaults
        modal: true,
        movable: true,
        resizable: true,
        closable: true,
        maximizable: true,
        pinnable: true,
        pinned: true,
        padding: true,
        overflow: true,
        maintainFocus: true,
        transition :'fade',

        // notifier defaults
        notifier: {
            // auto-dismiss wait time (in seconds)
            delay: 5,
            // default position
            position: 'bottom-right'
        },

        // language resources
        glossary: {
            // ok button text
            ok: 'OK',
            // cancel button text
            cancel: _('Cancel')
        },
        theme: {
            // class name attached to prompt dialog input textbox.
            input: 'ajs-input form-control',
            // class name attached to ok button
            ok: 'ajs-ok btn btn-primary',
            // class name attached to cancel button
            cancel: 'ajs-cancel btn btn-default'
        }
    };

    /* Styling selects */
    if ($.fn.select2) {
        $('select.selectpicker').select2(select2Options);
    }

    /* Date picker */
    if ($.fn.datetimepicker) {
        $('.date.datepicker').datetimepicker({
            locale: language,
            format: 'L'
        });
        $('.date.datetimepicker').datetimepicker({
            locale: language,
            format: 'L LT'
        });
    }

    /* Form reset button */
    formResetSetup();

    /* Handle «partial» links */
    partialBlockSetup();
    $('a.partial-loadmore').appear().on('appear', function(e) { $(this).click(); });

    /* Handle «API» links */
    apiLinkSetup();
    $('div.partial-block').on('load', function(e) { apiLinkSetup(this); });

    /* API forms */
    apiFormSetup();
    $('div.partial-block').on('load', function(e) { apiFormSetup(this); });

    /* Display flash messages using alertify.js */
    if (messages) {
        for (var i=0; i<messages.length; i++) {
            var msg = messages[i];
            alertify.notify(msg.message, msg.type, msg.wait);
        }
    }
});
