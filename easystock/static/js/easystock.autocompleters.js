/*
 * EasyStock -- Simple Stock and Assets mamagement tool
 * By: Cyril Lacoux <clacoux@ifeelgood.org>
 *
 * Copyright (C) 2018-2019 Cyril Lacoux
 * https://gitlab.com/yack/easystock
 *
 * This file is part of EasyStock.
 *
 * EasyStock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * EasyStock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var defaultAutocompletersOptions = {
    context: null,
    quietMillis: 250,
    minimumInputLength: 2,
    multiple: false,
    pageSize: 10
};


/*
 * Initialization
 */
function initAutocompleter(selector, options) {
    options = $.extend({}, defaultAutocompletersOptions, options);

    var $select = $(selector, options.context);
    var apiUrl = $select.data('apiUrl');
    var selectedIds = $select.data('value') || [];

    function init(initial_data) {
        $select.select2({
            allowClear: options.allowClear || select2Options.allowClear,
            placeholder: $select.attr('placeholder') || options.placeholder || select2Options.placeholder,
            theme: 'bootstrap',

            minimumInputLength: options.minimumInputLength,
            multiple: options.multiple,

            data: initial_data,

            templateResult: options.templateResult,
            templateSelection: options.templateSelection,

            ajax: {
                url: apiUrl,
                data: function(params) {
                    var data = {term: params.term};

                    // Pagination
                    data[options.pagerKey + '.limit'] = options.pageSize;
                    data[options.pagerKey + '.page'] = params.page || 1;

                    if (options.excludedIds) {
                        var excludedIds;
                        if (typeof options.excludedIds === 'function') {
                            excludedIds = options.excludedIds();
                        }
                        else {
                            excludedIds = options.excludedIds;
                        }
                        data.excluded_ids = excludedIds.join(',');
                    }

                    return data;
                },
                dataType: 'json',
                delay: 250,
                processResults: function(data, params) {
                    if (!data.result) {
                        var message = data.message ? data.message : _('Failed to communicate with remote server');
                        alertify.notify(message, 'error');
                        return;
                    }

                    var more = (data.data.page * data.data.limit) < data.data.item_count;

                    var results = [];
                    $.each(data.data.items, function(i, item) {
                        results.push(item);
                    });

                    return {
                        results: results,
                        pagination: {
                            more: more
                        }
                    };
                }
            }
        });
    }

    if (selectedIds.length > 0) {
        $.ajax({
            url: apiUrl,
            data: {
                page_size: 0,
                selected_ids: selectedIds.join(',')
            },
            dataType: 'json'
        })
        .done(function(data) {
            if (!data.result) {
                var message = data.message ? data.message : _('Failed to communicate with remote server');
                alertify.notify(message, 'error');
                return;
            }

            var initial_data = [];

            $.each(data.data.items, function(i, item) {
                item.selected = true;
                initial_data.push(item);
            });

            init(initial_data);
        })
        .fail(apiErrorCallback);
    }
    else {
        init();
    }
}


/*
 * Areas
 */
function initAreaAutocompleter(selector, options) {
    options = $.extend({}, options);

    options.templateResult = function(item) {
        if (!item.id) {
            return item.label;
        }

        var html = babel.format(
            _('<span>%s<br><span class="text-muted">%s, floor #%s</span></span>'),
            item.room,
            item.building,
            item.floor
        );
        return $(html);
    };

    options.templateSelection = function(item) {
        if (!item.id) {
            return item.label;
        }

        var str = babel.format(_('%s (%s, floor #%s)'), item.room, item.building, item.floor);
        return str;
    };

    options.pagerKey = 'areas';

    initAutocompleter(selector, options);
}


/*
 * Products
 */
function initProductAutocompleter(selector, options) {
    options = $.extend({}, options);

    options.templateResult = function(item) {
        if (!item.id) {
            return item.label;
        }

        var html = babel.format(
            '<span>[<tt>%s</tt>] %s<span class="pull-right">[%s]</span><br><span class="text-muted">%s / %s</span></span>',
            item.code,
            item.label,
            item.category,
            item.brand ? item.brand : '--',
            item.model ? item.model : '--'
        );
        return $(html);
    };

    options.templateSelection = function(item) {
        if (!item.id) {
            return item.label;
        }

        var str = babel.format(
            '[%s] %s / %s / %s / %s',
            item.code,
            item.label,
            item.category,
            item.brand ? item.brand : '--',
            item.model ? item.model : '--'
        );
        return str;
    };

    options.pagerKey = 'products';

    initAutocompleter(selector, options);
}


/*
 * Sites
 */
function initSiteAutocompleter(selector, options) {
    options = $.extend({}, options);

    options.templateResult = function(item) {
        if (!item.id) {
            return item.label;
        }

        return item.name;
    };

    options.templateSelection = function(item) {
        if (!item.id) {
            return item.label;
        }

        return item.name;
    };

    options.pagerKey = 'sites';

    initAutocompleter(selector, options);
}
