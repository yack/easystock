/*
 * EasyStock -- Simple Stock and Assets mamagement tool
 * By: Cyril Lacoux <clacoux@ifeelgood.org>
 *
 * Copyright (C) 2018-2019 Cyril Lacoux
 * https://gitlab.com/yack/easystock
 *
 * This file is part of EasyStock.
 *
 * EasyStock is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * EasyStock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Default form values
 */
formValues.search = {
     'brand': '',
     'category': '',
     'label': '',
     'model': '',
     'status': ''
};
