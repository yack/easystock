## -*- coding: utf-8 -*-
<%!
from datetime import date
import json

from easystock.models import User

year = date.today().year
skin = 'blue-light'
%>\
<%def name="head()">
</%def>\
<%
active_section = None
if request.matched_route is not None:
    route_name = request.matched_route.name
    if route_name.startswith('assets.'):
        active_section = 'assets'

    elif route_name == 'dashboard':
        active_section = 'dashboard'

    elif route_name == 'inventory':
        active_section = 'inventory'

    elif route_name.startswith('products.'):
        active_section = 'products'

    elif route_name.startswith('sites.'):
        active_section = 'sites'

    elif route_name.startswith('stocks.'):
        active_section = 'stocks'

    elif route_name.startswith('users.') or (route_name == 'imports.upload' and target == 'users'):
        active_section = 'users'
%>\
<!DOCTYPE html>
<html lang="${request.locale_name}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="pyramid web application">
    <meta name="author" content="Pylons Project">

    <title>EasyStock - ${title}${'- {0}'.format(subtitle) if subtitle else ''}</title>

    <!-- Babel -->
    <script src="${request.static_path('easystock:static/lib/babel.js', _query=dict(version=app_version))}"></script>

    <!-- jQuery -->
    <script src="${request.static_path('easystock:static/lib/jquery-3.3.1.min.js')}"></script>

    <!-- jQuery.appear -->
    <script src="${request.static_path('easystock:static/lib/jquery.appear-0.3.6.js')}"></script>

    <!-- jQuery.scrollTo -->
    <script src="${request.static_path('easystock:static/lib/jquery.scrollTo-1.4.14.min.js')}"></script>

    <!-- Bootstrap -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-3.3.7/css/bootstrap.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-3.3.7/js/bootstrap.min.js')}"></script>

    <!-- Font Awesome -->
    <link href="${request.static_path('easystock:static/font/fontawesome-5.0.6/css/fontawesome-all.min.css')}" rel="stylesheet" />

    <!-- AlertifyJS -->
    <link href="${request.static_path('easystock:static/lib/alertify-js-1.11.0/css/alertify.min.css')}" rel="stylesheet"/>
    <link href="${request.static_path('easystock:static/lib/alertify-js-1.11.0/css/themes/bootstrap.min.css')}" rel="stylesheet"/>
    <script src="${request.static_path('easystock:static/lib/alertify-js-1.11.0/alertify.min.js')}"></script>

    <!-- Select2 -->
    <link href="${request.static_path('easystock:static/lib/select2-4.0.7/css/select2.min.css')}" rel="stylesheet" />
    <link href="${request.static_path('easystock:static/lib/select2-4.0.7/css/select2-bootstrap.min.css')}" rel="stylesheet" />
    <script src="${request.static_path('easystock:static/lib/select2-4.0.7/js/select2.full.min.js')}"></script>
    <script src="${request.static_path('easystock:static/lib/select2-4.0.7/js/i18n/{0}.js'.format(localizer.locale_name))}"></script>

    <!-- Pretty checkbox -->
    <link href="${request.static_path('easystock:static/lib/pretty-checkbox-3.0.3.min.css')}" rel="stylesheet" />

    <!-- AdminLTE -->
    <script src="${request.static_path('easystock:static/lib/admin-lte-2.4.3/js/adminlte.min.js')}"></script>
    <script src="${request.static_path('easystock:static/lib/jquery.slimScroll-1.3.8/jquery.slimscroll.min.js')}"></script>
    <link href="${request.static_path('easystock:static/lib/admin-lte-2.4.3/css/AdminLTE.min.css')}" rel="stylesheet" />
    <link href="${request.static_path('easystock:static/lib/admin-lte-2.4.3/css/skins/skin-{0}.min.css'.format(skin))}" rel="stylesheet" />

    <!-- i18n -->
    % if localizer.locale_name != 'en':
    <script src="${request.static_path('easystock:static/translations/{0}.js'.format(localizer.locale_name), _query=dict(version=app_version))}"></script>
    % endif

    <!-- Custom files -->
    <link href="${request.static_url('easystock:static/css/easystock.css')}" rel="stylesheet">
    <script src="${request.static_url('easystock:static/js/easystock.js')}"></script>

    <!-- Global variables -->
    <script>
var language = "${localizer.locale_name}";
var messages = ${json.dumps(request.session.pop_flash()) | n};
    </script>

${self.head()}
</head>

<body class="hold-transition skin-${skin} fixed">
<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="${request.route_path('index')}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Easy</b>Stock</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <i class="fa fa-bars"> </i>
                <span class="sr-only">Toggle navigation</span>
            </a>
% if request.authenticated_user is not None:
            <div class="navbar-quick-actions">
    % if active_section == 'users':
        % if request.has_permission('users.create'):
                <a href="${request.route_path('users.create')}" title="${_('Create new user')}"><i class="far fa-plus-square fa-fw"> </i> <span>${_('New user')}</span></a>
        % endif
        % if request.has_permission('users.import'):
                <a href="${request.route_path('imports.upload', target='users')}" title="${_('Import user(s)')}"><i class="fa fa-upload fa-fw"> </i> <span>${_('Import user(s)')}</span></a>
        % endif
    % elif active_section == 'sites':
        % if request.has_permission('sites.create'):
                <a href="${request.route_path('sites.create')}" title="${_('Create new site')}"><i class="far fa-plus-square fa-fw"> </i> <span>${_('New site')}</span></a>
        % endif
    % elif active_section == 'products':
        % if request.has_permission('products.create'):
                <a href="${request.route_path('products.create')}" title="${_('Create new product')}"><i class="far fa-plus-square fa-fw"> </i> <span>${_('New product')}</span></a>
        % endif
    % endif
            </div>

            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="${request.static_path('easystock:static/img/user.svg')}" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">${request.authenticated_user.fullname}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="${request.static_path('easystock:static/img/user.svg')}" class="img-circle" alt="User Image">

                                <p>
                                    ${request.authenticated_user.fullname} - ${request.authenticated_user.site.name}
    % if request.authenticated_user.profiles:
                                    <small>${', '.join(_(User.PROFILES[profile]) for profile in request.authenticated_user.profiles)}</small>
    % endif
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="${request.route_path('users.profile')}" class="btn btn-default btn-flat">${_('Profile')}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="${request.route_path('auth.sign-out')}" class="btn btn-default btn-flat">${_('Sign out')}</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-custom-menu -->
% endif
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
% if request.authenticated_user is not None:
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="${request.static_path('easystock:static/img/user.svg')}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>${request.authenticated_user.fullname}</p>
                    <small>${request.authenticated_user.site.name}</small>
                </div>
            </div>

    % if request.has_permission('stocks.create') or request.has_permission('assets.create') or request.has_permission('assets.update'):
            <!-- search form (Optional) -->
            <form action="${request.route_path('inventory')}" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="barcode" class="form-control" placeholder="${_('EAN 13 code or asset number')}">
                    <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </span>
                </div>
            </form><!-- /.sidebar-form -->
    % endif

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li${' class="active"' if active_section == 'dashboard' else '' | n}>
                    <a href="${request.route_path('dashboard')}"><i class="fas fa-fw fa-tachometer-alt"></i> <span>${_('Dashboard')}</span></a>
                </li>
    % if request.has_permission('stocks.create') or request.has_permission('assets.create') or request.has_permission('assets.update'):
                <li${' class="active"' if active_section == 'inventory' else '' | n}>
                    <a href="${request.route_path('inventory')}"><i class="fas fa-fw fa-barcode"></i> <span>${_('Inventory')}</span></a>
                </li>
    % endif
    % if request.has_permission('products.search'):
                <li${' class="active"' if active_section == 'products' else '' | n}>
                    <a href="${request.route_path('products.search')}"><i class="fa fa-fw fa-list"></i> <span>${_('Products')}</span></a>
                </li>
    % endif
    % if request.has_permission('stocks.search'):
                <li${' class="active"' if active_section == 'stocks' else '' | n}>
                    <a href="${request.route_path('stocks.search')}"><i class="fa fa-fw fa-edit"></i> <span>${_('Stock')}</span></a>
                </li>
    % endif
    % if request.has_permission('assets.search'):
                <li${' class="active"' if active_section == 'assets' else '' | n}>
                    <a href="${request.route_path('assets.search')}"><i class="fa fa-fw fa-cubes"></i> <span>${_('Assets')}</span></a>
                </li>
    % endif
    % if request.has_permission('sites.search'):
                <li${' class="active"' if active_section == 'sites' else '' | n}>
                    <a href="${request.route_path('sites.search')}"><i class="fa fa-fw fa-building"></i> <span>${_('Sites')}</span></a>
                </li>
    % endif
    % if request.has_permission('users.search'):
                <li${' class="active"' if  active_section == 'users' else '' | n}>
                    <a href="${request.route_path('users.search')}"><i class="fa fa-fw fa-user"></i> <span>${_('Users')}</span></a>
                </li>
    % endif
            </ul><!-- /.sidebar-menu -->
% endif
        </section><!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ${title}
% if subtitle:
                <small>${subtitle}</small>
% endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="${request.route_path('dashboard')}"><i class="fas fa-tachometer-alt"></i> ${_('EasyStock')}</a></li>
% for name, url in breadcrumb:
                <li${' class="active"' if url is None else '' | n}>
    % if url is not None:
                    <a href="${url}">${name}</a>
    % else:
                    ${name}
    % endif
                </li>
% endfor
            </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
${self.body()}
        </section><!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Design by: <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; ${year} <a href="http://ifeelgood.org">Cyril Lacoux</a>.</strong> All rights reserved.
    </footer>
</div><!-- ./wrapper -->

</body>
</html>
