## -*- coding: utf-8 -*-
<%!
from l18n import tz_cities
%>\
<%namespace name="paginate" file="/paginate.mako"/>
<% pager = request.pagers['users'] %>
% if pager.item_count:
<div class="box box-solid">
    <div class="box-header">
        <i class="fa fa-list"></i>
        <h3 class="box-title">
    % if pager.page_count > 1:
            ${_('from {0} to {1} on {2} users').format(pager.first_item, pager.last_item, pager.item_count)}
    % else:
            ${ungettext('{0} user', '{0} users', pager.item_count).format(pager.item_count)}
    % endif
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table tale-condensed table-hover table-striped">
                <thead>
                    <tr>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='username', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('username')}"></span>
                                ${_('Username')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='fullname', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('fullname')}"></span>
                                ${_('Fullname')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='email', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('email')}"></span>
                                ${_('Email')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='site', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('site')}"></span>
                                ${_('Site')}
                            </a>
                        </th>
                        <th>${_('Timezone')}</th>
                        <th>${_('Profiles')}</th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='creation_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('creation_date')}"></span>
                                ${_('Creation date')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='modification_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('modification_date')}"></span>
                                ${_('Modification date')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='status', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('status')}"></span>
                                ${_('Status')}
                            </a>
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
    % for user in pager:
                    <tr>
        % if request.has_permission('users.modify'):
                        <td><a href="${request.route_path('users.modify', user=user.id)}" title="${_('Modify user «{0}»').format(user.fullname)}"><b><tt>${user.username}</tt></b></a></td>
        % else:
                        <td><b><tt>${user.username}</tt></b></td>
        % endif

                        <td>${user.fullname}</td>
                        <td>${user.email or ''}</td>
                        <td>${user.site.name}</td>
                        <td>${_(tz_cities.get(user.timezone, user.timezone), domain='l18n') if user.timezone else ''}</td>
                        <td>${', '.join(_(user.PROFILES[profile]) for profile in user.profiles) if user.profiles else ''}</td>
                        <td>${format_datetime(utctolocal(user.creation_date), date_format='short')}</td>
                        <td>${format_datetime(utctolocal(user.modification_date), date_format='short')}</td>
                        <td>${_(user.STATUSES[user.status])}</td>
                        <td>
        % if request.has_permission('users.delete') and user != request.authenticated_user:
                            <a href="${request.route_path('api.users.delete', user=user.id)}" class="api-link btn btn-xs btn-danger" data-method="delete" data-confirm-message="${_('Do you really want to delete user «{0}» ?').format(user.fullname)}"><i class="fa fa-trash"> </i> ${_('Delete')}</a>
        % endif
                        </td>
                    </tr>
    % endfor
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer text-right">
${paginate.render_pages(pager, extra_class='no-margin pagination-sm')}
${paginate.render_limit(pager, extra_class='no-margin pagination-sm')}
    </div><!-- /.box-footer -->
</div><!-- /.box -->
% else:
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info">
            <h4><i class="fa fa-info-circle"></i> ${_('No result')}</h4>
            ${_('No user match search criteria.')}
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
% endif
