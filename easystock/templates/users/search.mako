## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.autocompleters.js', _query=dict(version=app_version))}" type="text/javascript"></script>
    <script src="${request.static_path('easystock:static/js/easystock.users.search.js', _query=dict(version=app_version))}" type="text/javascript"></script>
</%def>

<!-- Search criteria -->
<div class="box box-solid">
    <div class="box-header">
        <i class="fa fa-search"></i>
        <h3 class="box-title">${_('Search criteria')}</h3>
    </div><!-- /.box-header -->
    <%form:form name="search" role="form" method="get">
        <div class="box-body">

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="firstname">${_('Firstname')}</label>
                    <%form:text type="text" class_="form-control input-sm" id="firstname" name="firstname" />
                </div>

                <div class="form-group col-md-4">
                    <label for="lastname">${_('Lastname')}</label>
                    <%form:text type="text" class_="form-control input-sm" id="lastname" name="lastname" />
                </div>

                <div class="form-group col-md-4">
                    <label for="email">${_('Email')}</label>
                    <%form:text type="text" class_="form-control input-sm" id="email" name="email" />
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="username">${_('Username')}</label>
                    <%form:text type="text" class_="form-control input-sm" id="username" name="username" />
                </div>

                <div class="form-group col-md-3">
                    <label for="site">${_('Site')}</label>
                    <%form:autocomplete class_="form-control input-sm" id="site" name="site" data_api_url="${request.route_path('api.sites.autocomplete')}" placeholder="--" />
                </div>
                <div class="form-group col-md-3">
                    <label for="profile">${_('Profile')}</label>
                    <%form:select placeholder="--" class_="form-control selectpicker input-sm no-search" id="profile" name="profile">
                        <%form:option value=""></%form:option>
% for value, name in profiles:
                        <%form:option value="${value}">${_(name)}</%form:option>
% endfor
                    </%form:select>
                </div>

                <div class="form-group col-md-3">
                    <label for="enabled">${_('Status')}</label>
                    <%form:select placeholder="--" class_="form-control selectpicker input-sm no-search" id="status" name="status">
                        <%form:option value=""></%form:option>
% for value, name in statuses:
                        <%form:option value="${value}">${_(name)}</%form:option>
% endfor
                    </%form:select>
                </div>
            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <div class="pull-right">
                <div class="btn-group">
                    <a class="btn btn-default form-reset">${_('Reset')}</a>
                    <button class="btn btn-primary" type="submit" name="search" value="search">${_('Search')}</button>
                </div>
                &nbsp;
                <div class="btn-group">
                    <button class="btn btn-default" type="submit" name="format" value="csv" title="${_('Export using CSV format')}">
                        <i class="far fa-file-alt icon"> </i>
                        <span class="sr-only">${_('CSV')}</span>
                    </button>
                    <button class="btn btn-default" type="submit" name="format" value="xlsx" title="${_('Export using XLSX format')}">
                        <i class="far fa-file-excel icon"> </i>
                        <span class="sr-only">${_('XLSX')}</span>
                    </button>
                </div>
            </div>
        </div><!-- /.box-footer -->
    </%form:form>
</div><!-- /.box -->


<!-- Search result -->
<div id="users-list" class="partial-block" data-partial-key="users.partial">
<%include file="list.mako" />
</div><!-- /.partial-block -->
