## -*- coding: utf-8 -*-
<%!
from l18n import tz_cities
%>\
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%form:form name="profile" method="post" role="form">
    <div class="box box-solid">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">${subtitle}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="form-group col-md-4 form-required">
                    <label for="username">${_('Username')}</label>
                    <%form:text id="username" name="username" class_="form-control input-sm" />
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="firstname">${_('Firstname')}</label>
                    <%form:text id="firstname" name="firstname" class_="form-control input-sm" />
                </div><!-- /.col -->

                <div class="form-group col-md-4">
                    <label for="lastname">${_('Lastname')}</label>
                    <%form:text id="lastname" name="lastname" class_="form-control input-sm" />
                </div><!-- /.col -->

                <div class="form-group col-md-4">
                    <label for="email">${_('Email')}</label>
                    <%form:text id="email" name="email" class_="form-control input-sm" />
                </div>
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="timezone">${_('Timezone')}</label>
                    <%form:select placeholder="--" class_="form-control input-sm selectpicker" id="timezone" name="timezone">
                        <%form:option value=""></%form:option>
% for value, name in timezones:
                        <%form:option value="${value}">${_(tz_cities.get(name, name), domain='l18n')}</%form:option>
% endfor
                    </%form:select>
                </div><!-- /.col -->

% if set_password:
                <div class="form-group col-md-4">
                    <label for="password">${_('Password')}</label>
                    <%form:password id="password" name="password" class_="form-control input-sm" autocomplete="off" />
                </div><!-- /.col -->

                <div class="form-group col-md-4">
                    <label for="password_confirm">${_('Password confirmation')}</label>
                    <%form:password id="password_confirm" name="password_confirm" class_="form-control input-sm" autocomplete="off" />
                </div><!-- /.col -->
% endif
            </div><!-- /.row -->
        </div><!-- /.box-body -->

        <div class="box-footer text-right">
            <button type="submit" class="btn btn-primary">${_('Save')}</button>
            <a href="${cancel_link}" class="btn btn-default">${_('Cancel')}</a>
        </div><!-- /.box-footer -->
    </div><!-- /.box -->
</%form:form>
