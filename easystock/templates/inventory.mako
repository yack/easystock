## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>\
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- TouchSpin -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-touchspin-3.1.2/jquery.bootstrap-touchspin.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-touchspin-3.1.2/jquery.bootstrap-touchspin.min.js')}"></script>

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.autocompleters.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('easystock:static/js/easystock.inventory.js', _query=dict(version=app_version))}"></script>
</%def>
<%
cancel_link = request.current_route_path(_query=None)

step = 2 if asset or product else 1
%>

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-pills nav-justified thumbnail setup-panel">
            <li${' class="active" ' if step == 1 else '' | n}>
                <a href="${cancel_link}">
                    <h4 class="list-group-item-heading">${_('Step 1')}</h4>
                    <p class="list-group-item-text">${_('Enter an EAN 13 code or inventory number')}</p>
                </a>
            </li>
            <li class="${'disabled' if step == 1 else 'active'}">
                <a href="javascript: void(0);">
                    <h4 class="list-group-item-heading">${_('Step 2')}</h4>
                    <p class="list-group-item-text">
% if asset:
                        ${_('Please fill asset information in the form below')}
% elif product:
                        ${_('Please fill stock operation in the form below')}
% else:
                        ...
% endif
                    </p>
                </a>
            </li>
        </ul>
    </div>
</div>

% if step == 1:
## First Step
<div id="inventory-scanner">
    % if barcode:
    <div class="alert alert-danger">
        <h4><i class="fas fa-exclamation-triangle icon"> </i>${_('Invalid code')}</h4>
        <p>${_('No product or inventory matched entered code.')}</p>
    </div>
    % endif
    <%form:form name="search" method="get">
        <div class="form-group">
            <div class="input-group">
                <%form:text name="barcode" id="barcode" class_="form-control input-lg" placeholder="${_('EAN 13 code or asset number')}"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"> </i> <span class="sr-only">${_('Search')}</span></button>
                </div>
            </div>
        </div><!-- /.form-group -->
    </%form:form>
</div><!-- /#inventory-scanner -->

## Asset
% elif asset:
<div class="row">
    <div class="col-md-12">
        <form action="${request.route_path('api.assets.update', asset=asset.id)}" name="asset" method="post" class="api-form">
            <input type="hidden" name="author" value="${request.authenticated_user.id}" />
            <input type="hidden" name="product" value="${product.id}" />
            <div class="box box-solid box-inventory">
                <div class="box-header">
                    <h4 class="box-title">${'{0} - {1}'.format(asset.code, product.label)}<small> - ${_('Update')}</small></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <dl>
                                <dt>${_('Asset number')}<dt>
                                <dd>${asset.code}</dd>

                                <dt>${_('Label')}<dt>
                                <dd><a href="${request.route_path('products.visual', product=product.id)}" title="${_('View product «{0}»').format(product.label)}">${product.label}</a></dd>

                                <dt>${_('Category')}<dt>
                                <dd>${product.category}</dd>

                                <dt>${_('Brand')}<dt>
                                <dd>${product.brand or ''}</dd>

                                <dt>${_('Model')}<dt>
                                <dd>${product.model or ''}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="asset-area">${_('Area')}</label>
                                <select class="form-control input-lg" id="asset-area" name="area" data-api-url="${request.route_path('api.areas.autocomplete')}" placeholder="--" data-value="[${asset.area_id}]"></select>
                            </div>
                            <div class="form-group">
                                <label for="asset-serial-number">${_('S/N')}</label>
                                <input type="text" name="serial_number" id="asset-serial-number" class="form-control input-lg" value="${asset.serial_number or ''}" />
                            </div>
                            <div class="form-group">
                                <label for="asset-state">${_('State')}</label>
                                <select name="state" id="asset-state" class="form-control input-lg selectpicker no-search">
    % for value, name in states:
                                    <option value="${value}"${' selected="selected"' if value == asset.state else '' | n}>${_(name) | n}</option>
    % endfor
                                </select>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-3">
    % if request.has_permission('assets.delete'):
                            <a class="api-link btn btn-danger btn-block btn-lg btn-social" href="${request.route_path('api.assets.delete', asset=asset.id)}" data-method="delete" data-confirm-message="${_('Do you really want to delete asset «{0}» ?').format(asset.code)}" title="${_('Delete asset «{0}»').format(asset.code)}">
                                <i class="far fa-trash-alt fa-fw"> </i>
                                ${_('Delete')}
                            </a>
    % endif
                        </div><!-- /.col -->
                        <div class="col-md-3">
    % if request.has_permission('assets.visual'):
                            <a class="btn btn-primary btn-block btn-lg btn-social" href="${request.route_path('assets.visual', asset=asset.id)}" title="${_('View asset «{0}»').format(asset.code)}">
                                <i class="far fa-file-alt fa-fw"> </i>
                                ${_('View')}
                            </a>
    % endif
                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <a class="btn btn-default btn-block btn-lg btn-social" href="${cancel_link}">
                                <i class="fa fa-arrow-left"> </i>
                                ${_('Cancel')}
                            </a>
                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-success btn-block btn-lg btn-social" title="${_('Validate')}">
                                <i class="fa fa-check"> </i>
                                ${_('Validate')}
                            </button>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-footer -->
            </div><!-- /.box -->
        </form>
    </div><!-- /.col -->
</div><!-- /.row -->

## Product
% elif product:
<div class="row">
    <div class="col-md-12">
        <form action="${request.route_path('api.stocks.create')}" name="stock" method="put" class="api-form">
            <input type="hidden" name="author" value="${request.authenticated_user.id}" />
            <input type="hidden" name="product" value="${product.id}" />
            <div class="box box-solid box-inventory">
                <div class="box-header">
                    <h4 class="box-title">${product.label}<small> - ${_('Stock operation')}</small></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <dl>
                                <dt>${_('Label')}<dt>
                                <dd>${product.label}</dd>

                                <dt>${_('Category')}<dt>
                                <dd>${product.category}</dd>

                                <dt>${_('Brand')}<dt>
                                <dd>${product.brand or ''}</dd>

                                <dt>${_('Model')}<dt>
                                <dd>${product.model or ''}</dd>
                            </dl>
                        </div><!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="stock-action">${_('Action')}</label>
                                <select name="action" id="stock-action" class="form-control input-lg selectpicker no-search" placeholder="--">
                                    <option value="">--</option>
    % for value, name in actions:
                                    <option value="${value}">${_(name) | n}</option>
    % endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="stock-area">${_('Area')}</label>
                                <select class="form-control input-lg" id="stock-area" name="area" data-api-url="${request.route_path('api.areas.autocomplete')}" placeholder="--"></select>
                            </div>
                            <div class="form-group">
                                <label for="stock-value">${_('Quantity')}</label>
                                <input type="text" name="value" id="stock-value" class="form-control input-lg" />
                            </div>
                            <div class="form-group">
                                <label for="stock-message">${_('Message')}</label>
                                <textarea name="message" id="stock-message" class="form-control input-lg" rows="5"></textarea>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-3">
    % if request.has_permission('products.delete'):
                            <a class="api-link btn btn-danger btn-block btn-lg btn-social" href="${request.route_path('api.products.delete', product=product.id)}" data-method="delete" data-confirm-message="${_('Do you really want to delete product «{0}» ?').format(product.label)}" title="${_('Delete product «{0}»').format(product.label)}">
                                <i class="far fa-trash-alt fa-fw"> </i>
                                ${_('Delete')}
                            </a>
    % endif
                        </div><!-- /.col -->
                        <div class="col-md-3">
    % if request.has_permission('products.visual'):
                            <a class="btn btn-primary btn-block btn-lg btn-social" href="${request.route_path('products.visual', product=product.id)}" title="${_('View product «{0}»').format(product.label)}">
                                <i class="far fa-file-alt fa-fw"> </i>
                                ${_('View')}
                            </a>
    % endif
                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <a class="btn btn-default btn-block btn-lg btn-social" href="${cancel_link}">
                                <i class="fa fa-arrow-left"> </i>
                                ${_('Cancel')}
                            </a>
                        </div><!-- /.col -->
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-success btn-block btn-lg btn-social" title="${_('Validate')}">
                                <i class="fa fa-check"> </i>
                                ${_('Validate')}
                            </button>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-footer -->
            </div><!-- /.box -->
        </form>
    </div><!-- /.col -->
</div><!-- /.row -->
% endif
