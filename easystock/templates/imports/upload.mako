## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}
    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.imports.upload.js', _query=dict(version=app_version))}" type="text/javascript"></script>
</%def>

<%form:form name="upload" method="post" role="form" class_="form-horizontal" enctype="multipart/form-data">
    <div class="box box-solid">
        <div class="box-header">
            <h3 class="box-title">${subtitle}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row form-group form-required">
                <div class="col-md-3">
                    <label for="qfile">${_('File')}</label>
                </div><!-- /.row -->
                <div class="col-md-9">
                    <div class="input-group input-group-sm input-upload">
                        <div class="input-group-btn">
                            <label class="btn btn-default">
                                <span class="sr-only">${_('Browse...')}</span>
                                <i class="fa fa-upload"> </i>
                                <%form:upload id="qfile" name="qfile" class_="hidden" accept=".csv,.xlsx" />
                            </label>
                        </div>
                        <%form:text class_="form-control" disabled="disabled" name="filename" />
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row form-group">
                <div class="col-md-9 col-md-offset-3">
                    <div class="pretty p-curve p-icon">
                        <%form:checkbox id="create-only" name="create_only" />
                        <div class="state p-primary">
                            <i class="icon fa fa-check"></i>
                            <label for="create-only">${_('Only create missing items')}</label>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row form-group form-required">
                <div class="col-md-3">
                    <label for="sheet">${_('Sheet')}</label>
                </div><!-- /.row -->
                <div class="col-md-9">
                    <%form:text class_="form-control input-sm" id="sheet" name="sheet" />
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.box-body -->

        <div class="box-footer text-right">
            <button type="submit" class="btn btn-primary">${_('Import')}</button>
            <a href="${cancel_link}" class="btn btn-default">${_('Cancel')}</a>
        </div><!-- /.box-footer -->
    </div><!-- /.box -->
</%form:form>
