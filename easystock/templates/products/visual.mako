## -*- coding: utf-8 -*-
<%!
import datetime

import sqlalchemy as sa

from easystock.models import Area
from easystock.models import Asset
from easystock.models import DBSession
from easystock.models import Stock
%>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- TouchSpin -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-touchspin-3.1.2/jquery.bootstrap-touchspin.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-touchspin-3.1.2/jquery.bootstrap-touchspin.min.js')}"></script>

    <!-- Moment.js -->
    <script src="${request.static_path('easystock:static/lib/moment-2.14.1/moment.min.js')}"></script>
    % if localizer.locale_name != 'en':
    <script src="${request.static_path('easystock:static/lib/moment-2.14.1/locale/{0}.js'.format(localizer.locale_name))}"></script>
    % endif

    <!-- Bootstrap-datetimepicker -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-datetimepicker-4.17.45/css/bootstrap-datetimepicker.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-datetimepicker-4.17.45/js/bootstrap-datetimepicker.min.js')}"></script>

    <!-- ChartJs -->
    <script src="${request.static_path('easystock:static/lib/chartjs-2.7.1.min.js')}"></script>

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.autocompleters.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('easystock:static/js/easystock.products.visual.js', _query=dict(version=app_version))}"></script>
</%def>
<%def name="render_body(product, with_link=False)">
<div class="row">
    <div class="col-md-2">
        <div class="media"></div>
    </div><!--/.col -->

    <div class="col-md-5">
        <dl class="dl-horizontal">
            <dt>${_('Label')}</dt>
% if with_link:
            <dd><a href="${request.route_path('products.visual', product=product.id)}" title="${_('View product «{0}»').format(product.label)}">${product.label}</a></dd>
% else:
            <dd>${product.label}</dd>
% endif

            <dt>${_('Brand')}</dt>
            <dd>${product.brand or ''}</dd>

            <dt>${_('Model')}</dt>
            <dd>${product.model or ''}</dd>

            <dt>${_('Reference')}</dt>
            <dd>${product.reference or ''}</dd>

            <dt>${_('EAN 13')}</dt>
            <dd>${product.barcode or ''}</dd>
        </dl><!-- /.dl-horizontal -->
    </div><!--/.col -->

    <div class="col-md-5">
        <dl class="dl-horizontal">
            <dt>${_('Category')}</dt>
            <dd>${product.category or ''}</dd>

            <dt>${_('Creation date')}</dt>
            <dd>${format_datetime(utctolocal(product.creation_date), date_format='short')}</dd>

            <dt>${_('Modification date')}</dt>
            <dd>${format_datetime(utctolocal(product.modification_date), date_format='short')}</dd>

            <dt>${_('Status')}</dt>
            <dd>${_(product.STATUSES[product.status]).lower()}</dd>
        </dl><!-- /.dl-horizontal -->
    </div><!--/.col -->
</div><!--/.row -->
</%def>

## Product
<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">${subtitle}</h3>
        <div class="box-tools">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fas fa-cog"> </i>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    % if request.has_permission('stocks.create'):
                <li><a href="javascript: void(0);" class="new-stock" data-action="out">${_('Record output')}</a></li>
                <li><a href="javascript: void(0);" class="new-stock" data-action="in">${_('Record input')}</a></li>
                <li><a href="javascript: void(0);" class="new-stock" data-action="stock">${_('Set stock')}</a></li>
    % endif

    % if request.has_permission('assets.create'):
                <li role="separator" class="divider"></li>
                <li><a href="javascript: void(0);" class="new-asset">${_('New asset')}</a></li>
    % endif

    % if request.has_permission('products.create'):
                <li role="separator" class="divider"></li>
        % if request.has_permission('products.modify'):
                <li><a href="${request.route_path('products.modify', product=product.id)}" title="${_('Modify product «{0}»'.format(product.label))}">${_('Modify')}</a></li>
        % endif
                <li><a href="${request.route_path('products.duplicate', product=product.id)}" title="${_('Duplicate product «{0}»'.format(product.label))}">${_('Duplicate')}</a></li>
    % endif
    % if request.has_permission('products.delete'):
                <li role="separator" class="divider"></li>
                <li><a href="${request.route_path('api.products.delete', product=product.id)}" class="api-link" data-method="delete" data-confirm-message="${_('Do you really want to delete product «{0}» ?').format(product.label)}">${_('Delete')}</a></li>
    % endif
              </ul>
            </div>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->

    <div class="box-body">
${render_body(product)}
    </div><!--/.box-body -->
</div><!--/.box -->

## Assets
## TODO: pagination
<%
query = product.assets.join(Area).filter(Area.site == request.authenticated_user.site)
query = query.order_by(sa.desc(Asset.inventory_date))
count = query.count()
%>
% if count:
<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">${_('Assets')}</h3>
        <div class="pull-right"><span class="badge bg-blue">${count}</span></div>
    </div><!-- /.box-header -->

    <div class="box-body table-responsive">
        <table class="table table-condensed table-striped table-hover">
            <thead>
                <tr>
                    <th>${_('Number')}</th>
                    <th>${_('S/N')}</th>
                    <th>${_('Area')}</th>
                    <th>${_('Creation')}</th>
                    <th>${_('Modification')}</th>
                    <th>${_('Inventory')}</th>
                    <th>${_('State')}</th>
                </tr>
            </thead>
            <tbody>
    % for asset in query:
        <% area = asset.area %>
                <tr>
                    <td><a href="${request.route_path('assets.visual', asset=asset.id)}" title="${_('View asset «{0}»').format(asset.code)}"><b><tt>${asset.code}</tt></b></a></td>
                    <td>${asset.serial_number or ''}</td>
                    <td>${_('{0} ({1}, floor #{2})').format(area.room, area.building, area.floor)}</td>
                    <td>${format_datetime(utctolocal(asset.creation_date), date_format='short')}</td>
                    <td>${format_datetime(utctolocal(asset.modification_date), date_format='short')}</td>
                    <td>${format_datetime(utctolocal(asset.inventory_date), date_format='short')}</td>
                    <td>${_(asset.STATES[asset.state])}</td>
                </tr>
    % endfor
            </tbody>
        </table>
    </div><!--/.box-body -->
</div><!-- /.box -->
% endif

## Stocks
<%
query = product.stocks.join(Area).filter(Area.site == request.authenticated_user.site)

# Get stock areas and counts
subquery = query.with_entities(
    sa.func.max(Stock.id).label('last'),
).group_by(Stock.area).subquery()

areas_query = query.with_entities(
    Area,
    Stock.total
).join(
    subquery,
    Stock.id == subquery.c.last
).order_by(Stock.area)

areas = areas_query.all()

total = 0
%>

% if areas:
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
    % for index, (area, count) in enumerate(areas):
        <% total += count %>
        <li${' class="active"' if index == 0 else '' | n}><a href="#stock-${index}" data-toggle="tab">${_('{0} ({1}, floor #{2})').format(area.room, area.building, area.floor)} <span class="badge">${count}</span></a></li>
    % endfor
        <li class="pull-right"><span class="badge bg-blue">${total}</span></li>
    </ul><!-- /.nav-tabs -->

    <div class="tab-content">
    % for index, (area, count) in enumerate(areas):
        <%
        area_query = query.filter(Stock.area == area).order_by(sa.desc(Stock.date), sa.desc(Stock.id))
        last_stock = area_query.first()
        %>
        <div class="tab-pane${' active' if index == 0 else ''}" id="stock-${index}">
            <div class="row chart">
                <div class="col-md-8 col-chart">
                    <canvas />
                </div><!-- /.col -->
                <div class="col-md-4 col-table table-responsive no-padding">
        % if request.has_permission('stocks.delete'):
                    <div class="form-group text-right">
                        <a href="${request.route_path('api.stocks.delete', stock=last_stock.id)}" class="api-link btn btn-danger btn-xs" data-method="delete" data-confirm-message="${_('Do you really want to remove stock operation of {0} from area {1} ?').format(format_datetime(utctolocal(last_stock.date), date_format='short'), last_stock.area)}" title="${_('Remove last operation')}"><i class="fa fa-trash icon"> </i> ${_('Remove last operation')}</a>
                    </div>
        % endif
                    <table class="table table-condensed table-striped table-hover">
                        <thead>
                            <tr>
                                <th>${_('Date')}</th>
                                <th>${_('Action')}</th>
                                <th class="text-right">${_('Qty')}</th>
                                <th class="text-right">${_('Stock')}</th>
                            </tr>
                        </thead>
                        <tbody>
        % for item in area_query.limit(20):
                            <tr class="chart-data ${'success' if item.action == 'in' else 'danger' if item.value < 0 else 'info' if item.action == 'stock' else ''}" data-stamp="${format_datetime(utctolocal(item.date), date_format='short')}" data-value="${item.total}">
                                <td>${format_datetime(utctolocal(item.date), date_format='short')}</td>
                                <td>${_(item.ACTIONS[item.action])}</td>
                                <td class="text-right"><tt>${item.value}</tt></td>
                                <td class="text-right"><tt>${item.total}</tt></td>
                            </tr>
        % endfor
                        </tbody>
                    </table>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.tab-pane -->
    % endfor
    </div><!--/.tab-content -->
</div><!-- /.nav-tabs-custom -->
% endif

## Asset modal
<div class="modal fade" id="asset-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="api-form" method="put" action="${request.route_path('api.assets.create')}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">${_('New asset')}</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="author" value="${request.authenticated_user.id}" />
                    <input type="hidden" name="product" value="${product.id}" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="asset-area">${_('Area')}</label>
                                <select class="form-control input-sm" id="asset-area" name="area" data-api-url="${request.route_path('api.areas.autocomplete')}" placeholder="--"></select>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="asset-date">${_('Date')}</label>
                                <div class="input-group date datetimepicker">
                                    <input type="text" class="form-control input-sm datepickerinput" id="asset-date" name="date" value="${datetime.datetime.now().strftime(_('%m/%d/%Y %H:%M'))}" />
                                    <div class="input-group-addon">
                                        <i class="far fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="asset-state">${_('State')}</label>
                                <select class="form-control input-sm selectpicker no-search" name="state" id="asset-state" placeholder="--">
                                    <option value="">--</option>
% for value, name in Asset.STATES.items():
                                    <option value="${value}">${_(name) | n}</option>
% endfor
                                </select>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="form-group">
                        <label for="asset-serial-number">${_('S/N')}</label>
                        <input type="text" class="form-control input-sm" name="serial_number" id="asset-serial-number" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">${_('Create')}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">${_('Cancel')}</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

## Stock modal
<div class="modal fade" id="stock-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="api-form" method="put" action="${request.route_path('api.stocks.create')}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="action" />
                    <input type="hidden" name="author" value="${request.authenticated_user.id}" />
                    <input type="hidden" name="product" value="${product.id}" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="stock-area">${_('Area')}</label>
                                <select class="form-control input-sm" id="stock-area" name="area" data-api-url="${request.route_path('api.areas.autocomplete')}" placeholder="--"></select>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="stock-date">${_('Date')}</label>
                                <div class="input-group date datetimepicker">
                                    <input type="text" class="form-control input-sm datepickerinput" id="stock-date" name="date" value="${datetime.datetime.now().strftime(_('%m/%d/%Y %H:%M'))}" />
                                    <div class="input-group-addon">
                                        <i class="far fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="stock-value">${_('Quantity')}</label>
                                <input type="text" class="form-control input-sm" name="value" id="stock-value" />
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="form-group">
                        <label for="stock-message">${_('Message')}</label>
                        <textarea class="form-control input-sm" name="message" id="stock-message" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">${_('Add record')}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">${_('Cancel')}</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
