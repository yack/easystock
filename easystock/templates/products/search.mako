## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.products.search.js', _query=dict(version=app_version))}"></script>
</%def>

<!-- Search criteria -->
<div class="box box-solid">
    <div class="box-header">
        <i class="fa fa-search"></i>
        <h3 class="box-title">${_('Search criteria')}</h3>
    </div><!-- /.box-header -->

    <%form:form name="search" method="get" role="form">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="label">${_('Label')}</label>
                        <%form:text class_="form-control input-sm" id="label" name="label" />
                    </div>
                </div><!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="category">${_('Category')}</label>
                        <%form:select class_="form-control input-sm selectpicker" id="category" name="category" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in categories:
                            <%form:option value="${value}">${_(name) | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="status">${_('Status')}</label>
                        <%form:select class_="form-control input-sm selectpicker" id="status" name="status" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in statuses:
                            <%form:option value="${value}">${_(name) | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="brand">${_('Brand')}</label>
                        <%form:select class_="form-control input-sm selectpicker" id="brand" name="brand" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in brands:
                            <%form:option value="${value}">${_(name) | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="model">${_('Model')}</label>
                        <%form:text class_="form-control input-sm" id="model" name="model" />
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.box-body -->

        <div class="box-footer">
            <div class="pull-right">
                <div class="btn-group">
                    <a class="btn btn-default form-reset" title="${_('Set default values')}">${_('Reset')}</a>
                    <button class="btn btn-primary" type="submit" name="search" value="search">${_('Search')}</button>
                </div>
                &nbsp;
                <div class="btn-group">
                    <button class="btn btn-default" type="submit" name="format" value="xlsx" title="${_('Export using XLSX format')}">
                        <i class="far fa-file-excel icon"> </i>
                        <span class="sr-only">${_('XLSX')}</span>
                    </button>
                </div>
            </div>
        </div><!-- /.box-footer -->
    </%form:form>
</div><!-- /.box -->

<!-- Search result -->
<div id="products-list" class="partial-block" data-partial-key="products.partial">
<%include file="list.mako" />
</div><!-- /.partial-block -->
