## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.products.modify.js', _query=dict(version=app_version))}"></script>
</%def>

<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">${subtitle}</h3>
    </div><!-- /.box-header -->

    <%form:form name="modify" method="post" role="form">
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group form-required">
                        <label for="category">${_('Category')}</label>
                        <%form:select id="category" name="category" class_="form-control input-sm" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in categories:
                            <%form:option value="${value}">${_(name) | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group form-required">
                        <label for="label">${_('Label')}</label>
                        <%form:text id="label" name="label" class_="form-control input-sm" />
                    </div>
                </div><!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="status">${_('Status')}</label>
                        <%form:select id="status" name="status" class_="form-control input-sm selectpicker no-search" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in statuses:
                            <%form:option value="${value}">${_(name) | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="brand">${_('Brand')}</label>
                        <%form:select id="brand" name="brand" class_="form-control input-sm" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in brands:
                            <%form:option value="${value}">${_(name) | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="model">${_('Model')}</label>
                        <%form:text id="model" name="model" class_="form-control input-sm" />
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="reference">${_('Reference')}</label>
                        <%form:text id="reference" name="reference" class_="form-control input-sm" />
                    </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="barcode">${_('EAN 13')}</label>
                        <%form:text id="barcode" name="barcode" class_="form-control input-sm" />
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.box-body -->

        <div class="box-footer text-right">
            <button class="btn btn-primary" type="submit" name="save" value="save">${_('Save')}</button>
            <a href="${cancel_link}" class="btn btn-default">${_('Cancel')}</a>
        </div><!-- /.box-footer -->
    </%form:form>
</div><!-- /.box -->
