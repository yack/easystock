## -*- coding: utf-8 -*-
<%namespace name="paginate" file="/paginate.mako"/>
<% pager = request.pagers['products'] %>
% if pager.item_count:
<div class="box box-solid">
    <div class="box-header">
        <i class="fas fa-list"> </i>
        <h3 class="box-title">
    % if pager.page_count > 1:
            ${_('from {0} to {1} on {2} products').format(pager.first_item, pager.last_item, pager.item_count)}
    % else:
            ${ungettext('{0} product', '{0} products', pager.item_count).format(pager.item_count)}
    % endif
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table table-condensed table-hover table-striped">
                <thead>
                    <tr>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='id', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('id')}"></span>
                                ${_('Code')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='label', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('label')}"></span>
                                ${_('Label')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='category', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('category')}"></span>
                                ${_('Category')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='brand', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('brand')}"></span>
                                ${_('Brand')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='model', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('model')}"></span>
                                ${_('Model')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='creation_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('creation_date')}"></span>
                                ${_('Creation')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='modification_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('modification_date')}"></span>
                                ${_('Modification')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='status', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('status')}"></span>
                                ${_('Status')}
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
    % for product in pager:
                    <tr${' class="danger"' if product.status == 'discontinued' else '' | n}>
                        <td><a href="${request.route_path('products.visual', product=product.id)}" title="${_('View product «{0}»').format(product.label)}"><b><tt>${product.code}</tt></b></a></td>
                        <td>${product.label}</td>
                        <td>${product.category or ''}</td>
                        <td>${product.brand or ''}</td>
                        <td>${product.model or ''}</td>
                        <td>${format_datetime(utctolocal(product.creation_date), date_format='short')}</td>
                        <td>${format_datetime(utctolocal(product.modification_date), date_format='short')}</td>
                        <td>${_(product.STATUSES[product.status])}</td>
                    </tr>
    % endfor
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer text-right">
${paginate.render_pages(pager, extra_class='no-margin pagination-sm')}
${paginate.render_limit(pager, extra_class='no-margin pagination-sm')}
    </div><!-- /.box-footer -->
</div><!-- /.box -->
% else:
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info">
            <h4><i class="fa fa-info-circle"></i> ${_('No result')}</h4>
            ${_('No product match search criteria.')}
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
% endif
