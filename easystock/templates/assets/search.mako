## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- Moment.js -->
    <script src="${request.static_path('easystock:static/lib/moment-2.14.1/moment.min.js')}"></script>
    % if localizer.locale_name != 'en':
    <script src="${request.static_path('easystock:static/lib/moment-2.14.1/locale/{0}.js'.format(localizer.locale_name))}"></script>
    % endif

    <!-- Bootstrap-datetimepicker -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-datetimepicker-4.17.45/css/bootstrap-datetimepicker.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-datetimepicker-4.17.45/js/bootstrap-datetimepicker.min.js')}"></script>

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.autocompleters.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('easystock:static/js/easystock.assets.search.js', _query=dict(version=app_version))}"></script>
</%def>

<!-- Search criteria -->
<div class="box box-solid">
    <div class="box-header">
        <i class="fa fa-search"></i>
        <h3 class="box-title">${_('Search criteria')}</h3>
    </div><!-- /.box-header -->

    <%form:form name="search" method="get" role="form">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="product">${_('Product')}</label>
                        <%form:autocomplete class_="form-control input-sm" id="product" name="product" data_api_url="${request.route_path('api.products.autocomplete')}" placeholder="--" />
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="category">${_('Category')}</label>
                        <%form:select class_="form-control input-sm selectpicker" id="category" name="category" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in categories:
                            <%form:option value="${value}">${name | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="label">${_('Label')}</label>
                        <%form:text class_="form-control input-sm" id="label" name="label" />
                    </div>
                </div><!-- /.col -->
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="area">${_('Area')}</label>
                        <%form:autocomplete class_="form-control input-sm" id="area" name="area" data_api_url="${request.route_path('api.areas.autocomplete')}" placeholder="--" />
                    </div>
                </div><!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="state">${_('State')}</label>
                        <%form:select class_="form-control input-sm selectpicker" id="state" name="state" placeholder="--">
                            <%form:option value="">--</%form:option>
% for value, name in states:
                            <%form:option value="${value}">${_(name) | n}</%form:option>
% endfor
                        </%form:select>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="date">${_('Inventory')}</label>
                        <div class="input-group date datetimepicker">
                            <%form:text class_="form-control input-sm datepickerinput" id="date" name="date" />
                            <div class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.box-body -->

        <div class="box-footer text-right">
            <div class="btn-group">
                <a class="btn btn-default form-reset" title="${_('Set default values')}">${_('Reset')}</a>
                <button class="btn btn-primary" type="submit" name="search" value="search">${_('Search')}</button>
            </div>
            &nbsp;
            <div class="btn-group">
                <button class="btn btn-default" type="submit" name="format" value="xlsx" title="${_('Export using XLSX format')}">
                    <i class="far fa-file-excel icon"> </i>
                    <span class="sr-only">${_('XLSX')}</span>
                </button>
            </div>
        </div><!-- /.box-footer -->
    </%form:form>
</div><!-- /.box -->

<!-- Search result -->
<div id="assets-list" class="partial-block" data-partial-key="assets.partial">
<%include file="list.mako" />
</div><!-- /.partial-block -->
