## -*- coding: utf-8 -*-
<%!
import datetime
%>
<%namespace name="products" file="/products/visual.mako" />
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- Moment.js -->
    <script src="${request.static_path('easystock:static/lib/moment-2.14.1/moment.min.js')}"></script>
    % if localizer.locale_name != 'en':
    <script src="${request.static_path('easystock:static/lib/moment-2.14.1/locale/{0}.js'.format(localizer.locale_name))}"></script>
    % endif

    <!-- Bootstrap-datetimepicker -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-datetimepicker-4.17.45/css/bootstrap-datetimepicker.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-datetimepicker-4.17.45/js/bootstrap-datetimepicker.min.js')}"></script>

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.autocompleters.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('easystock:static/js/easystock.assets.visual.js', _query=dict(version=app_version))}"></script>
</%def>

<% area = asset.area %>
<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">${subtitle}</h3>
        <div class="box-tools">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fas fa-cog"> </i>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    % if request.has_permission('assets.update'):
                <li><a href="#update-modal" data-toggle="modal" title="${_('Update asset «{0}»'.format(asset.code))}">${_('Update')}</a></li>
    % endif
    % if request.has_permission('assets.delete'):
                <li role="separator" class="divider"></li>
                <li><a href="${request.route_path('api.assets.delete', asset=asset.id)}" class="api-link" data-method="delete" data-confirm-message="${_('Do you really want to delete asset «{0}» ?').format(asset.code)}">${_('Delete')}</a></li>
    % endif
              </ul>
            </div>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->

    <div class="box-body">
${products.render_body(asset.product, with_link=True)}

        <hr>
        <div class="row">
            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>${_('Asset number')}</dt>
                    <dd><tt>${asset.code}</tt></dd>

                    <dt>${_('Serial number')}</dt>
                    <dd>${asset.serial_number or '-'}</dd>

                    <dt>${_('Location')}</dt>
                    <dd>${_('{0} ({1}, floor #{2})').format(area.room, area.building, area.floor)}</dd>

                    <dt>${_('State')}</dt>
                    <dd>${_(asset.STATES[asset.state])}</dd>
                </dl>
            </div><!-- /.col -->
            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>${_('Creation date')}</dt>
                    <dd>${format_datetime(utctolocal(asset.creation_date), date_format='short')}</dd>

                    <dt>${_('Modification date')}</dt>
                    <dd>${format_datetime(utctolocal(asset.modification_date), date_format='short')}</dd>

                    <dt>${_('Inventory date')}</dt>
                    <dd>${format_datetime(utctolocal(asset.inventory_date), date_format='short')}</dd>
                </dl>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!--/.box-body -->
</div><!--/.box -->

## Update modal
<div class="modal fade" id="update-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="api-form" method="post" action="${request.route_path('api.assets.update', asset=asset.id)}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">${_('Update')}</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="author" value="${request.authenticated_user.id}" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="update-area">${_('Area')}</label>
                                <select class="form-control input-sm" id="update-area" name="area" data-api-url="${request.route_path('api.areas.autocomplete')}" placeholder="--" data-value="[${area.id}]"></select>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="update-date">${_('Date')}</label>
                                <div class="input-group date datetimepicker">
                                    <input type="text" class="form-control input-sm datepickerinput" id="update-date" name="date" value="${datetime.datetime.now().strftime(_('%m/%d/%Y %H:%M'))}" />
                                    <div class="input-group-addon">
                                        <i class="far fa-calendar-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="update-state">${_('State')}</label>
                                <select class="form-control input-sm selectpicker no-search" name="state" id="update-state" placeholder="--">
                                    <option value="">--</option>
% for value, name in states:
                                    <option value="${value}"${' selected="selected"' if value == asset.state else '' | n}>${_(name) | n}</option>
% endfor
                                </select>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="form-group">
                        <label for="update-serial-number">${_('S/N')}</label>
                        <input type="text" class="form-control input-sm" name="serial_number" id="update-serial-number" value="${asset.serial_number or ''}"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">${_('Update')}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">${_('Cancel')}</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
