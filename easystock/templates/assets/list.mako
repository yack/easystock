## -*- coding: utf-8 -*-
<%namespace name="paginate" file="/paginate.mako"/>
<% pager = request.pagers['assets'] %>
% if pager.item_count:
<div class="box box-solid">
    <div class="box-header">
        <i class="fas fa-list"> </i>
        <h3 class="box-title">
    % if pager.page_count > 1:
            ${_('from {0} to {1} on {2} assets').format(pager.first_item, pager.last_item, pager.item_count)}
    % else:
            ${ungettext('{0} asset', '{0} assets', pager.item_count).format(pager.item_count)}
    % endif
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table table-condensed table-hover table-striped">
                <thead>
                    <tr>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='id', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('id')}"></span>
                                ${_('Code')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='area', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('area')}"></span>
                                ${_('Area')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='label', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('label')}"></span>
                                ${_('Label')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='category', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('category')}"></span>
                                ${_('Category')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='brand', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('brand')}"></span>
                                ${_('Brand')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='model', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('model')}"></span>
                                ${_('Model')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='serial_number', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('serial_number')}"></span>
                                ${_('S/N')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='creation_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('creation_date')}"></span>
                                ${_('Creation')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='modification_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('modification_date')}"></span>
                                ${_('Modification')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='inventory_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('inventory_date')}"></span>
                                ${_('Inventory')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='state', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('state')}"></span>
                                ${_('State')}
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
    % for asset in pager:
        <%
        area = asset.area
        product = asset.product
        %>
                    <tr>
                        <td><a href="${request.route_path('assets.visual', asset=asset.id)}" title="${_('View asset «{0}»').format(asset.code)}"><b><tt>${asset.code}</tt></b></a></td>
                        <td>${_('{0} ({1}, floor #{2})').format(area.room, area.building, area.floor)}</td>
                        <td><a href="${request.route_path('products.visual', product=product.id)}" title="${_('View product «{0}»').format(product.label)}">${product.label}</a></td>
                        <td>${product.category}</td>
                        <td>${product.brand or ''}</td>
                        <td>${product.model or ''}</td>
                        <td><tt>${asset.serial_number or ''}</tt></td>
                        <td>${format_datetime(utctolocal(asset.creation_date), date_format='short')}</td>
                        <td>${format_datetime(utctolocal(asset.modification_date), date_format='short')}</td>
                        <td>${format_datetime(utctolocal(asset.inventory_date), date_format='short')}</td>
                        <td>${_(asset.STATES[asset.state])}</td>
                    </tr>
    % endfor
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer text-right">
${paginate.render_pages(pager, extra_class='no-margin pagination-sm')}
${paginate.render_limit(pager, extra_class='no-margin pagination-sm')}
    </div><!-- /.box-footer -->
</div><!-- /.box -->
% else:
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info">
            <h4><i class="fa fa-info-circle"></i> ${_('No result')}</h4>
            ${_('No asset match search criteria.')}
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
% endif
