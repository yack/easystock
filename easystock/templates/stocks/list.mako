## -*- coding: utf-8 -*-
<%namespace name="paginate" file="/paginate.mako"/>
<% pager = request.pagers['stocks'] %>
% if pager.item_count:
<div class="box box-solid">
    <div class="box-header">
        <i class="fas fa-list"> </i>
        <h3 class="box-title">
    % if pager.page_count > 1:
            ${_('from {0} to {1} on {2} items').format(pager.first_item, pager.last_item, pager.item_count)}
    % else:
            ${ungettext('{0} item', '{0} items', pager.item_count).format(pager.item_count)}
    % endif
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table table-condensed table-hover table-striped">
                <thead>
                    <tr>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='id', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('id')}"></span>
                                ${_('Code')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='label', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('label')}"></span>
                                ${_('Product')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='category', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('category')}"></span>
                                ${_('Category')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='area', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('area')}"></span>
                                ${_('Area')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='action', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('action')}"></span>
                                ${_('Action')}
                            </a>
                        </th>
                        <th>${_('Message')}</th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('date')}"></span>
                                ${_('Date')}
                            </a>
                        </th>
                        <th class="text-right">
                            <a class="partial-link" href="${pager.link(sort='value', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('value')}"></span>
                                ${_('Qty')}
                            </a>
                        </th>
                        <th class="text-right">
                            <a class="partial-link" href="${pager.link(sort='total', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('total')}"></span>
                                ${_('Stock')}
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
    % for item in pager:
        <%
        area = item.area
        product = item.product
        if item.message:
            title, message = item.message.split('\n', 1) if '\n' in item.message else (item.message, _('No message'))
        else:
            title, message = ('-', _('No message'))
        %>
                    <tr class="${'success' if item.action == 'in' else 'danger' if item.value < 0 else 'info' if item.action == 'stock' else ''}">
                        <td><a href="${request.route_path('products.visual', product=product.id)}" title="${_('View product «{0}»').format(product.label)}"><b><tt>${product.code}</tt></b></a></td>
                        <td>${product.label}</td>
                        <td>${product.category or ''}</td>
                        <td>${_('{0} ({1}, floor #{2})').format(area.room, area.building, area.floor)}</td>
                        <td>${_(item.ACTIONS[item.action])}</td>
                        <td>
                            <a href="javascript: void(0);" data-toggle="popover" data-content="${message.replace('\n', '<br>') | n}" title="${_(item.ACTIONS[item.action])}">${title}</a>
                        </td>
                        <td>${format_datetime(utctolocal(item.date), date_format='short')}</td>
                        <td class="text-right"><tt>${item.value}</tt></td>
                        <td class="text-right"><tt>${item.total}</tt></td>
                    </tr>
    % endfor
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer text-right">
${paginate.render_pages(pager, extra_class='no-margin pagination-sm')}
${paginate.render_limit(pager, extra_class='no-margin pagination-sm')}
    </div><!-- /.box-footer -->
</div><!-- /.box -->
% else:
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info">
            <h4><i class="fa fa-info-circle"></i> ${_('No result')}</h4>
            ${_('No stock operation match search criteria.')}
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
% endif
