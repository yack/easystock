## -*- coding: utf-8 -*-
<%namespace name="paginate" file="/paginate.mako"/>
<% pager = request.pagers['sites'] %>
% if pager.item_count:
<div class="box box-solid">
    <div class="box-header">
        <i class="fa fa-list"></i>
        <h3 class="box-title">
    % if pager.page_count > 1:
            ${_('from {0} to {1} on {2} sites').format(pager.first_item, pager.last_item, pager.item_count)}
    % else:
            ${ungettext('{0} site', '{0} sites', pager.item_count).format(pager.item_count)}
    % endif
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table tale-condensed table-hover table-striped">
                <thead>
                    <tr>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='name', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('name')}"></span>
                                ${_('Name')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='address', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('address')}"></span>
                                ${_('Address')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='description', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('description')}"></span>
                                ${_('Description')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='creation_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('creation_date')}"></span>
                                ${_('Creation date')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='modification_date', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('modification_date')}"></span>
                                ${_('Modification date')}
                            </a>
                        </th>
                        <th>
                            <a class="partial-link" href="${pager.link(sort='status', order='toggle')}" title="${_('Ordering using this column')}">
                                <span class="${pager.header_class('status')}"></span>
                                ${_('Status')}
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
    % for site in pager:
                    <tr>
                        <td><a href="${request.route_path('sites.visual', site=site.id)}" title="${_('View site «{0}»').format(site.name)}">${site.name}</a></td>
                        <td>${site.address or ''}</td>
                        <td>${site.description or ''}</td>
                        <td>${format_datetime(utctolocal(site.creation_date), date_format='short')}</td>
                        <td>${format_datetime(utctolocal(site.modification_date), date_format='short')}</td>
                        <td>${_(site.STATUSES[site.status])}</td>
                    </tr>
    % endfor
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer text-right">
${paginate.render_pages(pager, extra_class='no-margin pagination-sm')}
${paginate.render_limit(pager, extra_class='no-margin pagination-sm')}
    </div><!-- /.box-footer -->
</div><!-- /.box -->
% else:
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info">
            <h4><i class="fa fa-info-circle"></i> ${_('No result')}</h4>
            ${_('No site match search criteria.')}
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
% endif
