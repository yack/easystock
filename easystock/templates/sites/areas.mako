## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- TouchSpin -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-touchspin-3.1.2/jquery.bootstrap-touchspin.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-touchspin-3.1.2/jquery.bootstrap-touchspin.min.js')}"></script>

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.multiple-items.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('easystock:static/js/easystock.sites.areas.js', _query=dict(version=app_version))}" type="text/javascript"></script>
</%def>

<%form:form name="areas" method="post" role="form">
    <div class="box box-solid">
        <div class="box-header">
            <i class="fa fa-building"></i>
            <h3 class="box-title">${subtitle}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
<% count = len(form_ctx['current'].decoded['areas']) or 1 %>
            <div class="multiple-items" data-group="areas">
% for index in range(count):
                <div class="item">
                    <%form:hidden name="areas-${str(index)}.area" />
                    <div class="row form-group">
                        <div class="col-md-4">
                            <%form:text id="areas-${str(index)}-building" name="areas-${str(index)}.building" class_="form-control input-sm" placeholder="${_('Building')}" />
                        </div>
                        <div class="col-md-2">
                            <%form:text id="areas-${str(index)}-floor" name="areas-${str(index)}.floor" class_="form-control input-sm floor" placeholder="${_('Floor')}" />
                        </div>
                        <div class="col-md-3">
                            <%form:text id="areas-${str(index)}-room" name="areas-${str(index)}.room" class_="form-control input-sm" placeholder="${_('Room')}" />
                        </div>
                        <div class="col-md-2">
                            <%form:select id="areas-${str(index)}-status" name="areas-${str(index)}.status" class_="form-control input-sm selectpicker no-search" placeholder="--">
% for value, name in statuses:
                                <%form:option value="${value}">${_(name)}</%form:option>
% endfor
                            </%form:select>
                        </div>
                        <div class="col-md-1 text-right">
                            <a href="#" class="btn btn-default btn-flat btn-sm del-item" title="${_('Remove this area')}"><i class="fa fa-minus"> </i><span class="sr-only"> ${_('Remove')}</span></a>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.item -->
% endfor
                <div class="row form-group">
                    <div class="col-md-3">
                        <a href="#" class="btn btn-primary btn-flat add-item"><i class="fa fa-plus"> </i> ${_('Add an area')}</a>
                    </div>
                </div>
            </div><!-- /.multiple-items -->
        </div><!-- /.box-body -->

        <div class="box-footer text-right">
            <button type="submit" class="btn btn-primary">${_('Save')}</button>
            <a href="${request.route_path('sites.visual', site=site.id)}" class="btn btn-default">${_('Cancel')}</a>
        </div><!-- /.box-footer -->
    </div><!-- /.box -->
</%form:form>
