## -*- encoding: utf-8 -*-
<%!
import sqlalchemy as sa
from easystock.models import DBSession
from easystock.models import Area
from easystock.models import Asset
from easystock.models import Product
from easystock.models import Stock
%>
<%inherit file="/site.mako" />

<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">${subtitle}</h3>
% if request.has_permission('sites.modify'):
        <div class="box-tools">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fas fa-cog"> </i>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="${request.route_path('sites.modify', site=site.id)}" title="${_('Modify site «{0}»').format(site.name)}">${_('Modify')}</a></li>
                <li><a href="${request.route_path('sites.areas.manage', site=site.id)}" title="${_('Manage areas of site «{0}»').format(site.name)}">${_('Manage areas')}</a></li>
    % if request.has_permission('sites.delete'):
                <li role="separator" class="divider"></li>
                <li><a href="${request.route_path('api.sites.delete', site=site.id)}" class="api-link" data-method="delete" data-confirm-message="${_('Do you really want to delete site «{0}» ?').format(site.name)}">${_('Delete')}</a></li>
    % endif
              </ul>
            </div>
        </div><!-- /.box-tools -->
% endif
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>${_('Address')}</dt>
                    <dd>${site.address.replace('\n', '<br>') if site.address else '' | n}</dd>

                    <dt>${_('Description')}</dt>
                    <dd>${site.description.replace('\n', '<br>') if site.description else '' | n}</dd>
                </dl>
            </div><!-- /.col -->

            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>${_('Status')}</dt>
                    <dd>${_(site.STATUSES[site.status])}</dd>

                    <dt>${_('Creation date')}</dt>
                    <dd>${format_datetime(utctolocal(site.creation_date), date_format='short')}</dd>

                    <dt>${_('Modification date')}</dt>
                    <dd>${format_datetime(utctolocal(site.modification_date), date_format='short')}</dd>
                </dl>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.box-body -->
</div><!-- /.box -->

## Areas
<%
query = DBSession.query(Area, sa.func.count(Asset.id), sa.func.count(Product.id.distinct()))
query = query.filter(Area.site == site)
query = query.outerjoin(Stock, Area.stocks).outerjoin(Product, Stock.product)
query = query.outerjoin(Asset, Area.assets)
query = query.order_by(Area.building, Area.floor, Area.room)
query = query.group_by(Area)
%>
% if query.count():
<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">${_('Areas')}</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-condensed table-hover table-striped no-padding">
                <thead>
                    <tr>
                        <th>${_('Building')}</th>
                        <th>${_('Floor')}</th>
                        <th>${_('Room')}</th>
                        <th>${_('Status')}</th>
                        <th>${_('Assets')}</th>
                        <th>${_('Products')}</th>
                    </tr>
                </thead>
                <tbody>
    % for area, assets_count, products_count in query:
                    <tr>
                        <td style="white-space: nowrap">${area.building}</td>
                        <td class="text-right">${area.floor}</td>
                        <td style="width: 100%">${area.room}</td>
                        <td>${_(area.STATUSES[area.status])}</td>
                        <td class="text-right">
        % if assets_count:
                            <a href="${request.route_path('assets.search', _query=dict(area=area.id))}" title="${_('View asset(s) in this area')}">${assets_count}</a>
        % else:
                            -
        % endif
                        </td>
                        <td class="text-right">
        % if products_count:
                            <a href="${request.route_path('stocks.search', _query=dict(area=area.id))}" title="${_('View stock(s) in this area')}">${products_count}</a>
        % else:
                            -
        % endif
                        </td>
                    </tr>
    % endfor
                </tbody>
            </table>
        </div><!-- /.table-responsive -->
    </div><!--/.box-body -->
</div><!--/.box -->
% else:
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info">
            <h4><i class="fa fa-info-circle"></i> ${_('No area')}</h4>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
% endif
