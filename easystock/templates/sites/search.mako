## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}

    <!-- Custom scripts -->
    <script src="${request.static_path('easystock:static/js/easystock.sites.search.js', _query=dict(version=app_version))}" type="text/javascript"></script>
</%def>

<!-- Search criteria -->
<div class="box box-solid">
    <div class="box-header">
        <i class="fa fa-search"></i>
        <h3 class="box-title">${_('Search criteria')}</h3>
    </div><!-- /.box-header -->
    <%form:form name="search" role="form" method="get">
        <div class="box-body">

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="name">${_('Name')}</label>
                    <%form:text type="text" class_="form-control input-sm" id="name" name="name" />
                </div>

                <div class="form-group col-md-4">
                    <label for="address">${_('address')}</label>
                    <%form:text type="text" class_="form-control input-sm" id="address" name="address" />
                </div>

                <div class="form-group col-md-4">
                    <label for="enabled">${_('Status')}</label>
                    <%form:select placeholder="--" class_="form-control selectpicker input-sm no-search" id="status" name="status">
                        <%form:option value=""></%form:option>
% for value, name in statuses:
                        <%form:option value="${value}">${_(name)}</%form:option>
% endfor
                    </%form:select>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label for="description">${_('Description')}</label>
                    <%form:text type="text" class_="form-control input-sm" id="description" name="description" />
                </div>

            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <div class="pull-right">
                <div class="btn-group">
                    <a class="btn btn-default form-reset">${_('Reset')}</a>
                    <button class="btn btn-primary" type="submit" name="search" value="search">${_('Search')}</button>
                </div>
            </div>
        </div><!-- /.box-footer -->
    </%form:form>
</div><!-- /.box -->


<!-- Search result -->
<div id="sites-list" class="partial-block" data-partial-key="sites.partial">
<%include file="list.mako" />
</div><!-- /.partial-block -->
