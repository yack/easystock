## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%form:form name="modify" method="post" role="form">
    <div class="box box-solid">
        <div class="box-header">
            <i class="fa fa-building"></i>
            <h3 class="box-title">${subtitle}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="form-group col-md-8 form-required">
                    <label for="name">${_('Name')}</label>
                    <%form:text id="name" name="name" class_="form-control input-sm" />
                </div><!-- /.col -->

                <div class="form-group col-md-4 form-required">
                    <label for="status">${_('Status')}</label>
                    <%form:select placeholder="--" class_="form-control input-sm selectpicker no-search" id="status" name="status">
                        <%form:option value=""></%form:option>
% for value, name in statuses:
                        <%form:option value="${value}">${_(name)}</%form:option>
% endfor
                    </%form:select>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="address">${_('Address')}</label>
                    <%form:textarea class_="form-control input-sm" id="address" name="address" rows="5"></%form:textarea>
                </div><!-- /.col -->

                <div class="form-group col-md-6">
                    <label for="description">${_('Description')}</label>
                    <%form:textarea class_="form-control input-sm" id="description" name="description" rows="5"></%form:textarea>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.box-body -->

        <div class="box-footer text-right">
            <button type="submit" class="btn btn-primary">${_('Save')}</button>
            <a href="${cancel_link}" class="btn btn-default">${_('Cancel')}</a>
        </div><!-- /.box-footer -->
    </div><!-- /.box -->
</%form:form>
