## -*- coding: utf-8 -*-
<%namespace name="form" file="/form-tags.mako"/>\
<%!

skin = 'blue-light'
%>\
<!DOCTYPE html>
<html lang="${request.locale_name}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="pyramid web application">
    <meta name="author" content="Pylons Project">

    <title>EasyStock - ${title}${'- {0}'.format(subtitle) if subtitle else ''}</title>

    <!-- jQuery -->
    <script src="${request.static_path('easystock:static/lib/jquery-3.3.1.min.js')}"></script>

    <!-- Bootstrap -->
    <link href="${request.static_path('easystock:static/lib/bootstrap-3.3.7/css/bootstrap.min.css')}" rel="stylesheet">
    <script src="${request.static_path('easystock:static/lib/bootstrap-3.3.7/js/bootstrap.min.js')}"></script>

    <!-- Font Awesome -->
    <link href="${request.static_path('easystock:static/font/fontawesome-5.0.6/css/fontawesome-all.min.css')}" rel="stylesheet" />

    <!-- AdminLTE -->
    <script src="${request.static_path('easystock:static/lib/admin-lte-2.4.3/js/adminlte.min.js')}"></script>
    <script src="${request.static_path('easystock:static/lib/jquery.slimScroll-1.3.8/jquery.slimscroll.min.js')}"></script>
    <link href="${request.static_path('easystock:static/lib/admin-lte-2.4.3/css/AdminLTE.min.css')}" rel="stylesheet" />
    <link href="${request.static_path('easystock:static/lib/admin-lte-2.4.3/css/skins/skin-{0}.min.css'.format(skin))}" rel="stylesheet" />

    <!-- Custom files -->
    <link href="${request.static_url('easystock:static/css/easystock.css')}" rel="stylesheet">
    <script src="${request.static_url('easystock:static/js/easystock.js')}"></script>
</head>
<body class="hold-transition login-page">

<div class="login-box">
    <div class="login-logo">
        <a href="${request.route_path('index')}"><b>Easy</b>Stock</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">${title}</p>

        <%form:form name="signin" method="post" role="form">
            <%form:hidden name="came_from" />

            <div class="form-group has-feedback">
                <%form:text name="username" id="username" class_="form-control" placeholder="${_('Username')}" />
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <%form:password name="password" id="password" class_="form-control" placeholder="${_('Password')}" />
                <span class="fa fa-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4 col-xs-offset-8">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">${_('Sign In')}</button>
                </div><!-- /.col -->
            </div>
        </%form:form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

</body>
</html>
