<%inherit file="/site.mako" />

<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>

    <div class="error-content">
        <h3><i class="fa fa-exclamation-triangle text-yellow"></i> ${_('Oops! Page not found.')}</h3>

        <p>
            ${_('We could not find the page you were looking for. Meanwhile, you may <a href="{0}">return to dashboard</a> or try using the search form.').format(request.route_path('index')) | n}
        </p>

        <!--form class="search-form">
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search">

                <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form-->
    </div>
    <!-- /.error-content -->
</div>
<!-- /.error-page -->
