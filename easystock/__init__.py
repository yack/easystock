# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Pyramid initialization """

import time

import pkg_resources

from pyramid.config import Configurator

from easystock.resources import RootFactory


APP_NAME = 'EasyStock'
APP_VERSION = '0.0'
APP_INSTANCE = 'Ifeelgood.org'


def on_before_renderer(event):
    """ Add global variables """

    request = event['request']
    registry = request.registry
    settings = registry.settings

    for key in ('instance', 'name', 'version'):
        event['app_{0}'.format(key)] = settings['app.{0}'.format(key)]


def main(global_config, **settings):
    """
    This function returns a Pyramid WSGI application.
    """

    # Global config
    config = Configurator(
        root_factory=RootFactory,
        settings=settings,
    )

    # EasyStock
    includeme(config)

    # Model setup
    config.include('easystock.models')

    # Export setup
    config.include('easystock.export')

    # Auth setup
    # Mako
    config.include('pyramid_mako')

    # Beaker session
    config.include('pyramid_beaker')

    # Transaction manager
    config.include('pyramid_tm')

    # Pyramid Helpers
    config.include('pyramid_helpers')

    # Subscribers setup
    config.add_subscriber(on_before_renderer, 'pyramid.events.BeforeRender')

    # Static route
    config.add_static_view('static', 'easystock:static', cache_max_age=3600)

    #
    # Applicative routes
    #
    config.add_route('index', '/')
    config.add_route('api-doc.index', '/api-doc')
    config.add_route('imports.upload', '/{target}/import', enum_predicate=dict(target=('users', )))

    # Auth
    config.add_route('auth.sign-in', '/auth/sign-in')
    config.add_route('auth.sign-out', '/auth/sign-out')

    # Dashboard
    config.add_route('dashboard', '/dashboard')

    # Inventory
    config.add_route('inventory', '/inventory')

    # Assets
    config.add_route('assets.search', '/assets')
    config.add_route('assets.visual', '/assets/{asset}', numeric_predicate='asset')

    # Products
    config.add_route('products.search', '/products')
    config.add_route('products.create', '/products/create')
    config.add_route('products.duplicate', '/products/{product}/duplicate', numeric_predicate='product')
    config.add_route('products.visual', '/products/{product}', numeric_predicate='product')
    config.add_route('products.modify', '/products/{product}/modify', numeric_predicate='product')

    # Sites
    config.add_route('sites.areas.manage', '/sites/{site}/areas/manage', numeric_predicate='site')
    config.add_route('sites.search', '/sites')
    config.add_route('sites.create', '/sites/create')
    config.add_route('sites.visual', '/sites/{site}', numeric_predicate='site')
    config.add_route('sites.modify', '/sites/{site}/modify', numeric_predicate='site')

    # Stock
    config.add_route('stocks.search', '/stocks')

    # Users
    config.add_route('users.search', '/users')
    config.add_route('users.create', '/users/create')
    config.add_route('users.profile', '/users/profile')
    config.add_route('users.modify', '/users/{user}/modify', numeric_predicate='user')

    #
    # API routes
    #

    # Areas
    config.add_route('api.areas.autocomplete', '/api/1.0/areas/autocomplete', request_method='GET')
    config.add_route('api.areas.delete', '/api/1.0//areas/{area}', numeric_predicate='area', request_method='DELETE')
    config.add_route('api.areas.get', '/api/1.0/areas/{area}', numeric_predicate='area', request_method='GET')

    # Assets
    config.add_route('api.assets.create', '/api/1.0/assets', request_method='PUT')
    config.add_route('api.assets.delete', '/api/1.0/assets/{asset}', numeric_predicate='asset', request_method='DELETE')
    config.add_route('api.assets.update', '/api/1.0/assets/{asset}', numeric_predicate='asset', request_method='POST')

    # Products
    config.add_route('api.products.autocomplete', '/api/1.0/products/autocomplete', request_method='GET')
    config.add_route('api.products.delete', '/api/1.0/products/{product}', numeric_predicate='product', request_method='DELETE')
    config.add_route('api.products.get', '/api/1.0/products/{product}', numeric_predicate='product', request_method='GET')

    # Sites
    config.add_route('api.sites.autocomplete', '/api/1.0/sites/autocomplete', request_method='GET')
    config.add_route('api.sites.delete', '/api/1.0//sites/{site}', numeric_predicate='site', request_method='DELETE')
    config.add_route('api.sites.get', '/api/1.0/sites/{site}', numeric_predicate='site', request_method='GET')

    # Stocks
    config.add_route('api.stocks.create', '/api/1.0/stocks', request_method='PUT')
    config.add_route('api.stocks.delete', '/api/1.0/stocks/{stock}', numeric_predicate='stock', request_method='DELETE')

    # Users
    config.add_route('api.users.delete', '/api/1.0/users/{user}', numeric_predicate='user', request_method='DELETE')

    # Scan EasyStock views modules
    config.scan('easystock.views')

    # Create Pyramid WSGI app
    app = config.make_wsgi_app()

    return app


def includeme(config):
    """
    Set up standard configurator registrations. Use via:

    .. code-block:: python

    config = Configurator()
    config.include('easystock')
    """

    registry = config.registry
    settings = registry.settings

    # Set application instance, name and version
    if settings.get('app.instance') is None:
        settings['app.instance'] = APP_INSTANCE

    if settings.get('app.name') is None:
        settings['app.name'] = APP_NAME

    if settings.get('app.version') is None:
        settings['app.version'] = 'V{0}'.format(pkg_resources.get_distribution(__name__).version)

    elif settings['app.version'] == 'devel':
        settings['app.version'] = str(int(time.time()))
