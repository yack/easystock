# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Stocks mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import unidecode

import sqlalchemy as sa

from pyramid.view import view_config

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.forms.stocks import SearchForm
from easystock.models import Area
from easystock.models import DBSession
from easystock.models import Product
from easystock.models import Stock

log = logging.getLogger(__name__)


def build_search_query(sort='id', order='asc', **criteria):
    """ Build a search query """

    query = DBSession.query(Stock).join(Area).join(Product)

    # Filters
    if criteria.get('excluded_ids'):
        query = query.filter(Stock.id.notin(criteria['excluded_ids']))

    if criteria.get('selected_ids'):
        query = query.filter(Stock.id.in_(criteria['selected_ids']))

    if criteria.get('action'):
        query = query.filter(Stock.action == criteria['action'])

    if criteria.get('area'):
        query = query.filter(Stock.area == criteria['area'])

    if criteria.get('category'):
        query = query.filter(Product.category == criteria['category'])

    if criteria.get('label'):
        query = query.filter(sa.func.unaccent(Product.label).ilike('%{0}%'.format(unidecode.unidecode(criteria['label']))))

    if criteria.get('product'):
        query = query.filter(Stock.product == criteria['product'])

    if criteria.get('site'):
        query = query.filter(Area.site == criteria['site'])

    if not criteria.get('view_all') is True:
        # Subquery to get «last» value
        subquery = DBSession.query(
            sa.func.max(Stock.id).label('last'),
        ).join(
            Area
        ).group_by(
            Stock.product_id,
            Area.id,
        )

        if criteria.get('area'):
            subquery = subquery.filter(Stock.area == criteria['area'])

        if criteria.get('date'):
            subquery = subquery.filter(Stock.date <= criteria['date'])

        if criteria.get('site'):
            subquery = subquery.filter(Area.site == criteria['site'])

        subquery = subquery.subquery()

        query = query.join(subquery, Stock.id == subquery.c.last)

    elif criteria.get('date'):
        query = query.filter(Stock.date <= criteria['date'])

    # Order
    if sort == 'action':
        order_by = [Stock.action, Product.label, Stock.date]
    elif sort == 'area':
        order_by = [Area.building, Area.floor, Area.room, Product.label, Stock.date]
    elif sort == 'category':
        order_by = [Product.category, Product.label, Stock.date]
    elif sort == 'id':
        order_by = [Product.id, Product.label]
    elif sort == 'date':
        order_by = [Stock.date, Product.label]
    elif sort == 'label':
        order_by = [Product.label, Stock.date]
    elif sort == 'total':
        order_by = [Stock.total, Product.label, Stock.date]
    elif sort == 'value':
        order_by = [Stock.value, Product.label, Stock.date]

    sa_func = sa.asc if order == 'asc' else sa.desc
    query = query.order_by(*[sa_func(column).nullsfirst() for column in order_by])

    return query


@view_config(route_name='stocks.search', renderer='/stocks/search.mako', permission='stocks.search')
@paginate('stocks', form='search', limit=10, sort='date', order='desc', partial_template='/stocks/list.mako', volatile_items=('format', 'search'))
@validate('search', SearchForm, method='get', persistent=True, volatile_items=('format', 'search'))
def search(request):
    _ = request.translate
    form = request.forms['search']
    pager = request.pagers['stocks']

    # Build query
    if not form.errors:
        query = build_search_query(sort=pager.sort, order=pager.order, site=request.authenticated_user.site, **form.result)


        pager.set_collection(query)

    if pager.partial:
        return dict()

    breadcrumb = [
        (_('Stocks'), None),
    ]

    # Get categories
    query = DBSession.query(sa.distinct(Product.category)).join(Stock).join(Area).filter(
        Area.site == request.authenticated_user.site,
    ).order_by(Product.category)

    categories = [(category, category) for category, in query]

    return dict(
        actions=[(k, Stock.ACTIONS[k]) for k in Stock.ACTIONS],
        breadcrumb=breadcrumb,
        categories=categories,
        subtitle=_('Find stocks'),
        title=_('Stocks'),
    )
