# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPInternalServerError
from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.security import forget
from pyramid.security import remember
from pyramid.view import forbidden_view_config
from pyramid.view import notfound_view_config
from pyramid.view import view_config

from pyramid_helpers.auth import check_credentials
from pyramid_helpers.forms import validate
from pyramid_helpers.utils import get_settings

from easystock.forms import InventorySearchForm
from easystock.forms import SignInForm

from easystock.models import DBSession
from easystock.models import Asset
from easystock.models import Product
from easystock.models import Stock

log = logging.getLogger(__name__)


@view_config(route_name='dashboard', renderer='/dashboard.mako', permission='dashboard')
def dashboard(request):
    _ = request.translate

    breadcrumb = [
        (_('Dashboard'), None),
    ]

    return dict(
        breadcrumb=breadcrumb,
        subtitle=None,
        title=_('Dashboard'),
    )


@forbidden_view_config(renderer='/error-403.mako')
def error_403(context, request):
    _ = request.translate

    if request.authenticated_user is None:
        if request.authentication_policy == 'basic':
            # Issuing a challenge
            response = HTTPUnauthorized()
            response.headers.update(forget(request))
            return response

        return HTTPFound(location=request.route_path('auth.sign-in', _query=dict(came_from=request.url)))

    # Prevent from rendering wrong template
    if hasattr(request, 'override_renderer'):
        del request.override_renderer

    request.response.status_int = 403

    breadcrumb = [
        (_('Access denied'), None),
    ]

    return dict(
        breadcrumb=breadcrumb,
        subtitle=None,
        title=_('403 Error Page'),
    )


@notfound_view_config(renderer='/error-404.mako')
def error_404(request):
    _ = request.translate

    # Prevent from rendering wrong template
    if hasattr(request, 'override_renderer'):
        del request.override_renderer

    request.response.status_int = 404

    breadcrumb = [
        (_('Page not found'), None),
    ]

    return dict(
        breadcrumb=breadcrumb,
        subtitle=None,
        title=_('404 Error Page'),
    )


@view_config(route_name='index')
def index(request):
    _ = request.translate
    session = request.session

    # Clear session
    session.delete()

    # Flash message
    session.flash(dict(
        message=_('Your session data has been cleared'),
        type='success',
    ))

    return HTTPFound(location=request.route_path('dashboard'))


@view_config(route_name='inventory', renderer='/inventory.mako')
@validate('search', InventorySearchForm, method='get')
def inventory(request):
    _ = request.translate

    asset = None
    barcode = None
    product = None

    form = request.forms['search']
    if not form.errors and form.result['barcode']:
        barcode = form.result['barcode']
        if barcode.isdigit():
            asset = DBSession.query(Asset).filter(Asset.id == int(barcode)).first()

        if asset:
            product = asset.product
        else:
            product = DBSession.query(Product).filter(Product.barcode == barcode).first()

    breadcrumb = [
        (_('Inventory'), None),
    ]

    return dict(
        actions=[(k, Stock.ACTIONS[k]) for k in Stock.ACTIONS],
        asset=asset,
        barcode=barcode,
        breadcrumb=breadcrumb,
        product=product,
        states=[(k, Asset.STATES[k]) for k in Asset.STATES],
        subtitle=None,
        title=_('Inventory'),
    )


@view_config(route_name='auth.sign-in', renderer='/sign-in.mako')
@validate('signin', SignInForm, extract='post')
def sign_in(request):
    _ = request.translate
    form = request.forms['signin']

    if request.authentication_policy == 'remote':
        params = get_settings(request, 'auth', 'policy:remote')
        login_url = params.get('login_url')
        if login_url is None:
            raise HTTPInternalServerError(explanation=_('Authentication not available, please retry later.'))

        return HTTPFound(location=login_url)

    if request.method == 'POST':
        if not form.errors:
            username = form.result['username']
            password = form.result['password']
            came_from = form.result['came_from']

            if check_credentials(username, password):
                headers = remember(request, username)
                return HTTPFound(location=came_from, headers=headers)

            form.errors['username'] = _('Bad username or password')

    else:
        data = dict(came_from=request.params.get('came_from', request.route_path('index')))
        form.set_data(data)

    return dict(
        title=_('Sign in'),
    )


@view_config(route_name='auth.sign-out', renderer='/sign-out.mako')
def sign_out(request):
    _ = request.translate
    session = request.session

    # Clear session
    session.delete()

    if request.authentication_policy == 'remote':
        params = get_settings(request, 'auth', 'policy:remote')
        logout_url = params.get('logout_url')
        if logout_url is None:
            # Display sign-out page
            return dict(
                title=_('Signed out'),
            )
    else:
        logout_url = request.route_path('index')

    headers = forget(request)
    return HTTPFound(location=logout_url, headers=headers)
