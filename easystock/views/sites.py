# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import unidecode

from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.forms.sites import AreasForm
from easystock.forms.sites import ModifyForm
from easystock.forms.sites import SearchForm

from easystock.models import DBSession
from easystock.models import Area
from easystock.models import Site
from easystock.views.common import get_site

log = logging.getLogger(__name__)


def build_search_query(sort='id', order='asc', **criteria):
    """ Build a search query """

    query = DBSession.query(Site)

    # Filters
    if criteria.get('excluded_ids'):
        query = query.filter(Site.id.notin(criteria['excluded_ids']))

    if criteria.get('selected_ids'):
        query = query.filter(Site.id.in_(criteria['selected_ids']))

    if criteria.get('address'):
        query = query.filter(sa.func.unaccent(Site.address).ilike('%{0}%'.format(unidecode.unidecode(criteria['address']))))

    if criteria.get('description'):
        query = query.filter(sa.func.unaccent(Site.description).ilike('%{0}%'.format(unidecode.unidecode(criteria['description']))))

    if criteria.get('name'):
        query = query.filter(sa.func.unaccent(Site.name).ilike('%{0}%'.format(unidecode.unidecode(criteria['name']))))

    if criteria.get('status'):
        query = query.filter(Site.status == criteria['status'])

    if criteria.get('term'):
        query = query.filter(sa.func.unaccent(Site.name).ilike('%{0}%'.format(unidecode.unidecode(criteria['term']))))

    # Order
    if sort == 'address':
        order_by = [Site.address, Site.name]
    elif sort == 'creation_date':
        order_by = [Site.creation_date, Site.name]
    elif sort == 'description':
        order_by = [Site.description, Site.name]
    elif sort == 'modification_date':
        order_by = [Site.modification_date, Site.name]
    elif sort == 'id':
        order_by = [Site.id]
    elif sort == 'name':
        order_by = [Site.name]
    elif sort == 'status':
        order_by = [Site.status, Site.name]

    sa_func = sa.asc if order == 'asc' else sa.desc
    query = query.order_by(*[sa_func(column) for column in order_by])

    return query


@validate('modify', ModifyForm)
def _edit(request, site=None):
    _ = request.translate
    form = request.forms['modify']

    if site is None:
        breadcrumb = [
            (_('Sites'), request.route_path('sites.search')),
            (_('New site'), request.route_path('sites.create')),
        ]
        subtitle = _('Create a new site')
        cancel_link = request.route_path('sites.search')
    else:
        breadcrumb = [
            (_('Sites'), request.route_path('sites.search')),
            (site.name, request.route_path('sites.visual', site=site.id)),
            (_('Edition'), request.route_path('sites.modify', site=site.id)),
        ]
        subtitle = _('Site «{0}» edition').format(site.name)
        cancel_link = request.route_path('sites.visual', site=site.id)

    if request.method == 'POST':
        if not form.errors:
            # Check name unicity
            same_site = DBSession.query(Site).filter_by(name=form.result['name']).first()
            if same_site and same_site != site:
                form.errors['name'] = _('Name already used by another site')

        if not form.errors:
            if site is None:
                site = Site()
                DBSession.add(site)

            site.from_dict(form.result)

            # Get newly created object's id
            DBSession.flush()

            return HTTPFound(location=request.route_path('sites.visual', site=site.id))

    elif site:
        data = site.to_dict()
        form.set_data(data)

    else:
        data = dict(status='active')
        form.set_data(data)

    return dict(
        breadcrumb=breadcrumb,
        cancel_link=cancel_link,
        statuses=[(k, Site.STATUSES[k]) for k in Site.STATUSES],
        subtitle=subtitle,
        title=_('Sites'),
    )


@view_config(route_name='sites.areas.manage', renderer='/sites/areas.mako', permission='sites.modify')
@validate('areas', AreasForm)
def areas_manage(request):
    _ = request.translate
    form = request.forms['areas']

    site = get_site(request)
    if site is None:
        raise HTTPNotFound(explanation=_('Invalid site id'))

    # Don't remove an area associated with assets or stocks
    not_deletable = [
        area
        for area in site.areas
        if area.assets.count() or area.stocks.count()
    ]

    if request.method == 'POST':
        if not form.errors:
            # Check room, build, floor unicity
            bfr = [
                (data['building'], data['floor'], data['room'])
                for data in form.result['areas']
            ]
            for index, item in enumerate(bfr):
                if bfr.count(item) > 1:
                    form.errors['areas-{0}.building'.format(index)] = _('Duplicate entry for building, floor and room triplet.')

        if not form.errors:
            areas = []
            for data in form.result['areas']:
                area = data['area']
                if area is None:
                    area = Area()

                area.from_dict(data)

                areas.append(area)

            # Silently keep not deletable areas
            for area in not_deletable:
                if area not in areas:
                    areas.append(area)

            # Update site
            site.from_dict(dict(areas=areas))

            return HTTPFound(location=request.route_path('sites.visual', site=site.id))

    else:
        areas = []
        for area in site.areas:
            data = area.to_dict()
            data['area'] = area
            areas.append(data)

        form.set_data(dict(areas=areas))

    breadcrumb = [
        (_('Sites'), request.route_path('sites.search')),
        (site.name, request.route_path('sites.visual', site=site.id)),
        (_('Areas'), request.route_path('sites.areas.manage', site=site.id)),
    ]
    subtitle = _('Site «{0}» areas').format(site.name)

    return dict(
        breadcrumb=breadcrumb,
        not_deletable=not_deletable,
        site=site,
        statuses=[(k, Area.STATUSES[k]) for k in Area.STATUSES],
        subtitle=subtitle,
        title=_('Sites'),
    )


@view_config(route_name='sites.create', renderer='/sites/modify.mako', permission='sites.create')
def create(request):
    return _edit(request)


@view_config(route_name='sites.modify', renderer='/sites/modify.mako', permission='sites.modify')
def modify(request):
    _ = request.translate

    site = get_site(request)
    if site is None:
        raise HTTPNotFound(explanation=_('Invalid site id'))

    return _edit(request, site=site)


@view_config(route_name='sites.search', renderer='/sites/search.mako', permission='sites.search')
@paginate('sites', form='search', limit=10, sort='modification_date', order='desc', partial_template='/sites/list.mako', volatile_items=('format', 'search'))
@validate('search', SearchForm, method='get', persistent=True, volatile_items=('format', 'search'))
def search(request):
    """ Search sites """
    _ = request.translate
    form = request.forms['search']
    pager = request.pagers['sites']

    # Build query
    if not form.errors:
        query = build_search_query(sort=pager.sort, order=pager.order, **form.result)

        pager.set_collection(query)

        if pager.item_count == 1 and 'search' in request.params:
            # Only one site is matching search criteria, bouncing to visual page
            site = query.first()
            return HTTPFound(location=request.route_path('sites.visual', site=site.id))

    if pager.partial:
        return dict()

    breadcrumb = [
        (_('Sites'), request.route_path('sites.search')),
    ]

    return dict(
        breadcrumb=breadcrumb,
        statuses=[(k, Site.STATUSES[k]) for k in Site.STATUSES],
        subtitle=_('Find sites'),
        title=_('Sites'),
    )


@view_config(route_name='sites.visual', renderer='/sites/visual.mako', permission='sites.visual')
def visual(request):
    _ = request.translate

    site = get_site(request)
    if site is None:
        raise HTTPNotFound(explanation=_('Invalid site id'))

    breadcrumb = [
        (_('Sites'), request.route_path('sites.search')),
        (site.name, None),
    ]

    return dict(
        breadcrumb=breadcrumb,
        site=site,
        subtitle=site.name,
        title=_('Sites'),
    )
