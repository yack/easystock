# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import mimetypes

from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

from pyramid_helpers.forms import validate

from easystock.export import CSVDoc
from easystock.export import XLSXDoc
from easystock.forms import UploadForm
from easystock.scripts import users


log = logging.getLogger(__name__)


@view_config(route_name='imports.upload', renderer='/imports/upload.mako', permission='imports.upload')
@validate('upload', UploadForm)
def upload(request):
    _ = request.translate
    form = request.forms['upload']
    target = request.matchdict.get('target')

    modules = dict(
        users=dict(
            title=_('Users'),
            subtitle=_('Import user(s)'),
            prefix='USERS',
            main=request.route_path('users.search'),
            func=users.import_users,
            options=dict(
                create_only=True,
                sheet='Users',
            ),
        ),
    )

    module = modules.get(target)
    if module is None:
        raise HTTPNotFound()

    if not request.has_permission('{0}.import'.format(target)):
        raise HTTPForbidden(explanation=_('Not enough permission to access this resource'))

    if request.method == 'POST':
        if not form.errors:
            qfile = form.result['qfile']
            qfile_type = mimetypes.guess_type(qfile.filename)[0]

            if qfile_type == 'text/csv':
                doc = CSVDoc(qfile.file, delimiter=';', mode='r')
            elif qfile_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                doc = XLSXDoc(qfile.file, mode='r', title=form.result['sheet'])
            else:
                form.errors['qfile'] = _('Invalid file content')

        if 'qfile' in form.errors:
            # Move errors to filename field
            form.errors['filename'] = form.errors.pop('qfile')

        if not form.errors:
            # Get import function
            import_func = module['func']

            # Compute import options
            kwargs = dict()
            if module['options']['create_only']:
                kwargs['create_only'] = form.result['create_only']

                # Import saved file so that fp.name is correctly set
                with doc:
                    import_func(doc, author=request.authenticated_user, **kwargs)

            return HTTPFound(location=module['main'])
    else:
        form.set_data(module['options'])

    breadcrumb = [
        (module['title'], module['main']),
        (_('Import'), request.route_path('imports.upload', target=target)),
    ]

    return dict(
        breadcrumb=breadcrumb,
        cancel_link=module['main'],
        subtitle=module['subtitle'],
        target=target,
        title=module['title'],
    )
