# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from easystock.models import Area
from easystock.models import Asset
from easystock.models import DBSession
from easystock.models import Product
from easystock.models import Site
from easystock.models import Stock
from easystock.models import User


def get_area(request):
    area_id = request.matchdict.get('area')
    if area_id is None:
        return None

    return DBSession.query(Area).get(area_id)


def get_asset(request):
    asset_id = request.matchdict.get('asset')
    if asset_id is None:
        return None

    return DBSession.query(Asset).get(asset_id)


def get_product(request):
    product_id = request.matchdict.get('product')
    if product_id is None:
        return None

    return DBSession.query(Product).get(product_id)


def get_site(request):
    site_id = request.matchdict.get('site')
    if site_id is None:
        return None

    return DBSession.query(Site).get(site_id)


def get_stock(request):
    stock_id = request.matchdict.get('stock')
    if stock_id is None:
        return None

    return DBSession.query(Stock).get(stock_id)


def get_user(request):
    user_id = request.matchdict.get('user')
    if user_id is None:
        return None

    return DBSession.query(User).get(user_id)
