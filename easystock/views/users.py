# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import pytz
import unidecode

from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.response import Response
from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.export import export
from easystock.forms.users import ModifyForm
from easystock.forms.users import ProfileForm
from easystock.forms.users import SearchForm
from easystock.models import DBSession
from easystock.models import Site
from easystock.models import User
from easystock.scripts.users import export_users
from easystock.views.common import get_user

log = logging.getLogger(__name__)


def build_search_query(sort='id', order='asc', **criteria):
    """ Build a search query """

    query = DBSession.query(User).join(Site)

    # Filters
    if criteria.get('excluded_ids'):
        query = query.filter(User.id.notin(criteria['excluded_ids']))

    if criteria.get('selected_ids'):
        query = query.filter(User.id.in_(criteria['selected_ids']))

    if criteria.get('email'):
        query = query.filter(sa.func.unaccent(User.email).ilike('%{0}%'.format(unidecode.unidecode(criteria['email']))))

    if criteria.get('firstname'):
        query = query.filter(sa.func.unaccent(User.firstname).ilike('%{0}%'.format(unidecode.unidecode(criteria['firstname']))))

    if criteria.get('lastname'):
        query = query.filter(sa.func.unaccent(User.lastname).ilike('%{0}%'.format(unidecode.unidecode(criteria['lastname']))))

    if criteria.get('profile'):
        query = query.filter(User.profiles.any(criteria['profile']))

    if criteria.get('site'):
        query = query.filter(User.site == criteria['site'])

    if criteria.get('status'):
        query = query.filter(User.status == criteria['status'])

    if criteria.get('username'):
        query = query.filter(sa.func.unaccent(User.username).ilike('%{0}%'.format(unidecode.unidecode(criteria['username']))))

    # Order
    if sort == 'creation_date':
        order_by = [User.creation_date, User.username]
    elif sort == 'fullname':
        order_by = [User.firstname, User.lastname, User.username]
    elif sort == 'modification_date':
        order_by = [User.modification_date, User.username]
    elif sort == 'email':
        order_by = [User.email]
    elif sort == 'site':
        order_by = [Site.name, User.username]
    elif sort == 'status':
        order_by = [User.status, User.username]
    elif sort == 'username':
        order_by = [User.username]

    sa_func = sa.asc if order == 'asc' else sa.desc
    query = query.order_by(*[sa_func(column) for column in order_by])

    return query


@validate('modify', ModifyForm)
def _edit(request, user=None):
    _ = request.translate
    form = request.forms['modify']

    # Check whether we need to set password or not
    set_password = request.authentication_policy in ('basic', 'ticket')

    if user is None:
        breadcrumb = [
            (_('Users'), request.route_path('users.search')),
            (_('New user'), request.route_path('users.create')),
        ]
        subtitle = _('Create a new user')
        cancel_link = request.route_path('users.search')
    else:
        breadcrumb = [
            (_('Users'), request.route_path('users.search')),
            (_('Edition'), request.route_path('users.modify', user=user.id)),
        ]
        subtitle = _('User «{0}» edition').format(user.fullname)
        cancel_link = request.route_path('users.search')

    if request.method == 'POST':
        if not form.errors:
            # Check username unicity
            same_user = DBSession.query(User).filter_by(username=form.result['username']).first()
            if same_user and same_user != user:
                form.errors['username'] = _('Username already used by another user')

            # if new user, check that both password and password_confirm are not empty
            if set_password and user is None and form.result['password'] is None and form.result['password_confirm'] is None:
                form.errors['password'] = _('Please enter a value')
                form.errors['password_confirm'] = _('Please enter a value')

        if not form.errors:
            if user is None:
                user = User()
                DBSession.add(user)

            user.from_dict(form.result)

            return HTTPFound(location=request.route_path('users.search'))

    elif user:
        data = user.to_dict()
        form.set_data(data)

    else:
        data = dict(status='active')
        form.set_data(data)

    return dict(
        breadcrumb=breadcrumb,
        cancel_link=cancel_link,
        profiles=[(k, User.PROFILES[k]) for k in User.PROFILES],
        set_password=set_password,
        statuses=[(k, User.STATUSES[k]) for k in User.STATUSES],
        subtitle=subtitle,
        timezones=[(timezone, timezone) for timezone in pytz.all_timezones],
        title=_('Users'),
    )


@view_config(route_name='users.create', renderer='/users/modify.mako', permission='users.create')
def create(request):
    return _edit(request)


@view_config(route_name='users.modify', renderer='/users/modify.mako', permission='users.modify')
def modify(request):
    _ = request.translate

    user = get_user(request)
    if user is None:
        raise HTTPNotFound(explanation=_('Invalid user id'))

    return _edit(request, user=user)


@view_config(route_name='users.profile', renderer='/users/profile.mako', permission='users.profile')
@validate('profile', ProfileForm)
def profile(request):
    _ = request.translate
    form = request.forms['profile']

    user = request.authenticated_user

    # Check whether we need to set password or not
    set_password = request.authentication_policy in ('basic', 'ticket')

    breadcrumb = [
        (_('Profile'), request.route_path('users.profile')),
    ]
    subtitle = _('User «{0}» profile').format(user.fullname)
    cancel_link = request.route_path('index')

    if request.method == 'POST':
        if not form.errors:
            # Check username unicity
            same_user = DBSession.query(User).filter_by(username=form.result['username']).first()
            if same_user and same_user != user:
                form.errors['username'] = _('Username already used by another user')

        if not form.errors:
            user.from_dict(form.result)

            return HTTPFound(location=request.route_path('index'))
    else:
        data = user.to_dict()
        form.set_data(data)

    return dict(
        breadcrumb=breadcrumb,
        cancel_link=cancel_link,
        set_password=set_password,
        subtitle=subtitle,
        timezones=[(timezone, timezone) for timezone in pytz.all_timezones],
        title=_('Profile'),
    )


@view_config(route_name='users.search', renderer='/users/search.mako', permission='users.search')
@paginate('users', form='search', limit=10, sort='username', order='asc', partial_template='/users/list.mako', volatile_items=('format', 'search'))
@validate('search', SearchForm, method='get', persistent=True, volatile_items=('format', 'search'))
def search(request):
    """ Search users """
    _ = request.translate
    form = request.forms['search']
    pager = request.pagers['users']

    # Build query
    if not form.errors:
        query = build_search_query(sort=pager.sort, order=pager.order, **form.result)

        if form.result['format'] in ('csv', 'xlsx'):
            fp, content_type, filename = export('users', export_users, query, format=form.result['format'])

            response = Response(
                body_file=fp,
                content_encoding='utf-8',
                content_type=content_type,
                content_disposition='attachment; filename="{0}"'.format(filename)
            )

            return response

        pager.set_collection(query)

    if pager.partial:
        return dict()

    breadcrumb = [
        (_('Users'), request.route_path('users.search')),
    ]

    return dict(
        breadcrumb=breadcrumb,
        profiles=[(k, User.PROFILES[k]) for k in User.PROFILES],
        statuses=[(k, User.STATUSES[k]) for k in User.STATUSES],
        subtitle=_('Find users'),
        title=_('Users'),
    )
