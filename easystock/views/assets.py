# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import unidecode

from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.forms.assets import SearchForm
from easystock.models import DBSession
from easystock.models import Area
from easystock.models import Asset
from easystock.models import Product
from easystock.views.common import get_asset

log = logging.getLogger(__name__)


def build_search_query(sort='id', order='asc', **criteria):
    """ Build a search query """

    query = DBSession.query(Asset).join(Area).join(Product)

    # Filters
    if criteria.get('excluded_ids'):
        query = query.filter(Asset.id.notin(criteria['excluded_ids']))

    if criteria.get('selected_ids'):
        query = query.filter(Asset.id.in_(criteria['selected_ids']))

    if criteria.get('area'):
        query = query.filter(Asset.area == criteria['area'])

    if criteria.get('category'):
        query = query.filter(Product.category == criteria['category'])

    if criteria.get('label'):
        query = query.filter(sa.func.unaccent(Product.label).ilike('%{0}%'.format(unidecode.unidecode(criteria['label']))))

    if criteria.get('product'):
        query = query.filter(Asset.product == criteria['product'])

    if criteria.get('serial_number'):
        query = query.filter(sa.func.unaccent(Asset.serial_number).ilike('%{0}%'.format(unidecode.unidecode(criteria['serial_number']))))

    if criteria.get('site'):
        query = query.filter(Area.site == criteria['site'])

    if criteria.get('state'):
        query = query.filter(Asset.state == criteria['state'])

    # Order
    if sort == 'area':
        order_by = [Area.building, Area.floor, Area.room, Product.label]
    elif sort == 'brand':
        order_by = [Product.brand, Product.label]
    elif sort == 'category':
        order_by = [Product.category, Product.label]
    elif sort == 'creation_date':
        order_by = [Asset.creation_date, Product.label]
    elif sort == 'id':
        order_by = [Asset.id]
    elif sort == 'inventory_date':
        order_by = [Asset.inventory_date, Product.label]
    elif sort == 'label':
        order_by = [Product.label]
    elif sort == 'model':
        order_by = [Product.model, Product.label]
    elif sort == 'modification_date':
        order_by = [Asset.modification_date, Product.label]
    elif sort == 'serial_number':
        order_by = [Asset.serial_number, Product.label]
    elif sort == 'state':
        order_by = [Asset.state, Product.label]

    sa_func = sa.asc if order == 'asc' else sa.desc
    query = query.order_by(*[sa_func(column).nullsfirst() for column in order_by])

    return query


@view_config(route_name='assets.search', renderer='/assets/search.mako', permission='assets.search')
@paginate('assets', form='search', limit=20, sort='inventory_date', order='desc', partial_template='/assets/list.mako', volatile_items=('format', 'search'))
@validate('search', SearchForm, method='get', persistent=True, volatile_items=('format', 'search'))
def search(request):
    _ = request.translate
    form = request.forms['search']
    pager = request.pagers['assets']

    # Build query
    if not form.errors:
        query = build_search_query(sort=pager.sort, order=pager.order, site=request.authenticated_user.site, **form.result)

        pager.set_collection(query)

        if pager.item_count == 1 and 'search' in request.params:
            # Only one asset is matching search criteria, bouncing to visual page
            asset = query.first()
            return HTTPFound(location=request.route_path('assets.visual', asset=asset.id))

    if pager.partial:
        return dict()

    breadcrumb = [
        (_('Assets'), None),
    ]

    # Get categories
    query = DBSession.query(sa.distinct(Product.category)).join(Asset).join(Area).filter(
        Area.site == request.authenticated_user.site,
    ).order_by(Product.category)

    categories = [(category, category) for category, in query]

    return dict(
        breadcrumb=breadcrumb,
        categories=categories,
        states=[(k, Asset.STATES[k]) for k in Asset.STATES],
        subtitle=_('Find assets'),
        title=_('Assets'),
    )


@view_config(route_name='assets.visual', renderer='/assets/visual.mako', permission='assets.visual')
def visual(request):
    _ = request.translate

    asset = get_asset(request)
    if asset is None:
        raise HTTPNotFound(explanation=_('Invalid asset Id'))

    breadcrumb = [
        (_('Assets'), request.route_path('assets.search')),
        (asset.code, None),
    ]

    return dict(
        breadcrumb=breadcrumb,
        asset=asset,
        states=[(k, Asset.STATES[k]) for k in Asset.STATES],
        subtitle='{0} - {1}'.format(asset.code, asset.product.label),
        title=_('Assets'),
    )
