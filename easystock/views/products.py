# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import unidecode

from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.forms.products import ModifyForm
from easystock.forms.products import SearchForm
from easystock.models import DBSession
from easystock.models import Product
from easystock.views.common import get_product

log = logging.getLogger(__name__)


def build_search_query(sort='id', order='asc', **criteria):
    """ Build a search query """

    query = DBSession.query(Product)

    # Filters
    if criteria.get('excluded_ids'):
        query = query.filter(Product.id.notin(criteria['excluded_ids']))

    if criteria.get('selected_ids'):
        query = query.filter(Product.id.in_(criteria['selected_ids']))

    if criteria.get('brand'):
        query = query.filter(Product.brand == criteria['brand'])

    if criteria.get('category'):
        query = query.filter(Product.category == criteria['category'])

    if criteria.get('label'):
        query = query.filter(sa.func.unaccent(Product.label).ilike('%{0}%'.format(unidecode.unidecode(criteria['label']))))

    if criteria.get('model'):
        query = query.filter(sa.func.unaccent(Product.model).ilike('%{0}%'.format(unidecode.unidecode(criteria['model']))))

    if criteria.get('status'):
        query = query.filter(Product.status == criteria['status'])

    if criteria.get('term'):
        query = query.filter(
            sa.or_(
                sa.func.unaccent(Product.label).ilike('%{0}%'.format(unidecode.unidecode(criteria['term']))),
                sa.func.unaccent(Product.brand).ilike('%{0}%'.format(unidecode.unidecode(criteria['term']))),
                sa.func.unaccent(Product.model).ilike('%{0}%'.format(unidecode.unidecode(criteria['term']))),
            )
        )

    # Order
    if sort == 'brand':
        order_by = [Product.brand, Product.label, Product.model]
    elif sort == 'category':
        order_by = [Product.category, Product.label, Product.brand, Product.model]
    elif sort == 'creation_date':
        order_by = [Product.creation_date, Product.label, Product.brand, Product.model]
    elif sort == 'id':
        order_by = [Product.id]
    elif sort == 'label':
        order_by = [Product.label, Product.brand, Product.model]
    elif sort == 'model':
        order_by = [Product.model, Product.label, Product.brand]
    elif sort == 'modification_date':
        order_by = [Product.modification_date, Product.label, Product.brand, Product.model]
    elif sort == 'status':
        order_by = [Product.status, Product.label, Product.brand, Product.model]

    sa_func = sa.asc if order == 'asc' else sa.desc
    query = query.order_by(*[sa_func(column).nullsfirst() for column in order_by])

    return query


@validate('modify', ModifyForm)
def _edit(request, product=None, duplicate=False):
    _ = request.translate
    form = request.forms['modify']

    if product is None:
        breadcrumb = [
            (_('Products'), request.route_path('products.search')),
            (_('Create'), None),
        ]
        subtitle = _('Create new product')
        cancel_link = request.route_path('products.search')

    elif duplicate:
        breadcrumb = [
            (_('Products'), request.route_path('products.search')),
            (product.label, request.route_path('products.visual', product=product.id)),
            (_('Duplicate'), None),
        ]
        subtitle = _('Duplicate product «{0}»').format(product.label)
        cancel_link = request.route_path('products.visual', product=product.id)

    else:
        breadcrumb = [
            (_('Products'), request.route_path('products.search')),
            (product.label, request.route_path('products.visual', product=product.id)),
            (_('Modify'), None),
        ]
        subtitle = _('Modify product «{0}»').format(product.label)
        cancel_link = request.route_path('products.visual', product=product.id)

    if request.method == 'POST':
        if not form.errors:
            same_product = DBSession.query(Product).filter(
                sa.and_(
                    Product.label == form.result['label'],
                    Product.brand == form.result['brand'],
                    Product.model == form.result['model'],
                )
            ).first()
            if same_product and (same_product != product or duplicate):
                form.errors['label'] = _('Label is already used by another product')

        if not form.errors:
            if product is None or duplicate:
                product = Product()
                DBSession.add(product)

            product.from_dict(form.result)

            # Get newly created object's id
            DBSession.flush()

            return HTTPFound(request.route_path('products.visual', product=product.id))

    elif product:
        data = product.to_dict()
        if duplicate:
            data['barcode'] = None
            data['model'] = None

        form.set_data(data)

    else:
        data = dict(status='available')
        form.set_data(data)

    brands = [(brand, brand) for brand, in DBSession.query(sa.distinct(Product.brand)).filter(Product.brand.isnot(None)).order_by(Product.brand)]
    categories = [(category, category) for category, in DBSession.query(sa.distinct(Product.category)).filter(Product.category.isnot(None)).order_by(Product.category)]

    return dict(
        brands=brands,
        breadcrumb=breadcrumb,
        cancel_link=cancel_link,
        categories=categories,
        statuses=[(k, Product.STATUSES[k]) for k in Product.STATUSES],
        subtitle=subtitle,
        title=_('Products'),
    )


@view_config(route_name='products.create', renderer='/products/modify.mako', permission='products.create')
def create(request):
    return _edit(request)


@view_config(route_name='products.duplicate', renderer='/products/modify.mako', permission='products.create')
def duplicate(request):
    _ = request.translate

    product = get_product(request)
    if product is None:
        raise HTTPNotFound(explanation=_('Invalid product Id'))

    return _edit(request, product=product, duplicate=True)


@view_config(route_name='products.modify', renderer='/products/modify.mako', permission='products.modify')
def modify(request):
    _ = request.translate

    product = get_product(request)
    if product is None:
        raise HTTPNotFound(explanation=_('Invalid product Id'))

    return _edit(request, product=product)


@view_config(route_name='products.search', renderer='/products/search.mako', permission='products.search')
@paginate('products', form='search', limit=20, sort='modification_date', order='desc', partial_template='/products/list.mako', volatile_items=('format', 'search'))
@validate('search', SearchForm, method='get', persistent=True, volatile_items=('format', 'search'))
def search(request):
    _ = request.translate
    form = request.forms['search']
    pager = request.pagers['products']

    # Build query
    if not form.errors:
        query = build_search_query(sort=pager.sort, order=pager.order, **form.result)

        pager.set_collection(query)

        if pager.item_count == 1 and 'search' in request.params:
            # Only one product is matching search criteria, bouncing to visual page
            product = query.first()
            return HTTPFound(location=request.route_path('products.visual', product=product.id))

    if pager.partial:
        return dict()

    breadcrumb = [
        (_('Products'), None),
    ]

    brands = [(brand, brand) for brand, in DBSession.query(sa.distinct(Product.brand)).filter(Product.brand.isnot(None)).order_by(Product.brand)]
    categories = [(category, category) for category, in DBSession.query(sa.distinct(Product.category)).filter(Product.category.isnot(None)).order_by(Product.category)]

    return dict(
        brands=brands,
        breadcrumb=breadcrumb,
        categories=categories,
        statuses=[(k, Product.STATUSES[k]) for k in Product.STATUSES],
        subtitle=_('Find products'),
        title=_('Products'),
    )


@view_config(route_name='products.visual', renderer='/products/visual.mako', permission='products.visual')
def visual(request):
    _ = request.translate

    product = get_product(request)
    if product is None:
        raise HTTPNotFound(explanation=_('Invalid product Id'))

    breadcrumb = [
        (_('Products'), request.route_path('products.search')),
        (product.label, None),
    ]

    return dict(
        breadcrumb=breadcrumb,
        product=product,
        subtitle=product.label,
        title=_('Products'),
    )
