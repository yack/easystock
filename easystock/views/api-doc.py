# -*- coding: utf-8 -*-

""" API REST interactive documentation view """

import logging

from pyramid.view import view_config
from pyramid_helpers.api_doc import api_doc

log = logging.getLogger(__name__)


@view_config(route_name='api-doc.index', renderer='')
def index(request):
    _ = request.translate
    renderer_input = dict(
        breadcrumb=[(_('API documentation'), None)],
        subtitle=None,
        title=_('API documentation'),
    )

    return api_doc(request, renderer_input, 'easystock')
