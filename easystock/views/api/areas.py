# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Areas API """

import logging
import unidecode

from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.forms import AutocompleteForm
from easystock.models import DBSession
from easystock.models import Area
from easystock.views.common import get_area

log = logging.getLogger(__name__)


def build_search_query(sort='id', order='asc', **criteria):
    """ Build a search query """

    query = DBSession.query(Area)

    # Filtering
    if criteria.get('excluded_ids'):
        query = query.filter(Area.id.notin(criteria['excluded_ids']))

    if criteria.get('selected_ids'):
        query = query.filter(Area.id.in_(criteria['selected_ids']))

    if criteria.get('site'):
        query = query.filter(Area.site == criteria['site'])

    if criteria.get('term'):
        query = query.filter(sa.func.unaccent(Area.room).ilike('%{0}%'.format(unidecode.unidecode(criteria['term']))))

    # Ordering
    query = query.order_by(
        Area.building,
        Area.floor,
        Area.room,
    )

    return query


@view_config(route_name='api.areas.autocomplete', renderer='json', request_method='GET', permission='areas.visual')
@paginate('areas', limit=10, sort='room', order='asc')
@validate('autocomplete', AutocompleteForm, method='get')
def autocomplete(request):
    """ Area autocompletion """

    _ = request.translate
    form = request.forms['autocomplete']
    pager = request.pagers['areas']

    result = dict(
        url=request.url,
        method=request.method,
        params=form.decoded,
        apiVersion='1.0',
        result=True,
    )

    if form.errors:
        result['message'] = _('Invalid or missing parameter')
        result['errors'] = form.errors
        result['result'] = False
        return result

    # Build query
    query = build_search_query(sort=pager.sort, order=pager.order, site=request.authenticated_user.site, **form.result)

    pager.set_collection(query)

    # Compute result
    result['data'] = dict(
        items=[area.to_json(context='api.areas.autocomplete') for area in pager],
        limit=pager.limit,
        page=pager.page,
        item_count=pager.item_count,
        page_count=pager.page_count,
    )

    return result


@view_config(route_name='api.areas.delete', renderer='json', request_method='DELETE', permission='areas.delete')
def delete(request):
    """ Removes a area """

    _ = request.translate

    area = get_area(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if area is None:
        result['message'] = _('Invalid area id')
        result['result'] = False
    elif area.assets.count():
        result['message'] = _('Area is associated with existing assets')
        result['result'] = False
    elif area.stocks.count():
        result['message'] = _('Area is associated with existing stocks')
        result['result'] = False
    else:
        room = area.room
        DBSession.delete(area)
        result['message'] = _('Successfully deleted area «{0}»').format(room)
        result['location'] = request.route_path('areas.search')

    return result


@view_config(route_name='api.areas.get', renderer='json', request_method='GET', permission='areas.visual')
def get(request):
    """ Get a area """

    _ = request.translate

    area = get_area(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if area is None:
        result['message'] = _('Invalid area id')
        result['result'] = False
    else:
        result['data'] = area.to_json(context='api.areas.get')

    return result
