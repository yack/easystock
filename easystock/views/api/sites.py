# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Sites API """

import logging

from pyramid.view import view_config

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.forms import AutocompleteForm
from easystock.models import DBSession
from easystock.views.common import get_site
from easystock.views.sites import build_search_query

log = logging.getLogger(__name__)


@view_config(route_name='api.sites.autocomplete', renderer='json', request_method='GET', permission='sites.visual')
@paginate('sites', limit=10, sort='name', order='asc')
@validate('autocomplete', AutocompleteForm, method='get')
def autocomplete(request):
    """ Site autocompletion """

    _ = request.translate
    form = request.forms['autocomplete']
    pager = request.pagers['sites']

    result = dict(
        url=request.url,
        method=request.method,
        params=form.decoded,
        apiVersion='1.0',
        result=True,
    )

    if form.errors:
        result['message'] = _('Invalid or missing parameter')
        result['errors'] = form.errors
        result['result'] = False
        return result

    # Build query
    query = build_search_query(sort=pager.sort, order=pager.order, **form.result)

    pager.set_collection(query)

    # Compute result
    result['data'] = dict(
        items=[site.to_json(context='api.sites.autocomplete') for site in pager],
        limit=pager.limit,
        page=pager.page,
        item_count=pager.item_count,
        page_count=pager.page_count,
    )

    return result


@view_config(route_name='api.sites.delete', renderer='json', request_method='DELETE', permission='sites.delete')
def delete(request):
    """ Removes a site """

    _ = request.translate

    site = get_site(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if site is None:
        result['message'] = _('Invalid site id')
        result['result'] = False
    elif site.assets.count():
        result['message'] = _('Site is associated with existing assets')
        result['result'] = False
    elif site.stocks.count():
        result['message'] = _('Site is associated with existing stocks')
        result['result'] = False
    else:
        name = site.name
        DBSession.delete(site)
        result['message'] = _('Successfully deleted site «{0}»').format(name)
        result['location'] = request.route_path('sites.search')

    return result


@view_config(route_name='api.sites.get', renderer='json', request_method='GET', permission='sites.visual')
def get(request):
    """ Get a site """

    _ = request.translate

    site = get_site(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if site is None:
        result['message'] = _('Invalid site id')
        result['result'] = False
    else:
        result['data'] = site.to_json(context='api.sites.get')

    return result
