# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Products API """

import logging

from pyramid.view import view_config

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from easystock.forms import AutocompleteForm
from easystock.models import DBSession
from easystock.views.common import get_product
from easystock.views.products import build_search_query

log = logging.getLogger(__name__)


@view_config(route_name='api.products.autocomplete', renderer='json', request_method='GET', permission='products.visual')
@paginate('products', limit=10, sort='label', order='asc')
@validate('autocomplete', AutocompleteForm, method='get')
def autocomplete(request):
    """ Product autocompletion """

    _ = request.translate
    form = request.forms['autocomplete']
    pager = request.pagers['products']

    result = dict(
        url=request.url,
        method=request.method,
        params=form.decoded,
        apiVersion='1.0',
        result=True,
    )

    if form.errors:
        result['message'] = _('Invalid or missing parameter')
        result['errors'] = form.errors
        result['result'] = False
        return result

    # Build query
    query = build_search_query(sort=pager.sort, order=pager.order, **form.result)

    pager.set_collection(query)

    # Compute result
    result['data'] = dict(
        items=[product.to_json(context='api.products.autocomplete') for product in pager],
        limit=pager.limit,
        page=pager.page,
        item_count=pager.item_count,
        page_count=pager.page_count,
    )

    return result


@view_config(route_name='api.products.delete', renderer='json', request_method='DELETE', permission='products.delete')
def delete(request):
    """ Removes a product """

    _ = request.translate

    product = get_product(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if product is None:
        result['message'] = _('Invalid product id')
        result['result'] = False
    elif product.assets.count():
        result['message'] = _('Product is associated with existing assets')
        result['result'] = False
    elif product.stocks.count():
        result['message'] = _('Product is associated with existing stocks')
        result['result'] = False
    else:
        label = product.label
        DBSession.delete(product)
        result['message'] = _('Successfully deleted product «{0}»').format(label)
        result['location'] = request.route_path('products.search')

    return result


@view_config(route_name='api.products.get', renderer='json', request_method='GET', permission='products.visual')
def get(request):
    """ Get a product """

    _ = request.translate

    product = get_product(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if product is None:
        result['message'] = _('Invalid product id')
        result['result'] = False
    else:
        result['data'] = product.to_json(context='api.products.get')

    return result
