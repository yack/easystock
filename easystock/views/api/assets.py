# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Assets API """

import datetime
import logging

from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate

from easystock.forms.assets import CreateForm
from easystock.forms.assets import UpdateForm
from easystock.models import DBSession
from easystock.models import Asset
from easystock.views.common import get_asset

log = logging.getLogger(__name__)


# TODO
# Check if authenticated user is allowed to add asset for product/author/site
@view_config(route_name='api.assets.create', renderer='json', request_method='PUT', permission='assets.create')
@validate('create', CreateForm)
def create(request):
    """ Create a new asset object for product """

    _ = request.translate
    form = request.forms['create']

    result = dict(
        url=request.url,
        method=request.method,
        params=form.decoded,
        apiVersion='1.0',
        result=True,
    )

    if not form.errors:
        # Get data from form result
        area = form.result['area']
        author = form.result['author']
        date = form.result['date']
        product = form.result['product']
        serial_number = form.result['serial_number']
        state = form.result['state']

        if date is None:
            date = datetime.datetime.utcnow()

        # Check serial number unicity
        if serial_number:
            same_asset = DBSession.query(Asset).filter(
                sa.and_(
                    Asset.product == product,
                    Asset.serial_number == serial_number,
                )
            ).first()
            if same_asset:
                form.errors['serial_number'] = _('An asset already exists for this serial number ({0})').format(same_asset.code)

    if not form.errors:
        # Add new stock record
        asset = Asset()

        asset.area = area
        asset.author = author
        asset.product = product

        asset.creation_date = date
        asset.modification_date = date
        asset.inventory_date = date

        asset.serial_number = serial_number
        asset.state = state

        DBSession.add(asset)

        # Get newly created object's id
        DBSession.flush()

        result['message'] = _('Successsfully added asset for product «{0}»').format(product.label)
        result['location'] = request.route_path('assets.visual', asset=asset.id)

    else:
        result['message'] = _('Invalid or missing parameter')
        result['errors'] = form.errors
        result['result'] = False

    return result


@view_config(route_name='api.assets.delete', renderer='json', request_method='DELETE', permission='assets.delete')
def delete(request):
    """ Removes an asset """

    _ = request.translate

    asset = get_asset(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if asset is None:
        result['message'] = _('Invalid asset id')
        result['result'] = False
    else:
        code = asset.code
        product = asset.product
        DBSession.delete(asset)
        result['message'] = _('Successfully deleted asset «{0}»').format(code)
        result['location'] = request.route_path('products.visual', product=product.id)

    return result


@view_config(route_name='api.assets.update', renderer='json', request_method='POST', permission='assets.update')
@validate('update', UpdateForm)
def update(request):
    """ Update an existing asset object """

    _ = request.translate
    form = request.forms['update']

    result = dict(
        url=request.url,
        method=request.method,
        params=form.decoded,
        apiVersion='1.0',
        result=True,
    )

    asset = get_asset(request)
    if asset is None:
        result['message'] = _('Invalid asset Id')
        result['result'] = False
        request.response.status_int = 404
        return result

    if not form.errors:
        # Get data from form result
        area = form.result['area']
        date = form.result['date']
        serial_number = form.result['serial_number']
        state = form.result['state']

        if date is None:
            date = datetime.datetime.utcnow()

        # Check serial number unicity
        if serial_number:
            same_asset = DBSession.query(Asset).filter(
                sa.and_(
                    Asset.product == asset.product,
                    Asset.serial_number == serial_number,
                )
            ).first()
            if same_asset and same_asset != asset:
                form.errors['serial_number'] = _('An asset already exists for this serial number ({0})').format(same_asset.code)

    if not form.errors:
        asset.area = area
        asset.inventory_date = date
        asset.serial_number = serial_number
        asset.state = state

        result['message'] = _('Successsfully updated asset «{0}»').format(asset.code)
        result['location'] = request.route_path('assets.visual', asset=asset.id)

        # TODO: timeline, author
    else:
        result['message'] = _('Invalid or missing parameter')
        result['errors'] = form.errors
        result['result'] = False

    return result
