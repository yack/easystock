# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Stocks API """

import datetime
import logging

from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate

from easystock.forms.stocks import CreateForm
from easystock.models import DBSession
from easystock.models import Stock
from easystock.views.common import get_stock

log = logging.getLogger(__name__)


# TODO
# Check if authenticated user is allowed to add record for product/author/area
@view_config(route_name='api.stocks.create', renderer='json', request_method='PUT', permission='stocks.create')
@validate('create', CreateForm)
def create(request):
    """ Create new stock operation """

    _ = request.translate
    form = request.forms['create']

    result = dict(
        url=request.url,
        method=request.method,
        params=form.decoded,
        apiVersion='1.0',
        result=True,
    )

    if not form.errors:
        utcnow = datetime.datetime.utcnow()

        # Get data from form result
        action = form.result['action']
        area = form.result['area']
        author = form.result['author']
        date = form.result['date']
        message = form.result['message']
        product = form.result['product']
        value = form.result['value']

        # Get last stock record for site/area/product
        last_stock = DBSession.query(Stock).filter(
            sa.and_(
                Stock.area == area,
                Stock.product == product,
            )
        ).order_by(sa.desc(Stock.date)).first()

        # Check date
        if date:
            if last_stock and date < last_stock.date:
                form.errors = dict(date=_('Value is before last stock entry'))
        else:
            date = utcnow

    if not form.errors:
        # Add new stock record
        stock = Stock()

        stock.author = author
        stock.product = product

        stock.area = area
        stock.date = date
        stock.action = action
        stock.message = message

        if action == 'stock':
            stock.total = value
            stock.value = value - last_stock.total if last_stock else value
        elif action == 'out':
            stock.value = value
            stock.total = last_stock.total - value if last_stock else -value
        elif action == 'in':
            stock.value = value
            stock.total = last_stock.total + value if last_stock else value

        DBSession.add(stock)

        result['message'] = _('Successsfully added stock record to product «{0}»').format(product.label)
        result['location'] = request.route_path('products.visual', product=product.id)

    else:
        result['message'] = _('Invalid or missing parameter')
        result['errors'] = form.errors
        result['result'] = False

    return result


@view_config(route_name='api.stocks.delete', renderer='json', request_method='DELETE', permission='stocks.delete')
def delete(request):
    """ Removes a stock """

    _ = request.translate

    stock = get_stock(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if stock is None:
        result['message'] = _('Invalid stock id')
        result['result'] = False

    elif stock.next:
        result['message'] = _('Only last stock operation can be deleted')
        result['result'] = False

    else:
        product = stock.product
        DBSession.delete(stock)
        result['message'] = _('Successfully deleted last stock operation for product «{0}»').format(product.label)
        result['location'] = request.route_path('products.visual', product=product.id)

    return result
