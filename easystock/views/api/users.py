# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Users API """

import logging

from pyramid.view import view_config

from easystock.models import DBSession
from easystock.views.common import get_user

log = logging.getLogger(__name__)


@view_config(route_name='api.users.delete', renderer='json', request_method='DELETE', permission='users.delete')
def delete(request):
    """ Removes a User """

    _ = request.translate

    user = get_user(request)

    result = dict(
        url=request.url,
        method=request.method,
        apiVersion='1.0',
        result=True,
    )

    if user is None:
        result['message'] = _('Invalid user id')
        result['result'] = False
    elif user == request.authenticated_user:
        result['message'] = _('Forbidden user id')
        result['result'] = False
        request.response.status_int = 403
    elif user.assets.count():
        result['message'] = _('User is associated with existing assets')
        result['result'] = False
    elif user.stocks.count():
        result['message'] = _('User is associated with existing stocks')
        result['result'] = False
    else:
        fullname = user.fullname
        DBSession.delete(user)
        result['message'] = _('Successfully deleted user «{0}»').format(fullname)
        result['reload'] = True

    return result
