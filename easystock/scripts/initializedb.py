# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import ArgumentParser
import datetime
import sys
import transaction

from pyramid.config import Configurator
from pyramid.paster import get_appsettings
from pyramid.paster import setup_logging

from easystock.models import DBSession
from easystock.models import Site
from easystock.models import User


def main(argv=None):

    if argv is None:
        argv = sys.argv[1:]

    parser = ArgumentParser(description='Database initialization script')

    parser.add_argument(
        'config_uri',
        nargs='?',
        help='Configuration file to use.'
    )

    args = parser.parse_args(argv)

    setup_logging(args.config_uri)

    try:
        # Production
        settings = get_appsettings(args.config_uri, name='easystock')
    except LookupError:
        # Development
        settings = get_appsettings(args.config_uri)

    config = Configurator(settings=settings)

    # Model setup
    config.include('easystock.models')

    with transaction.manager:
        utcnow = datetime.datetime.utcnow()

        site = DBSession.query(Site).filter(Site.name == 'Main site').first()
        if site is None:
            site = Site()
            site.creation_date = utcnow
            site.modification_date = utcnow
            site.name = 'Main site'
            site.status = 'active'

            DBSession.add(site)

        user = DBSession.query(User).filter(User.username == 'admin').first()
        if user is None:
            user = User()
            user.creation_date = utcnow
            user.modification_date = utcnow
            user.status = 'active'

            user.site = site

            user.username = 'admin'
            user.firstname = 'John'
            user.lastname = 'Doe'
            user.profiles = ['admin', ]

            user.set_password('admin')

            DBSession.add(user)
