# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Import/export users script through CSV or XLSX file """

from argparse import ArgumentParser
import datetime
import logging
import os
import sys
import transaction

from pyramid.paster import get_appsettings
from pyramid.paster import setup_logging
from pyramid.config import Configurator

from easystock.export import CSVDoc
from easystock.export import XLSXDoc
from easystock.export import export
from easystock.models import DBSession
from easystock.models import Site
from easystock.models import User
from easystock.models import UserTimeline

log = logging.getLogger(__name__)

EXP_INTERFACE = 'USER'
EXP_VERSION = 'V1'
EXP_HEADER = [
    'interface',
    'version',
    'status',
    'username',
    'firstname',
    'lastname',
    'email',
    'site',
    'timezone',
    'profiles',
    'password',
    'creation_date',
    'modification_date',
]

# CSV columns
COL_INTERFACE = 0
COL_VERSION = 1
COL_STATUS = 2
COL_USERNAME = 3
COL_FIRSTNAME = 4
COL_LASTNAME = 5
COL_EMAIL = 6
COL_SITE = 7
COL_TIMEZONE = 8
COL_PROFILES = 9
COL_PASSWORD = 10
COL_CREATION_DATE = 11
COL_MODIFICATION_DATE = 12
COL_EOL = 13

# Minimal row length to accept
COL_MIN = COL_PROFILES + 1


def export_users(query, doc, add_headers=True):

    doc.clear()

    # Add header
    if add_headers:
        doc.append(EXP_HEADER)

    for user in query:
        # Get data from user
        data = user.to_dict(context='export')

        # Add export columns
        data['interface'] = EXP_INTERFACE
        data['version'] = EXP_VERSION

        # Convert some items to suitable format
        data['site'] = data['site'].name
        data['profiles'] = ', '.join(data['profiles']) if data['profiles'] else None

        # Create row
        row = [None for _ in range(COL_EOL)]

        # Export row
        row = [
            data.get(column)
            for column in EXP_HEADER
        ]

        doc.append(row)


def import_users(doc, author=None, create_only=False):

    users_create = []
    users_delete = []
    users_modify = []

    utcnow = datetime.datetime.utcnow()
    for lineno, row in doc.safe_iterator(COL_INTERFACE, COL_VERSION, EXP_INTERFACE, EXP_VERSION, col_eol=COL_EOL, col_min=COL_MIN):
        # Get site
        site = DBSession.query(Site).filter(Site.name == row[COL_SITE]).first()
        if site is None:
            log.error('[%s:%06d] [INVALID] Site «%s» is missing from database', doc.filename, lineno, row[COL_SITE])
            continue

        # Get user
        user = DBSession.query(User).filter(User.username == row[COL_USERNAME]).first()

        status = row[COL_STATUS].lower()
        if status == 'deleted':
            # DELETE
            if user is None:
                continue

            log.info('[%s:%06d] [DELETE ] Deleting user «%s»', doc.filename, lineno, user.fullname)

            users_delete.append(user.fullname)

            DBSession.delete(user)
            continue

        # CREATE/UPDATE
        if user is None:
            user = User()
            DBSession.add(user)

        elif create_only:
            log.info('[%s:%06d] [SKIP   ] Skipping existing user «%s»', doc.filename, lineno, user.fullname)
            continue

        data = dict(
            username=row[COL_USERNAME],

            creation_date=row[COL_CREATION_DATE] if row[COL_CREATION_DATE] else None,
            modification_date=row[COL_MODIFICATION_DATE] if row[COL_MODIFICATION_DATE] else None,
            status=row[COL_STATUS],

            firstname=row[COL_FIRSTNAME] if row[COL_FIRSTNAME] else None,
            lastname=row[COL_LASTNAME] if row[COL_LASTNAME] else None,
            email=row[COL_EMAIL] if row[COL_EMAIL] else None,

            site=site,
            timezone=row[COL_TIMEZONE] if row[COL_TIMEZONE] else None,
            profiles=[profile.strip() for profile in row[COL_PROFILES].split(',') if profile.strip()] if row[COL_PROFILES] else [],
            password=row[COL_PASSWORD] if row[COL_PASSWORD] else None,
        )

        is_modified = user.from_dict(data)

        if user.id is None:
            log.info('[%s:%06d] [CREATE ] Creating user «%s»', doc.filename, lineno, user.fullname)
            users_create.append(user)

        elif is_modified:
            log.info('[%s:%06d] [UPDATE ] Updating user «%s»', doc.filename, lineno, user.fullname)
            users_modify.append(user)

    # Timelines
    if users_create:
        timeline = UserTimeline()
        timeline.action = 'create'
        timeline.author = author
        timeline.date = utcnow
        timeline.message = 'Created new user(s)'
        timeline.targets = users_create

        DBSession.add(timeline)

    if users_delete:
        timeline = UserTimeline()
        timeline.action = 'delete'
        timeline.author = author
        timeline.date = utcnow
        timeline.message = 'Deleted user(s) {0}.'.format(', '.join(users_delete))

        DBSession.add(timeline)

    if users_modify:
        timeline = UserTimeline()
        timeline.action = 'modify'
        timeline.author = author
        timeline.date = utcnow
        timeline.message = 'Modified user(s)'
        timeline.targets = users_modify

        DBSession.add(timeline)


def main(argv=None):

    if argv is None:
        argv = sys.argv[1:]

    parser = ArgumentParser(description='Users import/export script')

    parser.add_argument(
        'config_uri',
        nargs='?',
        help='Configuration file to use.'
    )

    parser.add_argument(
        '-c', '--create-only',
        action='store_true',
        help='Only create missing users (default is to create missing users and update existing ones).'
    )

    parser.add_argument(
        '-e', '--export-file',
        help='CSV file path to export users results to'
    )

    parser.add_argument(
        '-f', '--force',
        action='store_true',
        help='Overwrite any existing file when exporting users results'
    )

    parser.add_argument(
        '-i', '--import-file',
        help='CSV file path to import users from'
    )

    parser.add_argument(
        '-u', '--user',
        default='admin',
        help='User for timeline (default is admin)'
    )

    args = parser.parse_args(argv)

    setup_logging(args.config_uri)

    try:
        # Production
        settings = get_appsettings(args.config_uri, name='easystock')
    except LookupError:
        # Development
        settings = get_appsettings(args.config_uri)

    config = Configurator(settings=settings)

    # Model setup
    config.include('easystock.models')

    # Export setup
    config.include('easystock.export')

    # Check user
    args.author = DBSession.query(User).filter(User.username == args.user).first()
    if args.author is None:
        parser.error('Invalid user {0}'.format(args.user))

    if args.import_file is None and args.export_file is None:
        parser.error('Nothing to do. Please set --import-file or --export-file.')

    # Import
    if args.import_file:
        filepath = os.path.abspath(args.import_file)
        if not os.path.isfile(filepath):
            parser.error('Missing import file {0}'.format(filepath))

        if not os.access(filepath, os.R_OK):
            parser.error('Not enough permissions to access import file {0}'.format(filepath))

        _, extension = os.path.splitext(filepath)
        kind = extension[1:].lower()
        if kind not in ('csv', 'xlsx'):
            parser.error('Unknown extension {0}'.format(extension))

        with transaction.manager:
            with open(filepath, 'br') as fp:
                if kind == 'csv':
                    doc = CSVDoc(fp, delimiter=';', mode='r')
                elif kind == 'xlsx':
                    doc = XLSXDoc(fp, mode='r', title='Users')

                with doc:
                    import_users(doc, author=args.author, create_only=args.create_only)

    # Export
    if args.export_file:
        filepath = os.path.abspath(args.export_file)
        if os.path.isfile(filepath) and not args.force:
            parser.error('Export file already exists, please use --force option to overwrite')

        _, extension = os.path.splitext(filepath)
        kind = extension[1:].lower()
        if kind not in ('csv', 'xlsx'):
            parser.error('Unknown extension {0}'.format(extension))

        with transaction.manager:
            query = DBSession.query(User)
            query = query.order_by(User.username)

            with open(filepath, 'bw+') as fp:
                export('users', export_users, query, format=kind, fp=fp)
