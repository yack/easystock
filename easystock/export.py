# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Data export """

import csv
from configparser import SafeConfigParser
import datetime
import io
import logging
import os

from openpyxl import Workbook
from openpyxl import load_workbook


log = logging.getLogger(__name__)


class RowDoc:
    """ Base class for export document """

    APP_NAME = None
    templates_dir = None

    def __init__(self, file_, *args, **kwargs):
        self.file = file_
        self.args = args
        self.kwargs = kwargs
        self.rows = []

        self.mode = kwargs.pop('mode', 'r')
        assert self.mode in ('r', 'w', '+')

    def __contains__(self, row):
        return row in self.rows

    def __enter__(self):
        if self.mode not in ('r', '+'):
            return self

        self.load()
        return self

    def __exit__(self, type_, value, traceback):
        if self.mode not in ('w', '+'):
            return

        self.save()

    def __getitem__(self, x):
        return self.rows[x]

    def __getslice__(self, x, y):
        return self.rows[x:y]

    def __iter__(self):
        for row in self.rows:
            yield row

    def __len__(self):
        return len(self.rows)

    def __setitem__(self, x, row):
        self.rows[x] = row

    def __setslice__(self, x, y, rows):
        self.rows[x:y] = rows

    @property
    def filename(self):
        return os.path.basename(self.filepath)

    @property
    def filepath(self):
        return str(self.file.name)

    def append(self, row):
        self.rows.append(row)

    def clear(self):
        self.rows = []

    def extend(self, rows):
        self.rows.extend(rows)

    def index(self, row):
        self.rows.index(row)

    def insert(self, index, row):
        self.rows.insert(index, row)

    def pop(self):
        self.rows.pop()

    def remove(self, row):
        self.rows.remove(row)

    def reverse(self):
        self.rows.reverse()

    def safe_iterator(self, col_interface, col_version, interface, version, col_eol=0, col_min=0):
        """ Safe iterator which yields fitered and padded rows """

        count = 0
        for index, row in enumerate(self):
            count += 1
            lineno = index + 1

            if count > 100:
                # Stopping here
                log.warning('[%s:%06d] [INVALID] Too many consecutive invalid lines were detected, aborting', self.filename, lineno)
                break

            if not row:
                continue

            if row[col_interface] != interface:
                # Wrong interface
                continue

            if row[col_version] != version:
                # Invalid version
                log.error('[%s:%06d] [INVALID] Interface version does not match (%s instead of %s)', self.filename, lineno, row[col_version], version)
                continue

            row_length = len(row)

            if col_min and row_length < col_min:
                # Invalid row
                log.error('[%s:%06d] [INVALID] Row length does not match (%s instead of %s)', self.filename, lineno, row_length, col_min)
                continue

            if col_eol and row_length < col_eol:
                # Pad row
                row.extend([None for _ in range(col_eol - row_length)])

            count = 0

            # Safe row is ready to use
            yield lineno, row

    def sort(self, *args, **kwargs):
        """ Wrapper for list.sort() method """

        self.rows.sort(*args, **kwargs)

    def load(self):
        """ Load rows from file """
        raise NotImplementedError()

    def save(self):
        """ Save rows to file """
        raise NotImplementedError()


class CSVDoc(RowDoc):
    """ CSV export document """

    def load(self):
        """ Load rows from file """

        self.file.seek(0)

        reader = csv.reader(self.file, *self.args, **self.kwargs)

        self.rows = [row for row in reader]

    def save(self):
        """ Save rows to file """

        self.file.seek(0)

        writer = csv.writer(self.file, *self.args, **self.kwargs)
        writer.writerows(self.rows)

        self.file.seek(0)


class XLSXDoc(RowDoc):
    """ XLSX export document """

    def __init__(self, fp, *args, **kwargs):

        self.template = kwargs.pop('template', None)
        self.title = kwargs.pop('title', 'Sheet #1')

        self.__wb = None

        # Calling inherited
        super(XLSXDoc, self).__init__(fp, *args, **kwargs)

    def load(self, sheet=None):
        """ Load rows from file """

        if self.__wb is None:
            self.file.seek(0)
            self.__wb = load_workbook(self.file, *self.args, **self.kwargs)

        if self.template:
            title = self.template.get('{0}sheet'.format('{0}.'.format(sheet) if sheet else ''))
        else:
            title = self.title

        if title:
            if title not in self.__wb:
                self.rows = []
                return

            ws = self.__wb[title]
        else:
            ws = self.__wb.active

        self.rows = [
            [cell.value for cell in row]
            for row in ws.rows
        ]

    def save(self, sheet=None):
        """ Save rows to file """

        if self.template:
            title = self.template.get('{0}sheet'.format('{0}.'.format(sheet) if sheet else ''))
        else:
            title = self.title

        assert title is not None, 'title is undefined'

        if self.__wb is None:
            self.__wb = Workbook(*self.args, **self.kwargs)

        if self.template:
            if title in self.__wb:
                ws = self.__wb[title]
            else:
                ws = self.__wb.create_sheet(title)

            start_col = int(self.template.get('{0}col'.format('{0}.'.format(sheet) if sheet else ''), 1))
            start_row = int(self.template.get('{0}row'.format('{0}.'.format(sheet) if sheet else ''), 1))
            for i, row in enumerate(self.rows):
                for j, value in enumerate(row):
                    ws.cell(column=start_col + j, row=start_row + i, value=value)
        else:
            ws = self.__wb.active
            ws.title = title

            for row in self.rows:
                ws.append(row)

        if sheet is None:
            # Erase file content
            self.file.seek(0)
            self.file.truncate()

            self.__wb.save(self.file)

            # Rewind file
            self.file.seek(0)

        self.clear()


def export(target, func, source, **kwargs):
    """ Wrapper function for export """

    # Extract parameters
    format_ = kwargs.pop('format', 'xslx')
    assert format_ in ('csv', 'xlsx'), 'Invalid export format'

    fp = kwargs.pop('fp', None)

    version = kwargs.pop('version', None)
    if version is None:
        today = datetime.date.today()
        version = '{0}-01'.format(today.strftime('%Y%m%d'))

    if format_ == 'csv':
        if fp is None:
            fp = io.StringIO()

        content_type = 'text/csv; charset="utf-8"'
        doc = CSVDoc(fp, delimiter=';', mode='w')
        add_headers = True

    else:
        if fp is None:
            fp = io.BytesIO()

        content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset="utf-8"'
        template = get_template(target)
        if template is not None:
            with open(template['template'], 'rb') as fpt:
                fp.write(fpt.read())

            doc = XLSXDoc(fp, mode='+', template=template)
            add_headers = False
        else:
            doc = XLSXDoc(fp, mode='w', title=target.capitalize())
            add_headers = True

    with doc:
        func(source, doc, add_headers=add_headers, **kwargs)

    if format_ == 'csv':
        # Convert string to bytes
        fp = io.BytesIO(fp.read().encode('utf-8'))
        fp.seek(0)

    filename = '{0}-{1}-{2}.{3}'.format(doc.APP_NAME, target.upper(), version, format_)

    return fp, content_type, filename


def get_template(target):
    """ Get template file for target """

    if RowDoc.templates_dir is None:
        log.error('[Export] Templates directory is not defined in configuration')
        return None

    config_path = os.path.join(RowDoc.templates_dir, 'config.ini')
    if not os.access(config_path, os.F_OK):
        log.error('[Export] The config file %s is missing', config_path)
        return None

    if not os.access(config_path, os.R_OK):
        log.error('[Export] The config file %s is not readable', config_path)
        return None

    config_parser = SafeConfigParser()
    config_parser.read(config_path)

    if target not in config_parser.sections():
        log.warning('[Export] The section «%s» is missing in config file %s', target, config_path)
        return None

    options = config_parser.options(target)
    if not options:
        log.error('[Export] The section «%s» is empty in config file %s', target, config_path)
        return None

    if 'template' not in options:
        log.error('[Export] The option «template» of section «%s» is missing in config file %s', target, config_path)
        return None

    config = dict()
    for option in options:
        value = config_parser.get(target, option)

        if option == 'template':
            value = os.path.join(RowDoc.templates_dir, value)
            if not os.access(value, os.F_OK) or not os.access(value, os.R_OK):
                # template file doesn't exist or is not readable
                config = None
                log.error('[Export] Template file defined in section «%s» of config file %s does not exist or is not readable', target, config_path)
                break

        config[option] = value

    return config


def includeme(config):
    """
    Set up standard configurator registrations. Use via:

    .. code-block:: python

    config = Configurator()
    config.include('easystock.export')
    """

    registry = config.registry
    settings = registry.settings

    RowDoc.APP_NAME = settings['app.name']
    RowDoc.templates_dir = settings.get('export_templates')
