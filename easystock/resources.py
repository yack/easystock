# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" EasyStock authorization module """

from pyramid.security import Allow


ALL_PERMISSIONS = [
    # Areas
    'areas.delete',
    'areas.search',
    'areas.visual',

    # Assets
    'assets.create',
    'assets.delete',
    'assets.search',
    'assets.update',
    'assets.visual',

    # Products
    'products.create',
    'products.delete',
    'products.modify',
    'products.search',
    'products.visual',

    # Sites
    'sites.create',
    'sites.delete',
    'sites.modify',
    'sites.search',
    'sites.visual',

    # Stocks
    'stocks.create',
    'stocks.delete',
    'stocks.search',

    # Users
    'users.create',
    'users.delete',
    'users.import',
    'users.modify',
    'users.profile',
    'users.search',

    # Misc
    'dashboard',
    'imports.upload',
]


class RootFactory:
    """ Base class for EasyStock resources """

    __acl__ = [
        # Administrator
        (Allow, 'profile:admin', ALL_PERMISSIONS),

        # Manager
        (Allow, 'profile:manager', ('assets.delete')),
        (Allow, 'profile:manager', ('products.delete')),
        (Allow, 'profile:manager', ('users.search')),

        # Stock manager
        (Allow, 'profile:stock-manager', ('products.create', 'products.modify')),
        (Allow, 'profile:stock-manager', ('stocks.create', 'stocks.delete')),

        # Asset manager
        (Allow, 'profile:asset-manager', ('assets.create', 'assets.update')),

        # User
        (Allow, 'profile:user', ('areas.search', 'areas.visual')),
        (Allow, 'profile:user', ('assets.search', 'assets.visual')),
        (Allow, 'profile:user', ('sites.search', 'sites.visual')),
        (Allow, 'profile:user', ('stocks.search', 'stocks.visual')),
        (Allow, 'profile:user', ('products.search', 'products.visual')),
        (Allow, 'profile:user', ('dashboard', 'users.profile')),
    ]

    def __init__(self, request):
        self.request = request
