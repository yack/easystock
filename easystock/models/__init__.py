# -*- coding: utf-8 -*-

# EasyStock -- Simple Stock and Assets mamagement tool
# By: Cyril Lacoux <clacoux@ifeelgood.org>
#
# Copyright (C) 2018-2019 Cyril Lacoux
# https://gitlab.com/yack/easystock
#
# This file is part of EasyStock
#
# EasyStock is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# EasyStock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Database model """

from collections import OrderedDict
import datetime
import logging
import warnings

from passlib.context import CryptContext

from pyramid.decorator import reify

import sqlalchemy as sa
from sqlalchemy import engine_from_config
from sqlalchemy import exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import DeferredReflection
from sqlalchemy.orm import backref
from sqlalchemy.orm import relationship
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import synonym

from zope.sqlalchemy import register

from pyramid_helpers.i18n import N_

from easystock.utils import slugify


DBSession = scoped_session(sessionmaker())
register(DBSession)
Base = declarative_base(cls=DeferredReflection)


log = logging.getLogger(__name__)


class CodeMixin:
    """ Mixin that adds support for `code` and fullcode `properties` """

    APP_NAME = None
    APP_INSTANCE = None

    @reify
    def code(self):
        """ Compute stock code from object id """

        return '{0:06d}'.format(self.id)

    @reify
    def fullcode(self):
        """
        Compute full code from:
         * app_name
         * app_instance
         * object class
         * object id
        """

        fullcode = []

        if self.APP_NAME:
            fullcode.append(slugify(self.APP_NAME.upper()))

        if self.APP_INSTANCE:
            fullcode.append(slugify(self.APP_INSTANCE.upper()))

        fullcode.append(self.__class__.__name__.upper())
        fullcode.append(self.code)

        return '::'.join(fullcode)


class Area(Base, CodeMixin):
    """ ORM class mapped to areas table """

    __tablename__ = 'areas'

    STATUSES = OrderedDict([
        ('active', N_('active')),
        ('disabled', N_('disabled')),
    ])

    # Relationship
    site = relationship('Site', backref=backref('areas', cascade='all,delete-orphan', lazy='dynamic'))

    @property
    def fullname(self):
        """ Compute fullname """

        return '{0}, {1} , {2}, {3}'.format(self.site.name, self.building, self.floor, self.room)

    def from_dict(self, data):
        """ Load data from dict """

        utcnow = datetime.datetime.utcnow()

        if 'building' in data:
            self.building = data['building']

        if 'floor' in data:
            self.floor = data['floor']

        if 'room' in data:
            self.room = data['room']

        if 'site' in data:
            self.site = data['site']

        if 'status' in data:
            self.status = data['status']

        if self.creation_date is None:
            self.creation_date = data.get('creation_date') or utcnow

        if data.get('modification_date'):
            self.modification_date = data['modification_date']

        elif DBSession.is_modified(self):
            self.modification_date = utcnow

        return DBSession.is_modified(self)

    def to_dict(self, context=None):
        """ Dump data to dict """

        return dict(
            id=self.id,
            site=self.site,
            building=self.building,
            floor=self.floor,
            room=self.room,
        )

    def to_json(self, context=None):
        """ Dump data to JSON """

        data = self.to_dict(context=context)
        data['site'] = dict(
            id=data['site'].id,
            name=data['site'].name,
        )
        return data


class Asset(Base, CodeMixin):
    """ ORM class mapped to assets table """

    __tablename__ = 'assets'

    STATES = OrderedDict([
        ('new', N_('New')),
        ('good', N_('Good')),
        ('used', N_('Used')),
        ('lost', N_('Lost')),
        ('broken', N_('Broken')),
        ('onrepair', N_('On repair')),
        ('out-of-service', N_('Out of service')),
    ])

    # Relationship
    area = relationship('Area', backref=backref('assets', cascade='all,delete-orphan', lazy='dynamic'))
    author = relationship('User', backref=backref('assets', cascade='all,delete-orphan', lazy='dynamic'))
    product = relationship('Product', backref=backref('assets', cascade='all,delete-orphan', lazy='dynamic'))

    def to_dict(self, context=None):
        """ Dump data to dict """

        return dict(
            id=self.id,
            area=self.area,
            product=self.product,
            site=self.site,
            serial_number=self.serial_number,
            state=self.state,
        )


class Product(Base, CodeMixin):
    """ ORM class mapped to products table """

    __tablename__ = 'products'

    STATUSES = OrderedDict([
        ('available', N_('Available')),
        ('discontinued', N_('Discontinued')),
    ])

    def from_dict(self, data):
        """ Load data from dict """

        utcnow = datetime.datetime.utcnow()

        if 'barcode' in data:
            self.barcode = data['barcode']

        if 'brand' in data:
            self.brand = data['brand']

        if 'model' in data:
            self.model = data['model']

        if 'reference' in data:
            self.reference = data['reference']

        if 'barcode' in data:
            self.barcode = data['barcode']

        if 'category' in data:
            self.category = data['category']

        if 'label' in data:
            self.label = data['label']

        if 'status' in data:
            self.status = data['status']

        if self.creation_date is None:
            self.creation_date = data.get('creation_date') or utcnow

        if data.get('modification_date'):
            self.modification_date = data['modification_date']

        elif DBSession.is_modified(self):
            self.modification_date = utcnow

        return DBSession.is_modified(self)

    def to_dict(self, context=None):
        """ Dump data to dict """

        return dict(
            barcode=self.barcode,
            brand=self.brand,
            category=self.category,
            code=self.code,
            creation_date=self.creation_date,
            id=self.id,
            label=self.label,
            model=self.model,
            modification_date=self.modification_date,
            reference=self.reference,
            status=self.status,
        )

    def to_json(self, context=None):
        """ Dump data to JSON """

        data = self.to_dict(context=context)
        return data


class Site(Base):
    """ ORM class mapped to sites table """

    __tablename__ = 'sites'

    STATUSES = OrderedDict([
        ('active', N_('active')),
        ('disabled', N_('disabled')),
    ])

    @property
    def assets(self):
        """ Query to get assets from this site """

        return DBSession.query(Asset).join(Area).filter(Area.site == self)

    @property
    def stocks(self):
        """ Query to get stocks from this site """

        return DBSession.query(Stock).join(Area).filter(Area.site == self)

    def from_dict(self, data):
        """ Load data from dict """

        utcnow = datetime.datetime.utcnow()

        if 'areas' in data:
            self.areas = data['areas']

        if 'address' in data:
            self.address = data['address']

        if 'description' in data:
            self.description = data['description']

        if 'name' in data:
            self.name = data['name']

        if 'status' in data:
            self.status = data['status']

        if self.creation_date is None:
            self.creation_date = data.get('creation_date') or utcnow

        if data.get('modification_date'):
            self.modification_date = data['modification_date']

        elif DBSession.is_modified(self):
            self.modification_date = utcnow

        return DBSession.is_modified(self)

    def to_dict(self, context=None):
        """ Dump data to dict """

        return dict(
            address=self.address,
            creation_date=self.creation_date,
            description=self.description,
            id=self.id,
            modification_date=self.modification_date,
            name=self.name,
            status=self.status,
        )

    def to_json(self, context=None):
        """ Dump data to JSON """

        data = self.to_dict(context=context)
        return data


class Stock(Base):
    """ ORM class mapped to stocks table """

    __tablename__ = 'stocks'

    ACTIONS = OrderedDict([
        ('in', N_('In')),
        ('out', N_('Out')),
        ('stock', N_('Stocktake')),
    ])

    # Relationship
    area = relationship('Area', backref=backref('stocks', cascade='all,delete-orphan', lazy='dynamic'))
    author = relationship('User', backref=backref('stocks', cascade='all,delete-orphan', lazy='dynamic'))
    product = relationship('Product', backref=backref('stocks', cascade='all,delete-orphan', lazy='dynamic'))

    @reify
    def next(self):
        """ Return the next stock object for site/area/product """

        cls = self.__class__

        query = DBSession.query(cls).filter(
            sa.and_(
                cls.area == self.area,
                cls.date > self.date,
                cls.product == self.product,
            )
        ).order_by(sa.asc(cls.date))

        return query.first()

    @reify
    def prev(self):
        """ Return the previous stock object for site/area/product """

        cls = self.__class__

        query = DBSession.query(cls).filter(
            sa.and_(
                cls.area == self.area,
                cls.date < self.date,
                cls.product == self.product,
            )
        ).order_by(sa.desc(cls.date))

        return query.first()


class User(Base):
    """ ORM class mapped to users table """

    __tablename__ = 'users'

    CRYPT_SCHEMES = ['sha256_crypt', 'sha512_crypt']

    PROFILES = OrderedDict([
        ('admin', N_('Administrator')),
        ('manager', N_('Manager')),
        ('stock-manager', N_('Stock manager')),
        ('asset-manager', N_('Asset manager')),
        ('user', N_('User')),
    ])

    STATUSES = OrderedDict([
        ('active', N_('Active')),
        ('disabled', N_('Disabled')),
    ])

    # Relationship
    site = relationship('Site', backref=backref('users', cascade='all,delete-orphan', lazy='dynamic'))

    @property
    def fullname(self):
        """ Compute user's fullname """

        fullname = []
        if self.firstname:
            fullname.append(self.firstname)
        if self.lastname:
            fullname.append(self.lastname)
        if not fullname:
            fullname.append(self.username)

        return ' '.join(fullname)

    def from_dict(self, data):
        """ Load data from dict """

        utcnow = datetime.datetime.utcnow()

        if 'email' in data:
            self.email = data['email']

        if 'firstname' in data:
            self.firstname = data['firstname']

        if 'lastname' in data:
            self.lastname = data['lastname']

        if 'profiles' in data:
            self.profiles = data['profiles']

        if 'site' in data:
            self.site = data['site']

        if 'status' in data:
            self.status = data['status']

        if 'timezone' in data:
            self.timezone = data['timezone']

        if 'username' in data:
            self.username = data['username']

        if data.get('password') and not self.validate_password(data['password']):
            self.set_password(data['password'])

        if self.creation_date is None:
            self.creation_date = data.get('creation_date') or utcnow

        if data.get('modification_date'):
            self.modification_date = data['modification_date']

        elif DBSession.is_modified(self):
            self.modification_date = utcnow

        return DBSession.is_modified(self)

    def set_password(self, secret, scheme='sha512_crypt'):
        """ Hash and set password """

        if self.status != 'active':
            return

        if secret is None:
            self.password = None
            return

        # Encrypt password
        ctx = CryptContext(schemes=self.CRYPT_SCHEMES)
        self.password = ctx.encrypt(secret, scheme=scheme)

    def to_dict(self, context=None):
        """ Dump data to dict """

        data = dict(
            id=self.id,
            email=self.email,
            firstname=self.firstname,
            fullname=self.fullname,
            lastname=self.lastname,
            profiles=self.profiles,
            site=self.site,
            status=self.status,
            timezone=self.timezone,
            username=self.username,
        )

        if context == 'export':
            data.update(dict(
                creation_date=self.creation_date,
                modification_date=self.modification_date,
            ))

        return data

    def to_json(self, context=None):
        """ Dump data to JSON """

        data = self.to_dict(context=context)

        return data

    def validate_password(self, secret):
        """ Validate password """

        if self.status != 'active':
            return False

        if self.password is None:
            return False

        # Verify password
        ctx = CryptContext(schemes=self.CRYPT_SCHEMES)
        try:
            validated = ctx.verify(secret, self.password)
        except ValueError:
            log.exception('Failed to verify password using CryptContext.verify() for user #%s', self.id)
            validated = False

        return validated


#
# Timelines
#
class Timeline(Base):
    """ ORM class mapped to timelines table """

    __tablename__ = 'timelines'
    __mapper_args__ = dict(polymorphic_on='target')

    ACTIONS = OrderedDict([
        ('create', N_('Creation')),
        ('delete', N_('Deletion')),
        ('modify', N_('Modification')),
    ])

    TARGET_TYPES = OrderedDict([
        ('asset', N_('Asset')),
        ('product', N_('Product')),
        ('site', N_('Site')),
    ])

    # Relationship
    author = relationship('User')


class AssetTimeline(Timeline):
    """ Timeline polymorph for assets """

    __mapper_args__ = dict(polymorphic_identity='asset')

    # Relationship
    assets = relationship(
        'Asset',
        backref=backref('timelines', lazy='dynamic'),
        secondary='timelines_assets_associations',
        lazy='dynamic',
    )

    targets = synonym('assets')


class ProductTimeline(Timeline):
    """ Timeline polymorph for products """

    __mapper_args__ = dict(polymorphic_identity='product')

    # Relationship
    products = relationship(
        'Product',
        backref=backref('timelines', lazy='dynamic'),
        secondary='timelines_products_associations',
        lazy='dynamic',
    )

    targets = synonym('products')


class SiteTimeline(Timeline):
    """ Timeline polymorph for sites """

    __mapper_args__ = dict(polymorphic_identity='site')

    # Relationship
    sites = relationship(
        'Site',
        backref=backref('timelines', lazy='dynamic'),
        secondary='timelines_sites_associations',
        lazy='dynamic',
    )

    targets = synonym('sites')


class UserTimeline(Timeline):
    """ Timeline polymorph for users """

    __mapper_args__ = dict(polymorphic_identity='user')

    # Relationship
    users = relationship(
        'User',
        backref=backref('timelines', lazy='dynamic'),
        secondary='timelines_users_associations',
        lazy='dynamic',
    )

    targets = synonym('users')


def delete_orphan_timelines():
    """ Delete orphaned timelines """

    for model in (AssetTimeline, ProductTimeline, SiteTimeline, UserTimeline):
        # Get secondary table from targets property
        secondary = model.targets.property.secondary

        # Get all timelines without association in secondary table
        # Association was previously deleted when target has been removed
        subquery = DBSession.query(model.id)
        subquery = subquery.outerjoin(secondary)
        subquery = subquery.filter(
            sa.and_(
                model.action != 'delete',
                secondary.c.timeline_id.is_(None),
            )
        )
        subquery = subquery.subquery()

        # Delete orphaned timelines
        DBSession.query(model).filter(
            model.id.in_(subquery)
        ).delete(synchronize_session=False)


def get_user_by_username(username):
    """ Get user from database by username """

    query = DBSession.query(User).filter(User.username == username)
    return query.first()


def includeme(config):
    """
    Set up standard configurator registrations. Use via:

    .. code-block:: python

    config = Configurator()
    config.include('easystock.models')
    """

    registry = config.registry
    settings = registry.settings

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    # Ignore SA warnings about partial indexes
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', category=exc.SAWarning)

        Base.prepare(engine)

    CodeMixin.APP_NAME = settings['app.name']
    CodeMixin.APP_INSTANCE = settings['app.instance']
