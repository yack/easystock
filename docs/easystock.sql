-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2-beta
-- PostgreSQL version: 11.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- -- object: easystock | type: ROLE --
-- -- DROP ROLE IF EXISTS easystock;
-- CREATE ROLE easystock WITH 
-- 	ENCRYPTED PASSWORD 'easystock';
-- -- ddl-end --
-- 

-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: easystock | type: DATABASE --
-- -- DROP DATABASE IF EXISTS easystock;
-- CREATE DATABASE easystock
-- 	OWNER = easystock;
-- -- ddl-end --
-- 

-- object: public.sites_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.sites_id_seq CASCADE;
CREATE SEQUENCE public.sites_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.sites_id_seq OWNER TO easystock;
-- ddl-end --

-- object: public.sites | type: TABLE --
-- DROP TABLE IF EXISTS public.sites CASCADE;
CREATE TABLE public.sites (
	id integer NOT NULL DEFAULT nextval('public.sites_id_seq'::regclass),
	address text,
	creation_date timestamp NOT NULL,
	description text,
	modification_date timestamp NOT NULL,
	name character varying(255) NOT NULL,
	status character varying(255) NOT NULL,
	CONSTRAINT sites_pkey PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.sites OWNER TO easystock;
-- ddl-end --

-- object: public.products_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.products_id_seq CASCADE;
CREATE SEQUENCE public.products_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.products_id_seq OWNER TO easystock;
-- ddl-end --

-- object: public.products | type: TABLE --
-- DROP TABLE IF EXISTS public.products CASCADE;
CREATE TABLE public.products (
	id integer NOT NULL DEFAULT nextval('public.products_id_seq'::regclass),
	barcode character varying(255),
	brand character varying(255),
	category character varying(255) NOT NULL,
	creation_date timestamp NOT NULL,
	description text,
	label character varying(255) NOT NULL,
	model character varying(255),
	modification_date timestamp NOT NULL,
	reference character varying,
	status character varying(255) NOT NULL,
	CONSTRAINT products_pkey PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.products OWNER TO easystock;
-- ddl-end --

-- object: public.assets_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.assets_id_seq CASCADE;
CREATE SEQUENCE public.assets_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.assets_id_seq OWNER TO easystock;
-- ddl-end --

-- object: public.assets | type: TABLE --
-- DROP TABLE IF EXISTS public.assets CASCADE;
CREATE TABLE public.assets (
	id integer NOT NULL DEFAULT nextval('public.assets_id_seq'::regclass),
	area_id integer NOT NULL,
	author_id integer NOT NULL,
	product_id integer NOT NULL,
	creation_date timestamp NOT NULL,
	inventory_date timestamp NOT NULL,
	modification_date timestamp NOT NULL,
	serial_number character varying(255),
	state character varying(255) NOT NULL,
	CONSTRAINT assets_pkey PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.assets OWNER TO easystock;
-- ddl-end --

-- object: products_fk | type: CONSTRAINT --
-- ALTER TABLE public.assets DROP CONSTRAINT IF EXISTS products_fk CASCADE;
ALTER TABLE public.assets ADD CONSTRAINT products_fk FOREIGN KEY (product_id)
REFERENCES public.products (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.users_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.users_id_seq CASCADE;
CREATE SEQUENCE public.users_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.users_id_seq OWNER TO easystock;
-- ddl-end --

-- object: public.users | type: TABLE --
-- DROP TABLE IF EXISTS public.users CASCADE;
CREATE TABLE public.users (
	id integer NOT NULL DEFAULT nextval('public.users_id_seq'::regclass),
	site_id integer NOT NULL,
	creation_date timestamp NOT NULL,
	email character varying(255),
	firstname character varying(255),
	lastname character varying(255),
	modification_date timestamp NOT NULL,
	password character varying(255),
	profiles character varying(255)[] NOT NULL,
	status character varying(255) NOT NULL,
	username character varying(255) NOT NULL,
	timezone character varying(50),
	CONSTRAINT users_pkey PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.users OWNER TO easystock;
-- ddl-end --

-- object: sites_fk | type: CONSTRAINT --
-- ALTER TABLE public.users DROP CONSTRAINT IF EXISTS sites_fk CASCADE;
ALTER TABLE public.users ADD CONSTRAINT sites_fk FOREIGN KEY (site_id)
REFERENCES public.sites (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.timelines_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.timelines_id_seq CASCADE;
CREATE SEQUENCE public.timelines_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.timelines_id_seq OWNER TO easystock;
-- ddl-end --

-- object: public.timelines | type: TABLE --
-- DROP TABLE IF EXISTS public.timelines CASCADE;
CREATE TABLE public.timelines (
	id integer NOT NULL DEFAULT nextval('public.timelines_id_seq'::regclass),
	author_id integer NOT NULL,
	action character varying(20) NOT NULL,
	date timestamp NOT NULL,
	message text,
	target character varying(30) NOT NULL,
	CONSTRAINT timelines_pkey PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.timelines OWNER TO easystock;
-- ddl-end --

-- object: users_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines DROP CONSTRAINT IF EXISTS users_fk CASCADE;
ALTER TABLE public.timelines ADD CONSTRAINT users_fk FOREIGN KEY (author_id)
REFERENCES public.users (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.timelines_assets_associations | type: TABLE --
-- DROP TABLE IF EXISTS public.timelines_assets_associations CASCADE;
CREATE TABLE public.timelines_assets_associations (
	timeline_id integer NOT NULL,
	asset_id integer NOT NULL,
	CONSTRAINT timelines_assets_associations_pk PRIMARY KEY (timeline_id,asset_id)

);
-- ddl-end --
ALTER TABLE public.timelines_assets_associations OWNER TO easystock;
-- ddl-end --

-- object: public.timelines_products_associations | type: TABLE --
-- DROP TABLE IF EXISTS public.timelines_products_associations CASCADE;
CREATE TABLE public.timelines_products_associations (
	timeline_id integer NOT NULL,
	product_id integer NOT NULL,
	CONSTRAINT timelines_products_associations_pk PRIMARY KEY (timeline_id,product_id)

);
-- ddl-end --
ALTER TABLE public.timelines_products_associations OWNER TO easystock;
-- ddl-end --

-- object: timelines_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_products_associations DROP CONSTRAINT IF EXISTS timelines_fk CASCADE;
ALTER TABLE public.timelines_products_associations ADD CONSTRAINT timelines_fk FOREIGN KEY (timeline_id)
REFERENCES public.timelines (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: timelines_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_assets_associations DROP CONSTRAINT IF EXISTS timelines_fk CASCADE;
ALTER TABLE public.timelines_assets_associations ADD CONSTRAINT timelines_fk FOREIGN KEY (timeline_id)
REFERENCES public.timelines (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: products_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_products_associations DROP CONSTRAINT IF EXISTS products_fk CASCADE;
ALTER TABLE public.timelines_products_associations ADD CONSTRAINT products_fk FOREIGN KEY (product_id)
REFERENCES public.products (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: assets_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_assets_associations DROP CONSTRAINT IF EXISTS assets_fk CASCADE;
ALTER TABLE public.timelines_assets_associations ADD CONSTRAINT assets_fk FOREIGN KEY (asset_id)
REFERENCES public.assets (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.timelines_sites_associations | type: TABLE --
-- DROP TABLE IF EXISTS public.timelines_sites_associations CASCADE;
CREATE TABLE public.timelines_sites_associations (
	timeline_id integer NOT NULL,
	site_id integer NOT NULL,
	CONSTRAINT timelines_sites_associations_pk PRIMARY KEY (timeline_id,site_id)

);
-- ddl-end --
ALTER TABLE public.timelines_sites_associations OWNER TO easystock;
-- ddl-end --

-- object: timelines_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_sites_associations DROP CONSTRAINT IF EXISTS timelines_fk CASCADE;
ALTER TABLE public.timelines_sites_associations ADD CONSTRAINT timelines_fk FOREIGN KEY (timeline_id)
REFERENCES public.timelines (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: sites_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_sites_associations DROP CONSTRAINT IF EXISTS sites_fk CASCADE;
ALTER TABLE public.timelines_sites_associations ADD CONSTRAINT sites_fk FOREIGN KEY (site_id)
REFERENCES public.sites (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.stocks_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.stocks_id_seq CASCADE;
CREATE SEQUENCE public.stocks_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.stocks_id_seq OWNER TO easystock;
-- ddl-end --

-- object: public.stocks | type: TABLE --
-- DROP TABLE IF EXISTS public.stocks CASCADE;
CREATE TABLE public.stocks (
	id integer NOT NULL DEFAULT nextval('public.stocks_id_seq'::regclass),
	area_id integer NOT NULL,
	author_id integer NOT NULL,
	product_id integer NOT NULL,
	action character varying(255) NOT NULL,
	date timestamp NOT NULL,
	message text,
	total smallint NOT NULL,
	value smallint NOT NULL,
	CONSTRAINT stocks_pkey PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON COLUMN public.stocks.total IS 'Nombre d''éléments en stock';
-- ddl-end --
COMMENT ON COLUMN public.stocks.value IS 'Nombre d''éléments pour l''opération';
-- ddl-end --
ALTER TABLE public.stocks OWNER TO easystock;
-- ddl-end --

-- object: products_fk | type: CONSTRAINT --
-- ALTER TABLE public.stocks DROP CONSTRAINT IF EXISTS products_fk CASCADE;
ALTER TABLE public.stocks ADD CONSTRAINT products_fk FOREIGN KEY (product_id)
REFERENCES public.products (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: unaccent | type: EXTENSION --
-- DROP EXTENSION IF EXISTS unaccent CASCADE;
CREATE EXTENSION unaccent
WITH SCHEMA public
VERSION '1.1';
-- ddl-end --
COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';
-- ddl-end --

-- object: users_fk | type: CONSTRAINT --
-- ALTER TABLE public.stocks DROP CONSTRAINT IF EXISTS users_fk CASCADE;
ALTER TABLE public.stocks ADD CONSTRAINT users_fk FOREIGN KEY (author_id)
REFERENCES public.users (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: users_fk | type: CONSTRAINT --
-- ALTER TABLE public.assets DROP CONSTRAINT IF EXISTS users_fk CASCADE;
ALTER TABLE public.assets ADD CONSTRAINT users_fk FOREIGN KEY (author_id)
REFERENCES public.users (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.areas_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.areas_id_seq CASCADE;
CREATE SEQUENCE public.areas_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.areas_id_seq OWNER TO easystock;
-- ddl-end --

-- object: public.areas | type: TABLE --
-- DROP TABLE IF EXISTS public.areas CASCADE;
CREATE TABLE public.areas (
	id integer NOT NULL DEFAULT nextval('public.areas_id_seq'::regclass),
	site_id integer NOT NULL,
	creation_date timestamp NOT NULL,
	building character varying(255) NOT NULL,
	floor smallint NOT NULL,
	modification_date timestamp NOT NULL,
	room character varying(255) NOT NULL,
	status character varying(255) NOT NULL,
	CONSTRAINT areas_pkey PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.areas OWNER TO easystock;
-- ddl-end --

-- object: sites_fk | type: CONSTRAINT --
-- ALTER TABLE public.areas DROP CONSTRAINT IF EXISTS sites_fk CASCADE;
ALTER TABLE public.areas ADD CONSTRAINT sites_fk FOREIGN KEY (site_id)
REFERENCES public.sites (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: areas_fk | type: CONSTRAINT --
-- ALTER TABLE public.assets DROP CONSTRAINT IF EXISTS areas_fk CASCADE;
ALTER TABLE public.assets ADD CONSTRAINT areas_fk FOREIGN KEY (area_id)
REFERENCES public.areas (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: areas_fk | type: CONSTRAINT --
-- ALTER TABLE public.stocks DROP CONSTRAINT IF EXISTS areas_fk CASCADE;
ALTER TABLE public.stocks ADD CONSTRAINT areas_fk FOREIGN KEY (area_id)
REFERENCES public.areas (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.timelines_users_associations | type: TABLE --
-- DROP TABLE IF EXISTS public.timelines_users_associations CASCADE;
CREATE TABLE public.timelines_users_associations (
	user_id integer NOT NULL,
	timeline_id integer NOT NULL,
	CONSTRAINT timelines_users_associations_pk PRIMARY KEY (user_id,timeline_id)

);
-- ddl-end --

-- object: users_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_users_associations DROP CONSTRAINT IF EXISTS users_fk CASCADE;
ALTER TABLE public.timelines_users_associations ADD CONSTRAINT users_fk FOREIGN KEY (user_id)
REFERENCES public.users (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: timelines_fk | type: CONSTRAINT --
-- ALTER TABLE public.timelines_users_associations DROP CONSTRAINT IF EXISTS timelines_fk CASCADE;
ALTER TABLE public.timelines_users_associations ADD CONSTRAINT timelines_fk FOREIGN KEY (timeline_id)
REFERENCES public.timelines (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --


