--
-- EasyStock upgrade script for v0.2
--

SET search_path=public,pg_catalog;


-- Create areas table
CREATE SEQUENCE areas_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

ALTER SEQUENCE areas_id_seq OWNER TO easystock;


CREATE TABLE areas(
	id integer NOT NULL DEFAULT nextval('public.areas_id_seq'::regclass),
	site_id integer NOT NULL,
	building character varying(255) NOT NULL,
	floor smallint NOT NULL,
	room character varying(255) NOT NULL,
	CONSTRAINT areas_pkey PRIMARY KEY (id)
);

ALTER TABLE areas OWNER TO easystock;

ALTER TABLE public.areas ADD CONSTRAINT sites_fk FOREIGN KEY (site_id)
    REFERENCES public.sites (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO areas (site_id, building, floor, room) VALUES (1, 'Main building', 0, 'Main stock');


-- Assets
ALTER TABLE assets DROP COLUMN location;

ALTER TABLE assets DROP CONSTRAINT IF EXISTS sites_fk CASCADE;
ALTER TABLE assets DROP COLUMN site_id;

ALTER TABLE assets ADD COLUMN area_id integer;

ALTER TABLE assets ADD CONSTRAINT areas_fk FOREIGN KEY (area_id)
    REFERENCES areas (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;


UPDATE assets SET area_id = 1;
ALTER TABLE assets ALTER COLUMN area_id SET NOT NULL;


-- Stocks
ALTER TABLE stocks DROP COLUMN area;

ALTER TABLE stocks DROP CONSTRAINT IF EXISTS sites_fk CASCADE;
ALTER TABLE stocks DROP COLUMN site_id;

ALTER TABLE stocks ADD COLUMN area_id integer;

ALTER TABLE stocks ADD CONSTRAINT areas_fk FOREIGN KEY (area_id)
    REFERENCES areas (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;


UPDATE stocks SET area_id = 1;
ALTER TABLE stocks ALTER COLUMN area_id SET NOT NULL;


-- Remove users timeline
DROP TABLE timelines_users_associations CASCADE;


-- Users
ALTER TABLE users DROP COLUMN profile;
ALTER TABLE users ADD COLUMN profiles character varying(255)[];

UPDATE users SET profiles = '{"admin"}';
ALTER TABLE users ALTER COLUMN profiles SET NOT NULL;
