# Installation instructions for Debian 9

## Dependencies
```
apt install apache2 libapache2-mod-wsgi
apt install postgresql-9.6 postgresql-contrib-9.6

apt install liquidprompt
apt install git

apt install python-setuptools virtualenv
apt install gcc libssl-dev python-dev
```


## Create EasyStock user and prepare virtual environment
```
adduser --disabled-password --ingroup www-data --home /srv/easystock easystock

su - easystock

mkdir apps conf data temp venv
chmod 0750 apps conf venv
chmod 2770 data temp

virtualenv venv

liquidprompt_activate
```


## Download EasyStock software
```
su - easystock
cd apps

git clone https://gitlab.com/yack/pyramid-helpers.git
git clone https://gitlab.com/yack/easystock.git
```

**Copy and adapt** configuration files from ```~/apps/easystock/conf``` to ```~/conf```


## Database setup
```
su - postgres
psql

CREATE USER easystock WITH PASSWORD 'what-is-good-for-security';
CREATE DATABASE easystock WITH OWNER easystock;

\c easystock

CREATE EXTENSION unaccent;

\q

psql easystock < /srv/easystock/apps/easystock/docs/easystock.sql
```

Check permissions in ```/etc/postgresql/9.6/main/pg_hba.conf``` file:
```
# Database administrative login by Unix domain socket
local   all             postgres                                peer

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     postgres                                peer
#host    replication     postgres        127.0.0.1/32            md5
#host    replication     postgres        ::1/128                 md5
```


## Enable virtual environment

Add the following lines in ```~easystock/.bashrc```:

```
# Enable virtual environment if in interactive shell
if [ "$PS1" ]; then
    echo "Activating virtual environment for EasyStock (from ~/.bashrc) ..."
    source ~/venv/bin/activate
fi
```

Then reconnect. You should see «Activating virtual environment...» message, Prompt is ```[easystock@hostname:~] [venv] $```


## Install Python dependencies
```
su - easystock

pip install python-dateutil
pip install decorator
pip install mako
pip install babel
pip install po2json
pip install unidecode

pip install psycopg2
pip install sqlalchemy
pip install transaction

pip install formencode
pip install weberror
pip install webhelpers

pip install zope.sqlalchemy
pip install pyramid
pip install pyramid-mako
pip install pyramid-tm
pip install pyramid-beaker

pip install passlib
pip install pytz
pip install openpyxl

pip install ipython pyramid-ipython
```

## Install EasytStock software
```
su - easystock

# Pyramid-Helpers
cd ~/apps/pyramid-helpers

./setup.py compile_catalog
./setup.py install

# EasyStock
cd ~/apps/easystock

./setup.py compile_catalog
po2json easystock/locale/ easystock/static/translations/ EasyStock
./setup.py install
```

## Populate the database
```
su - easystock

easystock-init-db conf/application.ini
```

## Enable application
```
cd /etc/apache2/conf-available/

ln -s /srv/easystock/conf/apache.conf easystock.conf

a2enconf easystock.conf

systemctl reload apache2
```

Enjoy!
