--
-- EasyStock upgrade script for v0.3
--

SET search_path=public,pg_catalog;

-- Areas
ALTER TABLE areas ADD COLUMN creation_date timestamp;
ALTER TABLE areas ADD COLUMN modification_date timestamp;
ALTER TABLE areas ADD COLUMN status character varying(255);

UPDATE areas SET creation_date = NOW();
UPDATE areas SET modification_date = NOW();
UPDATE areas SET status = 'active';

ALTER TABLE areas ALTER COLUMN creation_date SET NOT NULL;
ALTER TABLE areas ALTER COLUMN modification_date SET NOT NULL;
ALTER TABLE areas ALTER COLUMN status SET NOT NULL;

-- Users <-> Timelines
CREATE TABLE public.timelines_users_associations (
       user_id integer NOT NULL,
       timeline_id integer NOT NULL,
       CONSTRAINT timelines_users_associations_pk PRIMARY KEY (user_id,timeline_id)

);

ALTER TABLE public.timelines_users_associations ADD CONSTRAINT users_fk FOREIGN KEY (user_id)
    REFERENCES public.users (id) MATCH FULL
    ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE public.timelines_users_associations ADD CONSTRAINT timelines_fk FOREIGN KEY (timeline_id)
    REFERENCES public.timelines (id) MATCH FULL
    ON DELETE SET NULL ON UPDATE CASCADE;
