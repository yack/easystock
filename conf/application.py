# *-* coding: utf-8 *-*

import os
os.environ['PYTHON_EGG_CACHE'] = '/tmp/easystock-app.eggs'

BASEDIR = '/srv/easystock'
VENVDIR = os.path.join(BASEDIR, 'venv')
INIFILE = os.path.join(BASEDIR, 'conf', 'application.ini')

# Activate virtual env
activation_script = os.path.join(VENVDIR, 'bin/activate_this.py')
execfile(activation_script, dict(__file__=activation_script))

# Load application
from pyramid.paster import get_app
from pyramid.paster import setup_logging

setup_logging(INIFILE)
application = get_app(INIFILE, 'main')
