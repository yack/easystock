# EasyStock

EasyStock is a simple tool to manage stock and assets.


## Database setup (PostgreSQL)
```
su - postgres
psql

CREATE USER easystock WITH PASSWORD 'easystock';
CREATE DATABASE easystock WITH OWNER easystock;

\c easystock

CREATE EXTENSION unaccent;

\d
psql < docs/easystock.sql
```


## Development
The project is managed using [Poetry](https://poetry.eustace.io/docs/#installation):
```
# Create virtualenv
mkdir .venv
python3 -m venv .venv

# Activate virtualenv
source .venv/bin/activate

# Install Poetry
pip install poetry

# Install application in development mode
poetry install --extras "{source|binary} [api-doc] [auth-ldap] [auth-radius]"
poetry run invoke i18n.generate

# Run web server in development mode
poetry run invoke serve
```


## I18n
Extract messages
```
poetry run invoke i18n.extract i18n.update
```

Compile catalogs and update JSON files
```
poetry run invoke i18n.generate
```

Create new language
```
poetry run invoke i18n.init {locale_name}
```


## Installation
```
pip install easystock

easystock-init-db development.ini
```
