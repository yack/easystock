from invoke import Collection, task

import tasks.i18n


@task
def serve(c):
    """Run the development server."""

    print(serve.__doc__)

    c.run('gunicorn --reload --paste development.ini', pty=True)


ns = Collection()
ns.add_task(serve)
ns.add_collection(tasks.i18n)
